import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'app/navigation.dart';
import 'app_config.dart';
import 'assets.dart';

class PaymentSubIOS extends StatefulWidget {
  final bool fromSignUp;
  final int premiumIndex;

  const PaymentSubIOS({Key key, this.fromSignUp = false, this.premiumIndex})
      : super(key: key);
  @override
  _PaymentSubIOSState createState() => _PaymentSubIOSState();
}

class _PaymentSubIOSState extends State<PaymentSubIOS> {
  PageController pc = PageController();
  int currentPage = 0;
  final packages =
      availablePackages.where((e) => !e.id.contains("ads")).toList();
  //BaseModel package = appSettingsModel.getModel(FEATURES_PREMIUM);

  StreamSubscription<List<PurchaseDetails>> _subscription;
  List<StreamSubscription> subs = List();
  bool _purchasePending = false;
  //final FlutterInappPurchase flutterIAP = FlutterInappPurchase.instance;
  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];
  bool inAppSetup = false;

  @override
  void initState() {
    super.initState();
    if (widget.fromSignUp) {
      pc = PageController(initialPage: widget.premiumIndex);
    }
//    Stream purchaseUpdated =
//        InAppPurchaseConnection.instance.purchaseUpdatedStream;
//    _subscription = purchaseUpdated.listen((purchaseDetailsList) {
//      _listenToPurchaseUpdated(purchaseDetailsList);
//    }, onDone: () {
//      _subscription.cancel();
//    }, onError: (error) {
//      // handle error here.
//    });
    asyncInitState();
  }

  void asyncInitState() async {
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');
    if (!mounted) return;
//    String msg = await FlutterInappPurchase.instance.consumeAllItems;
//    print("consumeAllItems: $msg");

    if (Platform.isAndroid) {
      // refresh items for android
      try {
        String msg = await FlutterInappPurchase.instance.consumeAllItems;
        print('consumeAllItems: $msg');
      } catch (err) {
        print('consumeAllItems error: $err');
      }
    }

    getInAppProducts();

    var sub = FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });

    var sub1 = FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      print('purchase-updated: $productItem');
      if (inAppSetup) handleSubscribed();
    });

    var sub2 = FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error: $purchaseError');
      onError(purchaseError);
    });
    subs.add(sub);
    subs.add(sub1);
    subs.add(sub2);
  }

  @override
  void dispose() {
//    _subscription.cancel();
    for (var s in subs) s?.cancel();
    FlutterInappPurchase.instance?.endConnection;
    super.dispose();
  }

  void getInAppProducts() async {
    final inAppIds = appSettingsModel.getList(SUB_IAP_IDS);
    print(inAppIds);
    List<IAPItem> items = await FlutterInappPurchase.instance
        .getProducts(List<String>.from(inAppIds));
//        .getProducts(List<String>.from(["android.test.purchased"]));
    print(items.length);
    for (var item in items) {
      print('${item.toString()}');
      if (item.productId.contains("ads")) continue;
      this._items.add(item);
    }
    //this._items = items;
    inAppSetup = true;
    setState(() {});
  }

  buyProduct() async {
    showProgress(true, context, msg: "Processing");

    final inAppItem = _items[currentPage];
    FlutterInappPurchase.instance.requestPurchase(
      inAppItem.productId,
    );
  }

  onError(e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0, "Opps Error!", e.toString(),
        delayInMilli: 1000);
  }

  handleSubscribed() {
    int months;
    if (currentPage == 0) months = 12;
    if (currentPage == 1) months = 1;
    if (currentPage == 2) months = 6;

    int duration =
        DateTime.now().add(Duration(days: months * 30)).millisecondsSinceEpoch;

    //int duration = Jiffy().add(months: months).millisecondsSinceEpoch;
    userModel
      ..put(ACCOUNT_TYPE, ACCOUNT_TYPE_PREMIUM)
      ..put(PREMIUM_INDEX, currentPage)
      ..put(AMOUNT, _items[currentPage].price)
      ..put(SUBSCRIPTION_EXPIRY, duration)
//      ..put(ORDER_ID, productItem.orderId)
      ..updateItems();
    showProgress(false, context);
    showMessage(context, Icons.check, green, "Purchase Successful",
        "You have subscribe for our Premium Services for ${_items[currentPage].title}",
        delayInMilli: 1200, onClicked: (_) {
      Navigator.pop(context);
    });

    print("xx Delivering Goods");
  }

//  void _handleInvalidPurchase(PurchaseDetails purchaseDetails) {
//    // handle invalid purchase here if  _verifyPurchase` failed.
//  }
//
//  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
//    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
//      if (purchaseDetails.status == PurchaseStatus.pending) {
//        print("xx Pending purchase");
//        showPendingUI();
//      } else {
//        if (purchaseDetails.status == PurchaseStatus.error) {
//          handleError(purchaseDetails.error);
//        } else if (purchaseDetails.status == PurchaseStatus.purchased) {
//          bool valid = await _verifyPurchase(purchaseDetails);
//          if (valid) {
//            deliverProduct(purchaseDetails);
//          } else {
//            _handleInvalidPurchase(purchaseDetails);
//            return;
//          }
//        }
//        if (Platform.isAndroid) {
//          if (!kAutoConsume /*&& purchaseDetails.productID == _kConsumableId*/) {
//            print("xx Consuming");
//            await InAppPurchaseConnection.instance
//                .consumePurchase(purchaseDetails);
//          }
//        }
//        if (purchaseDetails.pendingCompletePurchase) {
//          print("xx PendingComplete");
//          await InAppPurchaseConnection.instance
//              .completePurchase(purchaseDetails);
//        }
//      }
//    });
//  }

//  void deliverProduct(PurchaseDetails purchaseDetails) async {
//    int months;
//    if (currentPage == 0) months = 12;
//    if (currentPage == 1) months = 1;
//    if (currentPage == 2) months = 6;
//
//    int duration = Jiffy().add(months: months).millisecondsSinceEpoch;
//    userModel
//      ..put(ACCOUNT_TYPE, ACCOUNT_TYPE_PREMIUM)
//      ..put(PREMIUM_INDEX, currentPage)
//      ..put(AMOUNT, packages[currentPage].price)
//      ..put(SUBSCRIPTION_EXPIRY, duration)
//      ..updateItems();
//
//    showMessage(context, Icons.check, blue0, "Purchase successful",
//        "You have subscribe for our Premium Services for ${packages[currentPage].title}",
//        delayInMilli: 500);
//
//    print("xx Delivering Goods");
//    setState(() {
//      _purchasePending = false;
////      _consumables = consumables;
//    });
//  }
//
//  void showPendingUI() {
//    setState(() {
//      _purchasePending = true;
//    });
//  }
//
//  void handleError(IAPError error) {
//    setState(() {
//      _purchasePending = false;
//    });
//  }
//
//  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) {
//    // IMPORTANT!! Always verify a purchase before delivering the product.
//    // For the purpose of an example, we directly return true.
//    return Future<bool>.value(true);
//  }

  @override
  Widget build(BuildContext context) {
    //availablePackages.sort((a, b) => a.id.compareTo(b.id));
    return WillPopScope(
      onWillPop: () async {
        if (widget.fromSignUp) {
          popUpUntil(context, MainAdmin());
        } else {
          return true;
        }
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Stack(
          children: [
            Container(
              //padding: EdgeInsets.fromLTRB(0, 50, 0, 10),
              alignment: Alignment.topCenter,
              decoration: BoxDecoration(color: AppConfig.appColor),
              height: 150,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                            onTap: () {
                              if (widget.fromSignUp) {
                                popUpUntil(context, MainAdmin());
                              } else {
                                Navigator.of(context).pop();
                              }
                            },
                            child: Container(
                              width: 50,
                              height: 30,
                              child: Center(
                                  child: Icon(
                                Icons.keyboard_backspace,
                                color: black,
                                size: 20,
                              )),
                            )),
                        GestureDetector(
                          onTap: () {},
                          child: Center(
                              child: Text(
                            "Subscriptions",
                            //style: textStyle(true, 20, black)
                            style: textStyle(true, 25, black),
                          )),
                        ),
                        addSpaceWidth(10),
                        Spacer(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 120),
              decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20))),
              child: Builder(
                builder: (ct) {
                  if (!inAppSetup) return loadingLayout();

                  if (_items.isEmpty)
                    return emptyLayout(
                        Icons.sentiment_dissatisfied,
                        "No Subscriptions",
                        "Failed to retrive subscription packages",
                        clickText: "Try Again", click: () {
                      setState(() {
                        inAppSetup = false;
                      });
                      asyncInitState();
                    });

                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                        child: Card(
                          color: black.withOpacity(0.2),
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: List.generate(_items.length, (p) {
                              final title = _items[p].title;
                              bool selected = p == currentPage;
                              return Flexible(
                                child: GestureDetector(
                                  onTap: () {
                                    pc.animateToPage(p,
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.ease);
                                  },
                                  child: Container(
                                      margin: EdgeInsets.all(4),
                                      height: 30,
                                      decoration: !selected
                                          ? null
                                          : BoxDecoration(
                                              color: selected
                                                  ? white
                                                  : transparent,
                                              borderRadius:
                                                  BorderRadius.circular(25)),
                                      child: Center(
                                          child: Text(
                                        title,
                                        style: textStyle(
                                            true,
                                            16,
                                            selected
                                                ? black
                                                : (white.withOpacity(.7))),
                                        textAlign: TextAlign.center,
                                      ))),
                                ),
                                fit: FlexFit.tight,
                              );
                            }),
                          ),
                        ),
                      ),
                      //addSpace(30),
                      Flexible(
                        child: PageView.builder(
                            controller: pc,
                            onPageChanged: (p) {
                              setState(() {
                                currentPage = p;
                              });
                            },
                            itemCount: _items.length,
                            itemBuilder: (ctx, p) {
                              final fee = _items[p].price;
                              final features = _items[p].description;

                              return SingleChildScrollView(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    addSpace(20),
                                    Center(
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 20),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: black.withOpacity(0.5))),
                                        child: Text.rich(TextSpan(children: [
                                          TextSpan(
                                              text: "$fee",
                                              style:
                                                  textStyle(true, 50, black)),
                                          TextSpan(
                                              text: _items[p].currency,
                                              style:
                                                  textStyle(false, 20, black)),
                                        ])),
                                      ),
                                    ),

//                                Container(
//                                  decoration: BoxDecoration(
//                                      color: red,
//                                      borderRadius: BorderRadius.circular(8)),
//                                  padding: EdgeInsets.all(10),
//                                  margin: EdgeInsets.all(10),
//                                  child: Row(
//                                    children: [
//                                      Icon(
//                                        Icons.info,
//                                        color: white,
//                                      ),
//                                      addSpaceWidth(10),
//                                      Flexible(
//                                        child: Text(
//                                          "Note: The Price would be converted to your country's currency at checkout",
//                                          style: textStyle(false, 14, white),
//                                        ),
//                                      ),
//                                    ],
//                                  ),
//                                ),
                                    Container(
                                      margin: EdgeInsets.all(10),
                                      child: Center(
                                        child: Text(
                                          "Features",
                                          style: textStyle(true, 23, black),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 15),
                                      child: Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 5, vertical: 5),
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: black
                                                        .withOpacity(.05)))),
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 30,
                                              width: 30,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  color: green,
                                                  shape: BoxShape.circle),
                                              child: Icon(
                                                Icons.check,
                                                color: white,
                                              ),
                                            ),
                                            addSpaceWidth(20),
                                            Flexible(
                                              child: Text(features,
                                                  style: textStyle(
                                                    false,
                                                    18,
                                                    black.withOpacity(0.6),
                                                  )),
                                            )
                                          ],
                                        ),
                                      ),
                                      /* child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children:
                                            List.generate(features.length, (p) {
                                          return Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            padding: EdgeInsets.all(8),
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: black
                                                            .withOpacity(.05)))),
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 30,
                                                  width: 30,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                      color: green,
                                                      shape: BoxShape.circle),
                                                  child: Icon(
                                                    Icons.check,
                                                    color: white,
                                                  ),
                                                ),
                                                addSpaceWidth(20),
                                                Flexible(
                                                  child: Text(features[p],
                                                      style: textStyle(
                                                        false,
                                                        18,
                                                        black.withOpacity(0.6),
                                                      )),
                                                )
                                              ],
                                            ),
                                          );
                                        })),*/
                                    ),
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 25, right: 25),
                                      child: FlatButton(
                                        onPressed: () {
                                          buyProduct();

//                                          ProductDetails productDetails =
//                                              packages[currentPage];
//                                          PurchaseParam purchaseParam =
//                                              PurchaseParam(
//                                                  productDetails:
//                                                      productDetails,
//                                                  applicationUserName: null,
//                                                  sandboxTesting: true);
//
//                                          connection.buyConsumable(
//                                              purchaseParam: purchaseParam,
//                                              autoConsume: kAutoConsume ||
//                                                  Platform.isIOS);
//                                      pushAndResult(
//                                        context,
//                                        PaymentDialog(
//                                          premiumPosition: p,
//                                          amount: double.parse(fee),
//                                        ),
//                                        depend: false,
//                                      );
                                        },
                                        padding: EdgeInsets.all(20),
                                        color: AppConfig.appColor,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25)),
                                        child: Center(
                                            child: Text(
                                          "SUBSCRIBE",
                                          style: textStyle(true, 18, white),
                                        )),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            }),
                      )
                    ],
                  );
                },
              ),
            ),
            if (_purchasePending)
              Stack(
                children: [
                  Opacity(
                    opacity: 0.3,
                    child: const ModalBarrier(
                        dismissible: false, color: Colors.grey),
                  ),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';

class AdManager {
  static String get appId {
    BaseModel adKeys = appSettingsModel.getModel('adKeys');
    String android = adKeys.getModel('appId').getString('Android');
    String ios = adKeys.getModel('appId').getString('IOS');

    if (Platform.isAndroid) {
      return android;
    } else if (Platform.isIOS) {
      return ios;
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get bannerAdUnitId {
    BaseModel adKeys = appSettingsModel.getModel('adKeys');
    String android = adKeys.getModel('bannerAdUnitId').getString('Android');
    String ios = adKeys.getModel('bannerAdUnitId').getString('IOS');

    if (Platform.isAndroid) {
      return android;
    } else if (Platform.isIOS) {
      return ios;
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get interstitialAdUnitId {
    BaseModel adKeys = appSettingsModel.getModel('adKeys');
    String android =
        adKeys.getModel('interstitialAdUnitId').getString('Android');
    String ios = adKeys.getModel('interstitialAdUnitId').getString('IOS');

    if (Platform.isAndroid) {
      return android;
    } else if (Platform.isIOS) {
      return ios;
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get rewardedAdUnitId {
    BaseModel adKeys = appSettingsModel.getModel('adKeys');

    String android = adKeys.getModel('rewardedAdUnitIds').getString('Android');
    String ios = adKeys.getModel('rewardedAdUnitIds').getString('IOS');

    if (Platform.isAndroid) {
      return android;
    } else if (Platform.isIOS) {
      return ios;
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get nativeAdUnitId {
    BaseModel adKeys = appSettingsModel.getModel('adKeys');
    String android = adKeys.getModel('nativeAdUnitId').getString('Android');
    String ios = adKeys.getModel('nativeAdUnitId').getString('IOS');

    if (Platform.isAndroid) {
      return android;
    } else if (Platform.isIOS) {
      return ios;
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
}

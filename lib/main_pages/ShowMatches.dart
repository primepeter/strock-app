import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/main_pages/PeopleItems.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ShowMatches extends StatefulWidget {
  final bool showBar;
  final bool fromHome;

  const ShowMatches({Key key, this.showBar = true, this.fromHome = false})
      : super(key: key);
  @override
  _ShowMatchesState createState() => _ShowMatchesState();
}

class _ShowMatchesState extends State<ShowMatches> {
  final pc = PageController();
  int currentPage = 0;

  @override
  void initState() {
    super.initState();
    overlayController.add(false);
    loadMatches();
    loadMyViews();
  }

  loadMatches() async {
    //load persons i have been matched with
    //load my matches
    Firestore.instance
        .collection(USER_BASE)
        .where(MATCHED_LIST, arrayContains: userModel.getObjectId())
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(MATCHED_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        int index = matchesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          matchesList.add(model);
        } else {
          matchesList[index] = model;
        }
      }
      matchesSetup = true;
      setState(() {});
    });
  }

  loadMyViews() async {
    final list = userModel.getList(VIEWED_LIST);
    for (var docId in list) {
      final doc =
          await Firestore.instance.collection(USER_BASE).document(docId).get();
      if (!doc.exists) continue;
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
      if (!model.signUpCompleted) {
//        userModel
//          ..putInList(VIEWED_LIST, model.getUserId(), false)
//          ..updateItems();
        continue;
      }
      int index =
          seenList.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
      if (index == -1) {
        seenList.add(model);
      } else {
        seenList[index] = model;
      }
    }
    seenSetup = true;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        overlayController.add(true);
        Navigator.pop(context, "");
        return false;
      },
      child: Scaffold(
        backgroundColor: default_white,
        body: page(),
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, widget.showBar ? 40 : 0, 0, 10),
          color: white,
          child: Column(
            children: [
              //if (widget.fromHome) addSpace(40),
              // addSpace(10),
              Row(
                children: [
                  InkWell(
                      onTap: () {
                        overlayController.add(true);
                        Navigator.pop(context, "");
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        //color: red,
                        child: Center(
                            child: Icon(
                          Icons.keyboard_backspace,
                          color: black,
                          size: 25,
                        )),
                      )),
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 50, 0),
                      child: Card(
                        color: black.withOpacity(0.2),
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        child: Row(
                          children: List.generate(2, (p) {
                            String title =
                                p == 0 ? "My Matches" : "People I Viewed";
                            bool selected = p == currentPage;
                            return Flexible(
                              child: GestureDetector(
                                onTap: () {
                                  pc.animateToPage(p,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.ease);
                                },
                                child: Container(
                                    margin: EdgeInsets.all(4),
                                    height: 30,
                                    decoration: !selected
                                        ? null
                                        : BoxDecoration(
                                            color:
                                                selected ? white : transparent,
//                      border: Border.all(color: black.withOpacity(.1),width: 3),
                                            borderRadius:
                                                BorderRadius.circular(25)),
                                    child: Center(
                                        child: Text(
                                      title,
                                      style: textStyle(
                                          true,
                                          14,
                                          selected
                                              ? black
                                              : (white.withOpacity(.7))),
                                      textAlign: TextAlign.center,
                                    ))),
                              ),
                              fit: FlexFit.tight,
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Expanded(
            flex: 1,
            child: PageView(
                controller: pc,
                onPageChanged: (p) {
                  setState(() {
                    currentPage = p;
                  });
                },
                children: [PeopleItems(0), PeopleItems(1)]))
      ],
    );
  }
}

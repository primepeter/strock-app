import 'dart:async';
import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/SearchPlace.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:photo/photo.dart';

//BuildContext _frameContext;

class CreateAdsIOS extends StatefulWidget {
  @override
  _CreateAdsIOSState createState() => _CreateAdsIOSState();
}

class _CreateAdsIOSState extends State<CreateAdsIOS> {
  final titleController = TextEditingController();
  final urlLinkController = TextEditingController();
  final priceController = TextEditingController();
  File adsImage;
  int startAt = 0;
  int endAt = 0;
  String startAtStr;
  String endAtStr;
  BaseModel locationModel;
  BaseModel adsModel;
  double localAdsCost;

  String yourPaying = "";
  int adsRunFor;

  String baseCurrency = appSettingsModel.getString(APP_CURRENCY);
  String baseCurrencyName = appSettingsModel.getString(APP_CURRENCY_NAME);
  String myCountry = userModel.getString(COUNTRY);
  double adsCostPerDay;
  double baseAdsCost;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StreamSubscription<List<PurchaseDetails>> _subscription;
  bool _purchasePending = false;

  List<StreamSubscription> subs = List();
  //final FlutterInappPurchase flutterIAP = FlutterInappPurchase.instance;
  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];
  bool inAppSetup = false;

  final packages =
      availablePackages.where((e) => e.id.contains("ads")).toList();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    int myPlan = userModel.getInt(ACCOUNT_TYPE);
    String key = myPlan == 0 ? FEATURES_REGULAR : FEATURES_PREMIUM;
    BaseModel package = appSettingsModel.getModel(key);
    adsCostPerDay = package.getDouble(ADS_PRICE);

//    Stream purchaseUpdated =
//        InAppPurchaseConnection.instance.purchaseUpdatedStream;
//    _subscription = purchaseUpdated.listen((purchaseDetailsList) {
//      _listenToPurchaseUpdated(purchaseDetailsList);
//    }, onDone: () {
//      _subscription.cancel();
//    }, onError: (error) {
//      // handle error here.
//    });
    asyncInitState();
  }

//  void _handleInvalidPurchase(PurchaseDetails purchaseDetails) {
//    // handle invalid purchase here if  _verifyPurchase` failed.
//  }
//
//  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
//    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
//      if (purchaseDetails.status == PurchaseStatus.pending) {
//        print("xx Pending purchase");
//        showPendingUI();
//      } else {
//        if (purchaseDetails.status == PurchaseStatus.error) {
//          handleError(purchaseDetails.error);
//        } else if (purchaseDetails.status == PurchaseStatus.purchased) {
//          bool valid = await _verifyPurchase(purchaseDetails);
//          if (valid) {
//            deliverProduct(purchaseDetails);
//          } else {
//            _handleInvalidPurchase(purchaseDetails);
//            return;
//          }
//        }
//        if (Platform.isAndroid) {
//          if (!kAutoConsume /*&& purchaseDetails.productID == _kConsumableId*/) {
//            print("xx Consuming");
//            await InAppPurchaseConnection.instance
//                .consumePurchase(purchaseDetails);
//          }
//        }
//        if (purchaseDetails.pendingCompletePurchase) {
//          print("xx PendingComplete");
//          await InAppPurchaseConnection.instance
//              .completePurchase(purchaseDetails);
//        }
//      }
//    });
//  }
//
//  void deliverProduct(PurchaseDetails purchaseDetails) async {
//    handleAdsPayment(adsModel);
//  }
//
//  void showPendingUI() {
//    setState(() {
//      _purchasePending = true;
//    });
//  }
//
//  void handleError(IAPError error) {
//    setState(() {
//      _purchasePending = false;
//    });
//  }
//
//  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) {
//    // IMPORTANT!! Always verify a purchase before delivering the product.
//    // For the purpose of an example, we directly return true.
//    return Future<bool>.value(true);
//  }

  void asyncInitState() async {
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');
    if (!mounted) return;
//    String msg = await FlutterInappPurchase.instance.consumeAllItems;
//    print("consumeAllItems: $msg");

    if (Platform.isAndroid) {
      // refresh items for android
      try {
        String msg = await FlutterInappPurchase.instance.consumeAllItems;
        print('consumeAllItems: $msg');
      } catch (err) {
        print('consumeAllItems error: $err');
      }
    }

    getInAppProducts();

    var sub = FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });

    var sub1 = FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      print('purchase-updated: $productItem');
      handleAdsPayment(adsModel);
    });

    var sub2 = FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error: $purchaseError');
      onError(purchaseError);
    });
    subs.add(sub);
    subs.add(sub1);
    subs.add(sub2);
  }

  @override
  void dispose() {
//    _subscription.cancel();
    for (var s in subs) s?.cancel();
    FlutterInappPurchase.instance?.endConnection;
    super.dispose();
  }

  void getInAppProducts() async {
    final inAppIds = appSettingsModel.getList(ADS_IAP_IDS);
    List<IAPItem> items = await FlutterInappPurchase.instance
        .getProducts(List<String>.from(inAppIds));
//        .getProducts(List<String>.from(["android.test.purchased"]));
    for (var item in items) {
      print('${item.toString()}');
      if (!item.productId.contains("ads")) continue;
      this._items.add(item);
    }
    //this._items = items;
    inAppSetup = true;
    setState(() {});
  }

  buyProduct() async {
    showProgress(true, context, msg: "Processing");
    final inAppItem = _items[selectedPosition];
    FlutterInappPurchase.instance.requestPurchase(
      inAppItem.productId,
    );
  }

  onError(e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0, "Opps Error!", e.toString(),
        delayInMilli: 1000);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: white,
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
                color: white,
                child: Row(
                  children: <Widget>[
                    InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          width: 50,
                          height: 50,
                          child: Center(
                              child: Icon(
                            Icons.keyboard_backspace,
                            color: black,
                            size: 25,
                          )),
                        )),
                    Text(
                      "Create Ads",
                      style: textStyle(true, 25, black),
                    ),
                    Spacer()
                  ],
                ),
              ),
              page(),
//              Container(
//                padding: EdgeInsets.all(15),
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: [
//                    Text.rich(TextSpan(children: [
//                      TextSpan(
//                          text: "Cost Per Day",
//                          style: textStyle(true, 14, black.withOpacity(0.6))),
//                      TextSpan(
//                          text: " $baseCurrency $adsCostPerDay",
//                          style: textStyle(true, 18, black)),
//                    ])),
//                    if (null != adsRunFor)
//                      Text.rich(TextSpan(children: [
//                        TextSpan(
//                            text: "Your ads will run for ",
//                            style: textStyle(true, 14, black.withOpacity(0.6))),
//                        TextSpan(
//                            text: " $adsRunFor day(s)",
//                            style: textStyle(true, 18, black)),
//                      ])),
//                  ],
//                ),
//              ),
              Container(
                padding: EdgeInsets.all(15),
                child: FlatButton(
                  onPressed: () {
                    validateFields();
                  },
                  padding: EdgeInsets.all(15),
                  color: AppConfig.appColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  child: Center(
                      child: Text(
                    "PAY $yourPaying",
                    style: textStyle(true, 18, white),
                  )),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  //title, image, duration(start and end) ,generate price, payment button

  String selectedAds = "";
  int selectedPosition = -1;

  page() {
    return Flexible(
      child: ListView(
        padding: EdgeInsets.all(15),
        children: [
          GestureDetector(
            onTap: () {
              pickAssets();
            },
            child: Container(
              height: 200,
              alignment: Alignment.center,
              child: Text(
                "Add Advert Image",
                style: textStyle(true, 20, black),
              ),
              decoration: BoxDecoration(
                  image: adsImage == null
                      ? null
                      : DecorationImage(
                          image: FileImage(adsImage), fit: BoxFit.cover),
                  color: black.withOpacity(0.09)),
            ),
          ),
          addSpace(10),
          Container(
            decoration: BoxDecoration(
                color: red, borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.info,
                  color: white,
                ),
                addSpaceWidth(10),
                Flexible(
                  child: Text(
                    "Note: You are adviced to use a protrait image for better and clearer ads display",
                    style: textStyle(false, 14, white),
                  ),
                ),
              ],
            ),
          ),
          textFieldBox(titleController, "Enter Title", (v) => null),
          textFieldBox(urlLinkController, "Enter Web Address", (v) => null),
          addSpace(10),
          FlatButton(
            onPressed: () {
              pushAndResult(context, SearchPlace(), result: (BaseModel res) {
                setState(() {
                  locationModel = res;
                });
              }, depend: false);
            },
            padding: EdgeInsets.all(15),
            color: black.withOpacity(0.3),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.location_on,
                  color: white.withOpacity(.5),
                ),
                addSpaceWidth(5),
                Text(
                  locationModel == null
                      ? "Pick a Location"
                      : (locationModel.getString(PLACE_NAME)),
                  style: textStyle(true, 18, white),
                ),
              ],
            ),
          ),
          addSpace(10),
          Container(
            decoration: BoxDecoration(
                color: red, borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.info,
                  color: white,
                ),
                addSpaceWidth(10),
                Flexible(
                  child: Text(
                    "Select a Region/Country you would like your Ad to be displayed",
                    style: textStyle(false, 14, white),
                  ),
                ),
              ],
            ),
          ),
          addSpace(10),
          Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: black.withOpacity(0.05)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Ads Time Frame",
                  style: textStyle(true, 14, black.withOpacity(.5)),
                ),
                addSpace(10),
                Column(
                  children: List.generate(_items.length, (p) {
                    final inAppAds = _items[p];
                    String details = inAppAds.description;
                    String coins = inAppAds.price;
                    bool selected = selectedAds == coins;
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedAds = coins;
                          selectedPosition = p;
                        });
                      },
                      child: Card(
                        color: white,
                        elevation: .5,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        margin:
                            EdgeInsets.fromLTRB(10, p == 0 ? 10 : 0, 10, 10),
                        child: Container(
                          //height: 50,
                          padding: EdgeInsets.all(6),
                          child: Row(
                            //crossAxisAlignment:CrossAxisAlignment.center,
                            children: <Widget>[
                              addSpaceWidth(10),
                              new Container(
                                //padding: EdgeInsets.all(2),
                                child: Container(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: blue09,
                                      border: Border.all(
                                          color: black.withOpacity(.1),
                                          width: 1)),
                                  child: Container(
                                    width: 13,
                                    height: 13,
                                    margin: EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: selected ? blue6 : transparent,
                                    ),
                                  ),
                                ),
                              ),
                              addSpaceWidth(15),
//                              Image.asset(
//                                ic_coin,
//                                width: 20,
//                                height: 20,
//                                color: gold,
//                              ),
//                              addSpaceWidth(5),
//                              Text(
//                                "$coins",
//                                style: textStyle(true, 15, gold),
//                              ),
                              Text.rich(TextSpan(children: [
                                TextSpan(
                                    text: _items[p].currency,
                                    style: textStyle(false, 14, black)),
                                TextSpan(
                                    text: " $coins ",
                                    style: textStyle(true, 16, black)),
                              ])),
                              addSpaceWidth(5),
                              Flexible(
                                child: Text("${inAppAds.description}",
                                    style: textStyle(true, 14, black)),
                              ),
                              //addSpaceWidth(20),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
                ),
              ],
            ),
          ),
          addSpace(30),
        ],
      ),
    );
  }

  bool converted = false;

  doConversion() async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      snack("No internet connectivity");
      return;
    }

    if (0 == startAt || 0 == endAt) return;
    setState(() {
      converted = false;
      yourPaying = "";
      adsRunFor = null;
    });

//    final apiKey = "c4539a5b787cd95c601c";
//    String countryUrl = "https://restcountries.eu/rest/v2/name/$myCountry";
//    var response = await http.get(countryUrl);
//    final model = BaseModel(items: jsonDecode(response.body)[0]);
//    final currency = model.getListModel("currencies");
//    String code = currency[0].getString("code");
//    String symbol = currency[0].getString("symbol");
//    String conKey = "${baseCurrency}_$code";
//    String conBaseUrl = "https://free.currconv.com/api/v7/convert";
//    String conversionUrl = "$conBaseUrl?q=$conKey&compact=ultra&apiKey=$apiKey";
//    var response2 = await http.get(conversionUrl);
//    Map perUnitData = jsonDecode(response2.body);
//    final perUnitValue = perUnitData[conKey];
//    print("Data $perUnitValue");

    final startDT = DateTime.fromMillisecondsSinceEpoch(startAt);
    final endDT = DateTime.fromMillisecondsSinceEpoch(endAt);
    adsRunFor = endDT.difference(startDT).inDays;
    //localAdsCost = (adsRunFor * adsCostPerDay * perUnitValue).roundToDouble();
    String symbol = packages[0].price.substring(0, 1);
    String cost = packages[0].price.substring(1);
    baseAdsCost = (adsRunFor * double.parse(cost)).roundToDouble();
    yourPaying = "($symbol $localAdsCost)";
    print("Ads Costs $baseAdsCost");
    setState(() {
      converted = true;
    });
  }

  String formatTimeChosen(int time) {
    final date = DateTime.fromMillisecondsSinceEpoch(time);
    return new DateFormat("MMMM d y").format(date);
  }

  textFieldBox(
      TextEditingController controller, String hint, setstate(String v),
      {focusNode, int maxLength, bool number = false}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: TextFormField(
        focusNode: focusNode,
        maxLength: maxLength,
        //maxLengthEnforced: false,
        controller: controller,
        decoration: InputDecoration(labelText: hint, counter: Container()),
        onChanged: setstate,
        keyboardType: number ? TextInputType.number : null,
      ),
    );
  }

  void validateFields() async {
    FocusScope.of(context).requestFocus(FocusNode());

    String title = titleController.text;
    String urlLink = urlLinkController.text;

    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      snack("No internet connectivity");
      return;
    }

    if (adsImage == null) {
      snack("Add Advert Image");
      return;
    }

    if (title.isEmpty) {
      snack("Add Title to Ads");
      return;
    }

//    if (urlLink.isEmpty) {
//      snack("Add WebAddress to Ads");
//      return;
//    }
//
//    if (!urlLink.startsWith("http")) {
//      snack("Web Address must start with http");
//      return;
//    }

    if (locationModel == null) {
      snack("Add Display Location to Ads");
      return;
    }

    if (selectedPosition == -1) {
      snack("Select Ads Time Frame");
      return;
    }

    //showProgress(true, context, msg: "Please wait");
    String id = getRandomId();
    adsModel = BaseModel();
    adsModel.put(HAS_PAID, false);
    adsModel.put(OBJECT_ID, id);
    adsModel.put(ADS_IMAGE, adsImage.path);
    adsModel.put(TITLE, title);
    adsModel.put(ADS_URL, urlLink);
    adsModel.put(ADS_START_DATE, startAt);
    adsModel.put(ADS_END_DATE, endAt);
    adsModel.put(STATUS, /*isAdmin ? APPROVED :*/ PENDING);
    adsModel.put(COUNTRY, locationModel.getString(COUNTRY));
    adsModel.put(LATITUDE, locationModel.getString(LATITUDE));
    adsModel.put(LONGITUDE, locationModel.getString(LONGITUDE));
    if (selectedPosition == 0) adsRunFor = 1;
    if (selectedPosition == 2) adsRunFor = 5;
    if (selectedPosition == 1) adsRunFor = 10;
    adsModel.put(
        ADS_EXPIRY, Jiffy().add(days: adsRunFor).millisecondsSinceEpoch);

    if (isAdmin) {
      showProgress(true, context, msg: "Processing");
      handleAdsAdmin(adsModel);
      return;
    }

    buyProduct();
  }

  snack(String text) {
    Future.delayed(Duration(milliseconds: 500), () {
      showSnack(_scaffoldKey, text, useWife: true);
    });
  }

  void pickAssets() async {
    PhotoPicker.pickAsset(
            maxSelected: 1,
            thumbSize: 250,
            context: context,
            provider: I18nProvider.english,
            pickType: PickType.onlyImage,
            rowCount: 3)
        .then((value) async {
      if (value == null) return;
      String path = (await value[0].originFile).path;
      adsImage = File(path);
      setState(() {});
    }).catchError((e) {});

    /// Use assetList to do something.
  }

  String id = getRandomId();

  void handleAdsAdmin(BaseModel adsModel) {
    uploadFile(File(adsModel.getString(ADS_IMAGE)), (res, e) {
      if (e != null) {
        handleAdsAdmin(adsModel);
        return;
      }

      String id = adsModel.getObjectId();
      adsModel
        ..put(ADS_IMAGE, res)
        ..put(HAS_PAID, true)
        ..put(AMOUNT, _items[selectedPosition].price)
        ..saveItem(ADS_BASE, true, document: id, onComplete: () {
          showProgress(false, context);
          showMessage(context, Icons.check, green, "Successful!",
              "You ads has been created successfully and it's being reviewed!",
              onClicked: (_) {
            Navigator.pop(context);
          }, delayInMilli: 1200);
        });
    });
  }

  void handleAdsPayment(BaseModel adsModel) {
    final model = BaseModel();
    model.put(OBJECT_ID, id);
    model.put(AMOUNT, baseAdsCost);
    model.put(ADS_ID, adsModel.getObjectId());
    model.put(TYPE, 1);
    model.saveItem(TRANSACTION_BASE, true, document: id, onComplete: () {
      uploadFile(File(adsModel.getString(ADS_IMAGE)), (res, e) {
        if (e != null) {
          handleAdsPayment(adsModel);
          return;
        }

        String id = adsModel.getObjectId();
        adsModel
          ..put(ADS_IMAGE, res)
          ..put(HAS_PAID, true)
          ..put(AMOUNT, baseAdsCost)
          ..saveItem(ADS_BASE, true, document: id, onComplete: () {
            showProgress(false, context);
            print("xx Delivering Goods");
            showMessage(context, Icons.check, green, "Successful!",
                "You ads has been created successfully and it's being reviewed!",
                onClicked: (_) {
              Navigator.pop(context);
            }, delayInMilli: 1200);
          });
      });
    });
  }
}

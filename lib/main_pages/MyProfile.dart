import 'dart:io';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/AppSettings.dart';
import 'package:Strokes/admin/AppAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/inputDialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filesize/filesize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:photo/photo.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:video_compress/video_compress.dart';

class MyProfile extends StatefulWidget {
  final bool showBar;

  const MyProfile({Key key, this.showBar = true}) : super(key: key);

  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  TextEditingController aboutController = TextEditingController();
  int myPreference = -1;
  bool refresh = false;
  List<BaseModel> profilePhotos = userModel.profilePhotos;
  List<BaseModel> hookUpPhotos = userModel.hookUpPhotos;
  int defProfilePhoto = userModel.getInt(DEF_PROFILE_PHOTO);
  int defStrockPhoto = userModel.getInt(DEF_STROCK_PHOTO);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  String get formatBirthDate {
    final date = DateTime.parse(userModel.birthDate);
    return new DateFormat("MMMM d").format(date);
  }

  double pixels = 0;
  page() {
    double blur = (pixels / 10);
    double imageHeight =
        (getScreenHeight(context) * 0.5) - (pixels > 0 ? 0 : (pixels));

    int pos = 0;
    bool isVideo = false;
    String imageUrl = '';

    if (userModel.profilePhotos.isNotEmpty) {
      pos = userModel.getInt(DEF_PROFILE_PHOTO) >
                  userModel.profilePhotos.length - 1 ||
              (userModel.getInt(DEF_PROFILE_PHOTO).isNegative)
          ? 0
          : userModel.getInt(DEF_PROFILE_PHOTO);
      imageUrl = userModel.profilePhotos[pos].imageUrl;
    }

    return Stack(
      children: [
        if (userModel.profilePhotos[pos].isLocal)
          placeHolder(imageHeight, width: double.infinity),
        if (userModel.profilePhotos[pos].isLocal)
          Image.file(
            File(imageUrl),
            fit: BoxFit.cover,
            height: imageHeight,
            width: double.infinity,
          )
        else
          CachedNetworkImage(
            imageUrl: imageUrl,
            fit: BoxFit.cover,
            height: imageHeight,
            width: double.infinity,
            placeholder: (c, s) {
              return placeHolder(imageHeight, width: double.infinity);
            },
          ),
        NotificationListener(
          onNotification: (ScrollNotification sn) {
            pixels = sn.metrics.pixels;
            print("Scroll $pixels");
            setState(() {});
            return false;
          },
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: double.infinity,
                  height: getScreenHeight(context) / 2,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: double.infinity,
                          height: 60,
                          decoration: BoxDecoration(
                              color: white,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(25),
                                  topLeft: Radius.circular(25))),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(20, 0, 10, 0),
                              height: 100,
                              width: 100,
                              child: Card(
                                margin: EdgeInsets.only(bottom: 5),
                                clipBehavior: Clip.antiAlias,
                                shape: CircleBorder(
                                    side: BorderSide(color: white, width: 2)),
                                child: CachedNetworkImage(
                                  imageUrl:
                                      userModel.profilePhotos[pos].imageUrl,
                                  fit: BoxFit.cover,
                                  placeholder: (c, s) {
                                    return placeHolder(100, width: 100);
                                  },
                                ),
                              ),
                            ),
                            Flexible(
                              child: Container(
                                  margin: EdgeInsets.only(bottom: 15),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        fit: FlexFit.tight,
                                        child: Text(
                                          userModel.getString(NAME),
                                          style: textStyle(false, 22, black),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      addSpaceWidth(5),
                                      IconButton(
                                        icon: Image.asset(
                                          ic_edit_profile,
                                          color: black,
                                          height: 20,
                                        ),
                                        onPressed: () {
                                          pushAndResult(
                                              context,
                                              inputDialog(
                                                "Your Name",
                                                message:
                                                    userModel.getString(NAME),
                                                hint: "What's your name?",
                                              ), result: (_) {
                                            if (null == _) return;
                                            userModel
                                              ..put(NAME, _)
                                              ..updateItems();

                                            setState(() {});
                                          }, depend: false);
                                        },
                                      ),
                                      addSpaceWidth(5),
                                    ],
                                  )),
                            )
                          ],
                        ),
                      ),
//                    Align(
//                      alignment: Alignment.bottomRight,
//                      child: Container(
//                        margin: EdgeInsets.fromLTRB(0, 0, 20, 80),
//                        child: FloatingActionButton(
//                          onPressed: () {},
//                          heroTag: "cax",
//                          backgroundColor: white,
//                          child: Icon(
//                            Icons.add_a_photo,
//                            size: 20,
//                            color: black,
//                          ),
//                        ),
//                      ),
//                    )
                    ],
                  ),
                ),
                Container(
                  color: white,
                  child: ListView(
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.all(0),
                    shrinkWrap: true,
                    children: [
                      addSpace(10),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
//                          Flexible(
//                              fit: FlexFit.tight,
//                              child: Column(
//                                children: [
//                                  Text(
//                                    "Popularity\n ${userModel.isPremium ? "Full" : "Very low"}",
//                                    textAlign: TextAlign.center,
//                                    style: textStyle(
//                                        false, 14, black.withOpacity(.5)),
//                                  ),
//                                  //addSpace(5),
//                                  Image.asset(
//                                    "assets/icons/${userModel.isPremium ? "battery" : "low-battery"}.png",
//                                    width: 50,
//                                  )
//                                ],
//                              )),
                          Container(
                            padding: EdgeInsets.only(left: 25),
                            child: GestureDetector(
                              onTap: () {
                                if (userModel.isPremium) return;
                                openSubscriptionPage(context);
                              },
                              child: Column(
                                children: [
                                  Text(
                                    "Your\nStatus",
                                    textAlign: TextAlign.center,
                                    style: textStyle(
                                        false, 14, black.withOpacity(.5)),
                                  ),
                                  addSpace(5),
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        userModel.isPremium
                                            ? "PREMIUM"
                                            : "REGULAR",
                                        style: textStyle(false, 18, black),
                                      ),
                                      addSpaceWidth(5),
                                      Icon(
                                        Icons.add_circle_outline,
                                        size: 18,
                                        color: blue0,
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
//                          Flexible(
//                              fit: FlexFit.tight,
//                              child: Column(
//                                children: [
//                                  Text(
//                                    "Super\nPowers",
//                                    textAlign: TextAlign.center,
//                                    style: textStyle(
//                                        false, 14, black.withOpacity(.5)),
//                                  ),
//                                  addSpace(5),
//                                  Row(
//                                    mainAxisSize: MainAxisSize.min,
//                                    children: [
//                                      Text(
//                                        userModel.isPremium ? "On" : "Off",
//                                        style: textStyle(false, 18, black),
//                                      ),
//                                      addSpaceWidth(5),
//                                      Icon(
//                                        Icons.add_circle_outline,
//                                        size: 18,
//                                        color: blue0,
//                                      )
//                                    ],
//                                  )
//                                ],
//                              )),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Profile Photos",
                                  style: textStyle(true, 22, black),
                                ),
                                Spacer(),
                                IconButton(
                                  onPressed: () {
                                    pickAssets("normal");
                                  },
                                  icon: Icon(Icons.add_a_photo),
                                )
                              ],
                            ),
                            photoBox(profilePhotos, (p) {
                              if (profilePhotos.length <= 1) return;
                              profilePhotos.removeAt(p);
                              defProfilePhoto = defProfilePhoto - 1;
                              userModel
                                ..put(DEF_PROFILE_PHOTO, defProfilePhoto)
                                ..updateItems();

                              setState(() {});
                              userModel
                                ..put(PROFILE_PHOTOS,
                                    profilePhotos.map((e) => e.items).toList())
                                ..updateItems();
                            }, def: defProfilePhoto, type: "normal"),
//                            addSpace(20),
//                            if (hookUpPhotos.isNotEmpty) ...[
//                              Row(
//                                children: [
//                                  Text(
//                                    "Quick Strock Photos",
//                                    style: textStyle(true, 22, black),
//                                  ),
//                                  Spacer(),
//                                  IconButton(
//                                    onPressed: () {
//                                      pickAssets("nah");
//                                    },
//                                    icon: Icon(Icons.add_a_photo),
//                                  )
//                                ],
//                              ),
//                              //if (hookUpPhotos.isNotEmpty)
//                              photoBox(hookUpPhotos, (p) {
//                                //if (hookUpPhotos.length <= 1) return;
//
//                                hookUpPhotos.removeAt(p);
//                                defStrockPhoto = defStrockPhoto - 1;
//                                userModel
//                                  ..put(DEF_STROCK_PHOTO, defStrockPhoto)
//                                  ..updateItems();
//
//                                setState(() {});
//                                userModel
//                                  ..put(HOOKUP_PHOTOS,
//                                      hookUpPhotos.map((e) => e.items).toList())
//                                  ..updateItems();
//                              }, onVisible: (p, v) {
//                                hookUpPhotos[p].put(VISIBILITY, v);
//                                userModel
//                                  ..put(HOOKUP_PHOTOS,
//                                      hookUpPhotos.map((e) => e.items).toList())
//                                  ..updateItems();
//                                setState(() {});
//                              }, def: defStrockPhoto, type: "nah"),
//                              addSpace(10)
//                            ] else
//                              GestureDetector(
//                                onTap: () {
//                                  pickAssets("nah");
//                                },
//                                child: Container(
//                                  decoration: BoxDecoration(
//                                      color: AppConfig.appColor,
//                                      borderRadius:
//                                          BorderRadius.all(Radius.circular(10)),
//                                      border: Border.all(
//                                          color: black.withOpacity(.1),
//                                          width: 1)),
//                                  padding: EdgeInsets.all(10),
//                                  child: Row(
//                                    children: [
//                                      Flexible(
//                                        child: Row(
//                                          children: [
//                                            Container(
//                                              width: 60,
//                                              height: 60,
//                                              decoration: BoxDecoration(
//                                                  shape: BoxShape.circle,
//                                                  border: Border.all(
//                                                      color:
//                                                          white.withOpacity(.5),
//                                                      width: 1)),
//                                              child: Center(
//                                                child: Icon(Icons.contacts),
//                                              ),
//                                            ),
//                                            addSpaceWidth(10),
//                                            Text(
//                                              "JOIN QUICK STROCK",
//                                              style:
//                                                  textStyle(false, 18, black),
//                                            ),
//                                          ],
//                                        ),
//                                      ),
//                                      addSpaceWidth(10),
//                                      Icon(
//                                        Icons.navigate_next,
//                                        size: 24,
//                                        color: white.withOpacity(.4),
//                                      )
//                                    ],
//                                  ),
//                                ),
//                              ),
                            addSpace(20),

                            Row(
                              children: [
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      "Wow Factor!!!",
                                      style: textStyle(true, 22, black),
                                    ),
                                    addSpaceWidth(5),
                                    GestureDetector(
                                        onTap: () {
                                          showMessage(
                                              context,
                                              Icons.info,
                                              black,
                                              "Wow Factor!!!",
                                              "What will make you go on a spontaneous meet/date?");
                                        },
                                        child: Icon(Icons.info)),
                                  ],
                                ),
                                Spacer(),
                                IconButton(
                                  icon: Image.asset(
                                    ic_edit_profile,
                                    color: black,
                                    height: 20,
                                  ),
                                  onPressed: () {
                                    pushAndResult(
                                        context,
                                        inputDialog(
                                          "Wow Factor!!!",
                                          hint: "Write about your wow factor?",
                                          message:
                                              userModel.getString(WOW_FACTOR),
                                        ), result: (_) {
                                      if (null == _) return;
                                      userModel
                                        ..put(WOW_FACTOR, _)
                                        ..updateItems();

                                      setState(() {});
                                    }, depend: false);
                                  },
                                ),
                              ],
                            ),
                            addSpace(15),
                            Container(
                                child: Text(
                                    userModel.getString(WOW_FACTOR).isEmpty
                                        ? "Write about your Wow Factor?"
                                        : userModel.getString(WOW_FACTOR),
                                    style: textStyle(
                                        false, 18, black.withOpacity(.6)))),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),

                            Row(
                              children: [
                                Text(
                                  "About you",
                                  style: textStyle(true, 22, black),
                                ),
                                Spacer(),
                                IconButton(
                                  icon: Image.asset(
                                    ic_edit_profile,
                                    color: black,
                                    height: 20,
                                  ),
                                  onPressed: () {
                                    pushAndResult(
                                        context,
                                        inputDialog(
                                          "About you",
                                          hint: "Write a bit about yourself",
                                          maxLength: 80,
                                          message:
                                              userModel.getString(ABOUT_ME),
                                        ), result: (_) {
                                      if (null == _) return;
                                      userModel
                                        ..put(ABOUT_ME, _)
                                        ..updateItems();

                                      setState(() {});
                                    }, depend: false);
                                  },
                                ),
                              ],
                            ),
                            addSpace(15),
                            Container(
                                child: Text(
                                    userModel.getString(ABOUT_ME).isEmpty
                                        ? "Write a bit about yourself"
                                        : userModel.getString(ABOUT_ME),
                                    style: textStyle(
                                        false, 18, black.withOpacity(.6)))),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                            Text(
                              "Your Birthday?",
                              style: textStyle(true, 22, black),
                            ),
                            addSpace(15),
                            Text(
                              formatBirthDate,
                              style:
                                  textStyle(false, 16, black.withOpacity(.7)),
                            ),
                            addSpace(15),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                            Text(
                              "Your Ethnicity preference?",
                              style: textStyle(true, 22, black),
                            ),
                            addSpace(15),
                            groupedButtons(
                                ethnicityType,
                                userModel.get(ETHNICITY) == null
                                    ? ""
                                    : ethnicityType[userModel
                                        .getInt(ETHNICITY)], (text, p) {
                              userModel
                                ..put(ETHNICITY, p)
                                ..updateItems();
                              refresh = true;
                              setState(() {
                                //myPreference = p;
                              });
                            },
                                selectedColor: AppConfig.appColor,
                                normalColor: black.withOpacity(.6),
                                selectedTextColor: white,
                                normalTextColor: black),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                            addSpace(15),
                            Text(
                              "What's your Gender preference?",
                              style: textStyle(true, 22, black),
                            ),
                            addSpace(15),
                            groupedButtons(
                                preferenceType,
                                userModel.get(PREFERENCE) == null
                                    ? ""
                                    : preferenceType[userModel
                                        .getInt(PREFERENCE)], (text, p) {
                              userModel
                                ..put(PREFERENCE, p)
                                ..updateItems();
                              refresh = true;
                              setState(() {
                                //myPreference = p;
                              });
                            },
                                selectedColor: AppConfig.appColor,
                                normalColor: black.withOpacity(.6),
                                selectedTextColor: white,
                                normalTextColor: black),
                            addSpace(15),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                            Text(
                              "Your Relationship preference?",
                              style: textStyle(true, 22, black),
                            ),
                            addSpace(15),
                            groupedButtons(
                                relationshipType,
                                userModel.get(RELATIONSHIP) == null
                                    ? ""
                                    : relationshipType[userModel
                                        .getInt(RELATIONSHIP)], (text, p) {
                              userModel
                                ..put(RELATIONSHIP, p)
                                ..updateItems();
                              refresh = true;
                              setState(() {
                                //myPreference = p;
                              });
                            },
                                selectedColor: AppConfig.appColor,
                                normalColor: black.withOpacity(.6),
                                selectedTextColor: white,
                                normalTextColor: black),
                            /* addSpace(15),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                            Text(
                              "Where do you live in?",
                              style: textStyle(true, 22, black),
                            ),
                            addSpace(15),
                            GestureDetector(
                              onTap: () {
                                pushAndResult(context, SearchPlace(),
                                    result: (BaseModel res) {
                                  String yourCity = res.getString(PLACE_NAME);
                                  userModel
                                    ..put(CITY, yourCity)
                                    ..updateItems();
                                  setState(() {});
                                }, depend: false);
                              },
                              child: Container(
                                  height: 40,
                                  width: double.infinity,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                        child: Text(
                                          userModel.city.isEmpty
                                              ? "Search for your city"
                                              : userModel.city,
                                          style: textStyle(
                                              false,
                                              18,
                                              black.withOpacity(
                                                  userModel.city.isEmpty
                                                      ? 0.7
                                                      : 1)),
                                        ),
                                      ),
                                      Icon(
                                        Icons.search,
                                        color: black.withOpacity(.7),
                                      )
                                    ],
                                  )),
                            ),*/
                            addSpace(15),
//                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
//                            Text(
//                              "Location",
//                              style: textStyle(true, 22, black),
//                            ),
//                            addSpace(15),
//                            Container(
//                                height: 40,
//                                width: double.infinity,
//                                child: Row(children: [
//                                  Flexible(
//                                      fit: FlexFit.tight,
//                                      child: Text(
//                                        userModel.getString(MY_LOCATION),
//                                        style: textStyle(false, 18, black),
//                                      )),
//                                  Text(userModel.getString(COUNTRY),
//                                      style: textStyle(
//                                          false, 14, black.withOpacity(.5)))
//                                ])),
//                            addSpace(15),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                            InkWell(
                              onTap: () {
                                pushAndResult(context, AppSettings(),
                                    depend: false);
                              },
                              child: Container(
                                  height: 50,
                                  width: double.infinity,
                                  child: Row(children: [
                                    Flexible(
                                        fit: FlexFit.tight,
                                        child: Text(
                                          "General Settings",
                                          style: textStyle(true, 18, black),
                                        )),
                                    Icon(
                                      Icons.navigate_next,
                                      size: 18,
                                      color: black,
                                    )
                                  ])),
                            ),
//                            addLine(1, black.withOpacity(.1), 0, 0, 0, 10),
//                            InkWell(
//                              onTap: () {
//                                pushAndResult(context, AdsPage());
//                              },
//                              child: Container(
//                                  height: 50,
//                                  width: double.infinity,
//                                  child: Row(children: [
//                                    Flexible(
//                                        fit: FlexFit.tight,
//                                        child: Text(
//                                          "Advertize With Us",
//                                          style: textStyle(true, 18, black),
//                                        )),
//                                    Icon(
//                                      Icons.navigate_next,
//                                      size: 18,
//                                      color: black,
//                                    )
//                                  ])),
//                            ),
                            addLine(1, black.withOpacity(.1), 0, 10, 0, 0),
//                            if (userModel.getInt(ACCOUNT_TYPE) == 1)
//                              InkWell(
//                                onTap: () {
//                                  showMessage(
//                                      context,
//                                      Icons.error,
//                                      red0,
//                                      "Cancel?",
//                                      "Are you sure you want to cancel your Subscription? This would revert you back to a regular user.",
//                                      textSize: 14,
//                                      clickYesText: "UnSubscribe",
//                                      onClicked: (_) {
//                                    if (_) {
//                                      userModel
//                                        ..put(ACCOUNT_TYPE, 0)
//                                        ..updateItems();
//                                      setState(() {});
//                                    }
//                                  }, clickNoText: "Cancel");
//                                },
//                                child: Container(
//                                    //height: 50,
//                                    color: red,
//                                    padding: EdgeInsets.all(10),
//                                    width: double.infinity,
//                                    child: Row(children: [
//                                      Flexible(
//                                          fit: FlexFit.tight,
//                                          child: Text(
//                                            "Cancel Subscription",
//                                            style: textStyle(true, 16, white),
//                                          )),
//                                      Icon(
//                                        Icons.navigate_next,
//                                        size: 18,
//                                        color: white,
//                                      )
//                                    ])),
//                              ),
//                            addLine(1, black.withOpacity(.1), 0, 0, 0, 10),

                            addLine(1, black.withOpacity(.1), 0, 0, 0, 10),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                if (isAdmin) ...[
                  InkWell(
                    onTap: () {
                      pushAndResult(context, AppAdmin(), depend: false);
                    },
                    child: Container(
                        //height: 50,
                        color: red,
                        padding: EdgeInsets.all(15),
                        width: double.infinity,
                        child: Row(children: [
                          Flexible(
                              fit: FlexFit.tight,
                              child: Text(
                                "Admin Portal",
                                style: textStyle(true, 18, white),
                              )),
                          Icon(
                            Icons.navigate_next,
                            size: 18,
                            color: white,
                          )
                        ])),
                  ),
                ],
//              addSpace(150),
              ],
            ),
          ),
        ),
        if (widget.showBar)
          Align(
            alignment: Alignment.topLeft,
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context, refresh);
              },
              child: Container(
                  margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                  padding: EdgeInsets.only(left: 10, right: 15),
                  height: 45,
                  width: 45,
                  decoration: BoxDecoration(
                      color: white,
                      boxShadow: [
                        BoxShadow(color: black.withOpacity(.3), blurRadius: 5)
                      ],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(15),
                          bottomRight: Radius.circular(15))),
                  child: Icon(
                    Icons.cancel,
                    size: 35,
                    color: black.withOpacity(.5),
                  )),
            ),
          ),
      ],
    );
  }

  photoBox(List<BaseModel> photos, onRemoved,
      {int def = 0, String type, onVisible}) {
    String key = DEF_PROFILE_PHOTO;
    if (type == "nah") key = DEF_STROCK_PHOTO;

    return Column(
      children: [
        if (photos.isNotEmpty)
          Container(
            height: 240,
            child: LayoutBuilder(
              builder: (ctx, b) {
                int photoLength = photos.length;
                return Column(
                  children: <Widget>[
                    Flexible(
                      child: ListView.builder(
                          itemCount: photoLength,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (ctx, p) {
                            BaseModel photo = photos[p];
                            bool isVideo = photo.isVideo;
                            String imageUrl = photo
                                .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);

                            String localUrl = photo.getString(
                                isVideo ? THUMBNAIL_PATH : IMAGE_PATH);

                            bool isLocal = photo.isLocal;
                            bool isDef = def == p;
                            bool isPublic = photo.getInt(VISIBILITY) == 0;

                            return GestureDetector(
                              onLongPress: () {
                                showMessage(context, Icons.error, red0,
                                    "Default?", "Make the photo default?",
                                    textSize: 14,
                                    clickYesText: "Yes", onClicked: (_) {
                                  if (_) {
                                    userModel
                                      ..put(key, p)
                                      ..updateItems();
                                    //if(type=="nah")
                                    defProfilePhoto =
                                        userModel.getInt(DEF_PROFILE_PHOTO);
//                                    defStrockPhoto =
//                                        userModel.getInt(DEF_STROCK_PHOTO);
                                    setState(() {});
                                    if (type == "nah")
                                      showMessage(
                                        context,
                                        Icons.error,
                                        red0,
                                        "Note!",
                                        "This Photo even though locked will appear on Quick Strock Searches",
                                        textSize: 14,
                                      );
                                  }
                                }, clickNoText: "Cancel");
                              },
                              child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Opacity(
                                    opacity: isPublic ? 1 : 0.15,
                                    child: Container(
                                      margin: EdgeInsets.all(8),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Stack(
                                            children: [
                                              if (isLocal)
                                                placeHolder(200, width: 160),
                                              if (isLocal)
                                                Image.file(
                                                  File(imageUrl),
                                                  height: 200,
                                                  width: 160,
                                                  fit: BoxFit.cover,
                                                )
                                              else
                                                CachedNetworkImage(
                                                  imageUrl: imageUrl,
                                                  height: 200,
                                                  width: 160,
                                                  fit: BoxFit.cover,
                                                  placeholder: (ctx, s) {
                                                    return placeHolder(200,
                                                        width: 160);
                                                  },
                                                ),
                                            ],
                                          )),
                                    ),
                                  ),
                                  if (isVideo)
                                    Center(
                                      child: Container(
                                        height: 50,
                                        width: 50,
                                        child: Icon(
                                          Icons.play_arrow,
                                          color: Colors.white,
                                        ),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.8),
                                            border: Border.all(
                                                color: Colors.white,
                                                width: 1.5),
                                            shape: BoxShape.circle),
                                      ),
                                    ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      height: 200,
                                      width: 160,
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(
                                                5, 25, 0, 0),
                                            width: 30,
                                            height: 30,
                                            child: new RaisedButton(
                                                padding: EdgeInsets.all(0),
                                                elevation: 2,
                                                shape: CircleBorder(),
                                                color: red0,
                                                child: Icon(
                                                  Icons.close,
                                                  color: white,
                                                  size: 13,
                                                ),
                                                onPressed: () {
                                                  if (def == p) {
                                                    showMessage(
                                                      context,
                                                      Icons.error,
                                                      red0,
                                                      "Opps Sorry!",
                                                      "Sorry you cant' delete photo set as default until you set another as default",
                                                      textSize: 14,
                                                    );
                                                    return;
                                                  }

                                                  onRemoved(p);
                                                }),
                                          ),
                                          if (type == "nah")
                                            Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  5, 25, 0, 0),
                                              width: 30,
                                              height: 30,
                                              child: new RaisedButton(
                                                  padding: EdgeInsets.all(0),
                                                  elevation: 2,
                                                  shape: CircleBorder(),
                                                  color: AppConfig.appColor,
                                                  child: Icon(
                                                    isPublic
                                                        ? Icons.visibility_off
                                                        : Icons.visibility,
                                                    color: white,
                                                    size: 13,
                                                  ),
                                                  onPressed: () {
                                                    onVisible(
                                                        p, isPublic ? 1 : 0);
                                                  }),
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  if (def == p)
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        padding: const EdgeInsets.all(8.0),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: white, width: 2),
                                            color: AppConfig.appColor,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Icon(
                                              Icons.check,
                                              size: 18,
                                            ),
                                            addSpaceWidth(5),
                                            Text("Default"),
                                          ],
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            );
                          }),
                    ),
                  ],
                );
              },
            ),
          ),
      ],
    );
  }

  void uploadPhotos(BaseModel model, String s, String id) {
    final photos = s == "normal" ? profilePhotos : hookUpPhotos;
    photos.add(model);
    String key = s == "normal" ? PROFILE_PHOTOS : HOOKUP_PHOTOS;
    userModel
      ..put(key, photos.map((e) => e.items).toList())
      ..updateItems();
    if (mounted) setState(() {});
    //return;
    uploadFile(File(model.imageUrl), (res, err) {
      if (null != err) return;
      final photos = userModel.profilePhotos;

      int pos = photos.indexWhere((e) => e.getObjectId() == id);
      if (pos != -1) {
        model.put(IMAGE_URL, res);
        photos[pos] = model;
        userModel
          ..put(key, photos.map((e) => e.items).toList())
          ..updateItems();
      }

      if (mounted) setState(() {});
    });
  }

  void pickAssets(String s) async {
//    final photos = s == "normal" ? profilePhotos : hookUpPhotos;
    PhotoPicker.pickAsset(
            thumbSize: 250,
            context: context,
            provider: I18nProvider.english,
            pickType: PickType.onlyImage,
            rowCount: 3)
        .then((value) async {
      if (value == null) return;
      for (var a in value) {
        String path = (await a.originFile).path;
        bool isVideo = a.type == AssetType.video;
        BaseModel model = BaseModel();
        model.put(OBJECT_ID, a.id);
        model.put(IMAGE_URL, path);
        model.put(IS_VIDEO, isVideo);
        if (isVideo) {
          File file = File(path);
          String fileSize = filesize(file.lengthSync())
              .replaceAll("MB", "")
              .replaceAll("KB", "")
              .replaceAll("TB", "");
          bool hasMB = filesize(file.lengthSync()).contains("MB");
          bool tooLarge = num.parse(fileSize).toInt() > 15;
          if (isVideo && hasMB && tooLarge) {
            showMessage(context, Icons.error, red0, "Opps Sorry!",
                "You cannot upload videos larger than 15MB. Try selecting another video file",
                textSize: 14);
            continue;
          }

          model.put(THUMBNAIL_URL,
              (await VideoCompress().getThumbnailWithFile(path)).path);
        }
//        int p = photos.indexWhere((e) => e.getObjectId() == a.id);
//        if (p != -1) {
//          photos[p] = model;
//        } else {
//          photos.add(model);
//        }
        uploadPhotos(model, s, a.id);
      }

      setState(() {});
    }).catchError((e) {});

    /// Use assetList to do something.
  }
}

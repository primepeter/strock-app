import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'ShowProfile.dart';

//0 -- my matches
//1 -- People i viewed
//2 -- Recommended Matches
//3 -- SuperLikes

class PeopleItems extends StatefulWidget {
  final int type;
  const PeopleItems(this.type);
  @override
  _PeopleItemsState createState() => _PeopleItemsState();
}

class _PeopleItemsState extends State<PeopleItems> {
  @override
  void initState() {
    super.initState();
    overlayController.add(false);
  }

  List<BaseModel> get listType {
    int type = widget.type;
    if (type == 0) return matchesList;
    if (type == 1) return seenList;
    if (type == 2) return recommendedMatches;
    if (type == 3) return superLikesList;
    return seenByList;
  }

  bool get setupType {
    int type = widget.type;
    if (type == 0) return matchesSetup;
    if (type == 1) return seenSetup;
    if (type == 2) return recommendedSetup;
    if (type == 3) return superSetup;
    return seenBySetup;
  }

  @override
  Widget build(BuildContext context) {
    return body();
  }

  body() {
    print(setupType);

    if (!setupType) return loadingLayout();

    if (listType.isEmpty) return emptyLayout(Icons.person, "No Views Yet", "");

    return Container(
        child: GridView.builder(
      itemBuilder: (c, p) {
        return personItem(p);
      },
      shrinkWrap: true,
      itemCount: listType.length,
      padding: EdgeInsets.only(top: 10, right: 5, left: 5, bottom: 40),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.8,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5),
    ));
  }

  personItem(int p) {
    BaseModel model = listType[p];

    bool isVideo = model.profilePhotos[0].isVideo;
    String imageUrl =
        model.profilePhotos[0].getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);

    return GestureDetector(
      onTap: () {
        pushAndResult(
            context,
            ShowProfile(
              theUser: model,
              //fromMeetMe: widget.fromStrock,
            ));
      },
      child: Card(
        color: white,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Stack(
                  children: [
                    CachedNetworkImage(
                      imageUrl: imageUrl,
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: double.infinity,
                      placeholder: (c, s) {
                        return Container(
                            height: double.infinity,
                            width: double.infinity,
                            child: Center(
                                child: Icon(
                              Icons.person,
                              color: white,
                              size: 15,
                            )),
                            decoration: BoxDecoration(
                              color: black.withOpacity(.09),
                              //shape: BoxShape.circle
                            ));
                      },
                    ),
                    if (isVideo)
                      Center(
                        child: Container(
                          height: 50,
                          width: 50,
                          child: Icon(
                            Icons.play_arrow,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.8),
                              border:
                                  Border.all(color: Colors.white, width: 1.5),
                              shape: BoxShape.circle),
                        ),
                      ),
                  ],
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Flexible(
                          fit: FlexFit.loose,
                          child: Text(
                            getFirstName(model),
                            style: textStyle(true, 14, black),
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        addSpaceWidth(5),
                        if (isOnline(model))
                          Container(
                            height: 8,
                            width: 8,
                            decoration: BoxDecoration(
                                color: green, shape: BoxShape.circle),
                          ),
                        Text(
                          getMyAge(model).toString(),
                          style: textStyle(false, 11, black.withOpacity(.5)),
                        ),
                      ],
                    ),
//                    FlatButton(
//                      onPressed: () {
//                        clickChat(context, model, false);
//                      },
//                      padding: EdgeInsets.all(5),
//                      shape: RoundedRectangleBorder(
//                          side: BorderSide(color: black.withOpacity(.2))),
//                      child: Center(
//                        child: Text(
//                          "Chat Now",
//                          style: textStyle(false, 12, black.withOpacity(.7)),
//                        ),
//                      ),
//                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'PeopleItems.dart';

class ShowRecommendations extends StatefulWidget {
  @override
  _ShowRecommendationsState createState() => _ShowRecommendationsState();
}

class _ShowRecommendationsState extends State<ShowRecommendations> {
  final pc = PageController();
  int currentPage = 0;

  @override
  void initState() {
    super.initState();
    overlayController.add(false);
    loadSuperLikes();
    loadRecommended();
  }

  loadRecommended({bool reset = false}) async {
    QuerySnapshot shots1 = await Firestore.instance
        .collection(USER_BASE)
        .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
        .where(RELATIONSHIP, isEqualTo: userModel.getInt(RELATIONSHIP))
        .getDocuments();

    QuerySnapshot shots2 = await Firestore.instance
        .collection(USER_BASE)
        .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
        .where(ETHNICITY, isEqualTo: userModel.getInt(ETHNICITY))
        .getDocuments();

    List allShots = [];
    allShots.addAll(shots1.documents);
    allShots.addAll(shots2.documents);

    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);

    for (DocumentSnapshot doc in allShots) {
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (!model.signUpCompleted) continue;
      if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;

      if (useLimit && model.getMap(POSITION).isEmpty) continue;
      if (model.getMap(POSITION).isNotEmpty) {
        final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
        final distance = (await calculateDistanceTo(geoPoint) / 1000);
        if (useLimit && (distance > limit)) {
          String email = model.getEmail();
          print("Distance $distance limit $limit email $email");

          continue;
        }
        model.put(DISTANCE, distance);
      }

      int index = recommendedMatches
          .indexWhere((bm) => model.getObjectId() == bm.getObjectId());
      if (index == -1) {
        recommendedMatches.add(model);
      }
    }
    recommendedSetup = true;
    if (mounted) setState(() {});
  }

  loadSuperLikes() async {
    Firestore.instance
        .collection(USER_BASE)
        .where(SUPER_LIKE_LIST, arrayContains: userModel.getObjectId())
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(SUPER_LIKE_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        //if (!userModel.getList(SUPER_LIKE_LIST).contains(model.getObjectId()))continue;
        int index = superLikesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          superLikesList.add(model);
        } else {
          superLikesList[index] = model;
        }
      }
      superSetup = true;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    print(superLikesList.length);
    return Scaffold(
      backgroundColor: default_white,
      body: page(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
          color: white,
          child: Column(
            children: [
              Row(
                children: [
                  InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        child: Center(
                            child: Icon(
                          Icons.keyboard_backspace,
                          color: black,
                          size: 25,
                        )),
                      )),
                  Flexible(
                    child: Container(
                      //height: 50,
//                  width: 270,
                      margin: EdgeInsets.fromLTRB(0, 0, 50, 0),
                      child: Card(
                        color: black.withOpacity(0.2),
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        child: Row(
                          children: List.generate(2, (p) {
                            String title =
                                p == 0 ? "Recommended" : "Super Likes";
                            bool selected = p == currentPage;
                            return Flexible(
                              child: GestureDetector(
                                onTap: () {
                                  if (p == 1 && !userModel.isPremium) {
                                    showMessage(
                                        context,
                                        Icons.error,
                                        red0,
                                        "Opps Sorry!",
                                        "You cannot view persons who SuperLiked your profile until you become a Premium User",
                                        textSize: 14,
                                        clickYesText: "Subscribe",
                                        onClicked: (_) {
                                      if (_) {
                                        openSubscriptionPage(context);
                                      }
                                    }, clickNoText: "Cancel");

                                    return;
                                  }

                                  pc.animateToPage(p,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.ease);
                                },
                                child: Container(
                                    margin: EdgeInsets.all(4),
                                    height: 30,
                                    decoration: !selected
                                        ? null
                                        : BoxDecoration(
                                            color:
                                                selected ? white : transparent,
//                      border: Border.all(color: black.withOpacity(.1),width: 3),
                                            borderRadius:
                                                BorderRadius.circular(25)),
                                    child: Center(
                                        child: Text(
                                      title,
                                      style: textStyle(
                                          true,
                                          14,
                                          selected
                                              ? black
                                              : (white.withOpacity(.7))),
                                      textAlign: TextAlign.center,
                                    ))),
                              ),
                              fit: FlexFit.tight,
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Expanded(
            flex: 1,
            child: PageView(
              controller: pc,
              onPageChanged: (p) {
                if (p == 1 && !userModel.isPremium) {
                  showMessage(context, Icons.error, red0, "Opps Sorry!",
                      "You cannot view persons who SuperLiked your profile until you become a Premium User",
                      textSize: 14, clickYesText: "Subscribe", onClicked: (_) {
                    if (_) {
                      openSubscriptionPage(context);
                    }
                  }, clickNoText: "Cancel");

                  return;
                }
                setState(() {
                  currentPage = p;
                });
              },
              physics:
                  userModel.isPremium ? null : NeverScrollableScrollPhysics(),
              children: [PeopleItems(2), PeopleItems(3)],
            ))
      ],
    );
  }
}

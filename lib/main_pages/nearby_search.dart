import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/FilterDialog.dart';
import 'package:Strokes/ad_manager.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_native_admob/flutter_native_admob.dart';
import 'package:flutter_native_admob/native_admob_controller.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:geoflutterfire/geoflutterfire.dart';

import 'MyProfile.dart';
import 'ShowMatches.dart';
import 'ShowProfile.dart';
import 'chat_page.dart';

BuildContext baseContext;

class NearbySearch extends StatefulWidget {
  final bool fromStrock;
  final int currentPage;

  const NearbySearch({Key key, this.fromStrock = false, this.currentPage = 0})
      : super(key: key);
  @override
  _NearbySearchState createState() => _NearbySearchState();
}

class _NearbySearchState extends State<NearbySearch> {
  bool setup = false;
  List peopleList = [];
  BaseModel filterLocation;
  int genderType = -1;
  int onlineType = -1;
  int interestType = -1;
  int minAge = 18;
  int maxAge = 80;
  bool filtering = false;

  PageController pc;
  int currentPage = 0;

  @override
  void initState() {
    super.initState();
    currentPage = widget.currentPage;
    pc = PageController(initialPage: widget.currentPage);

    loadItems();
  }

  var loadingSub;
  loadItems() async {
    /*if (loadingSub != null) {
      loadingSub.cancel();
      peopleList.clear();
      setup = false;
      setState(() {});
    }*/

    //loadingSub.cancel();
    peopleList.clear();
    setup = false;
    setState(() {});

    Geoflutterfire geo = Geoflutterfire();
    Map myPosition = userModel.getMap(POSITION);
    if (myPosition.isEmpty) {
      showMessage(context, Icons.error, red0, "No Location",
          "Sorry we could no find your location",
          onClicked: (_) => Navigator.pop(context));
      return;
    }
    GeoPoint geoPoint = myPosition["geopoint"];
    double lat = geoPoint.latitude;
    double lon = geoPoint.longitude;
    if (filterLocation != null) {
      lat = filterLocation.getDouble(LATITUDE);
      lon = filterLocation.getDouble(LONGITUDE);
    }
    GeoFirePoint center = geo.point(latitude: lat, longitude: lon);
    final collectionReference = Firestore.instance.collection(USER_BASE);

//    double radius = 100;
    double radius = appSettingsModel.getDouble(NEARBY_RADIUS);
    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);
    bool useNearby = appSettingsModel.getBoolean(USE_NEARBY_LIMIT);

    /*loadingSub = */ geo
        .collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: POSITION)
        .first
        .then((event) async {
      for (DocumentSnapshot doc in event) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (!model.signUpCompleted) continue;
        if (widget.fromStrock && model.hookUpPhotos.isEmpty) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (genderType != -1) {
          if ((genderType == 0 || genderType == 1) &&
              model.getInt(GENDER) != genderType) continue;
        } else {
          if (model.getInt(GENDER) != userModel.getInt(PREFERENCE)) continue;
        }
        if (model.getInt(ETHNICITY) != userModel.getInt(ETHNICITY)) continue;
        if (onlineType > 0) {
          if (onlineType == 1) if (!isOnline(model)) continue;
          int now = DateTime.now().millisecondsSinceEpoch;
          if (onlineType == 2) if ((now - (model.getInt(TIME))) >
              Duration.millisecondsPerSecond) continue;
        }

        if (minAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (minAge > age) continue;
        }
        if (maxAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (maxAge < age) continue;
        }
        if (interestType != -1) {
          if (model.getInt(RELATIONSHIP) != interestType) continue;
        }

        if (useLimit && model.getMap(POSITION).isEmpty) continue;
        if (model.getMap(POSITION).isNotEmpty) {
          final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
          final distance = (await calculateDistanceTo(geoPoint) / 1000);
          if (!filtering && useLimit && useNearby && (distance > limit))
            continue;
          print("Distance $distance limit $limit");
          model.put(DISTANCE, distance);
        }

        int index = peopleList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) peopleList.add(model);
      }
      //loadingSub.cancel();

      // List<BaseModel> newList = [];
      for (int p = 0; p < peopleList.length; p++) {
        if (p % 10 != 0) continue;
        BaseModel model = BaseModel();
        model.put(IS_ADS, true);
        peopleList.insert(p, model);
      }

      setup = true;
      if (mounted) setState(() {});
    });
  }

  final _nativeAdController = NativeAdmobController();

  Widget _getAdContainer() {
    return Container(
      height: 150,
      width: double.infinity,
      child: NativeAdmob(
        // Your ad unit id
        //adUnitID: appSettingsModel.getString('adUnitID'),
        adUnitID: AdManager.nativeAdUnitId,
        controller: _nativeAdController,
        type: NativeAdmobType.banner,
      ),
    );
  }

  @override
  void dispose() {
    //  for (var sub in loadingSub) {
    //     sub?.cancel();
    //   }
    _nativeAdController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  String get headerTitle {
    if (currentPage == 0)
      return widget.fromStrock ? "Quick Strock" : "Nearby Search";
    if (currentPage == 1) return "Matches";
    if (currentPage == 2) return "Chat";
    return "Profile";
  }

  page() {
    return Column(
      children: [
        addSpace(30),
        pages(),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(4, (p) {
              if (p == 0) {
                return btmTab(p, Feather.home, "Explore", isAsset: false);
              }
              if (p == 1) {
                return btmTab(p, Feather.heart, "Matches", isAsset: false);
              }
              if (p == 2) {
                return btmTab(p, Feather.message_circle, "Chat",
                    isAsset: false);
              }
              return btmTab(p, Feather.user, "Profile", isAsset: false);
            }))
      ],
    );
  }

  pages() {
    return Expanded(
      child: PageView(
        controller: pc,
        onPageChanged: (p) {
          setState(() {
            currentPage = p;
          });
        },
        children: [
          page1(),
          ShowMatches(
            showBar: false,
          ),
          MessagesPage(
            showBar: false,
          ),
          MyProfile(
            showBar: false,
          )
        ],
      ),
    );
  }

  page1() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
                child: Row(
              children: [
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Flexible(
                  child: Text(
                    headerTitle,
                    style: textStyle(true, 25, black),
                  ),
                ),
              ],
            )),
            IconButton(
                icon: Icon(Icons.sort),
                onPressed: () {
                  pushAndResult(
                      context,
                      FilterDialog(
                          filtering,
                          filterLocation,
                          genderType,
                          onlineType,
                          minAge,
                          maxAge,
                          interestType), result: (_) {
                    List items = _;
                    filterLocation = items[0];
                    genderType = items[1];
                    onlineType = items[2];
                    minAge = items[3];
                    maxAge = items[4];
                    interestType = items[5];
                    filtering = items[6];
                    loadItems();
                  }, depend: false);
                })
          ],
        ),
//

        Flexible(
          child: Builder(builder: (ctx) {
            if (!setup) return loadingLayout();

            if (peopleList.isEmpty)
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
//                      Image.asset(
//                        "assets/icons/gender.png",
//                        width: 50,
//                        height: 50,
//                        color: AppConfig.appColor,
//                      ),
                      Icon(
                        Icons.location_off,
                        color: AppConfig.appColor,
                        size: 50,
                      ),
                      Text(
                        "No One Nearby",
                        style: textStyle(true, 20, black),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              );

            return StaggeredGridView.countBuilder(
              crossAxisCount: 3,
              itemCount: peopleList.length,
              itemBuilder: (BuildContext context, int p) {
                BaseModel model = peopleList[p];

                if (model.getBoolean(IS_ADS)) return _getAdContainer();

                bool isVideo = false;
                int defPos = model.getInt(
                    widget.fromStrock ? DEF_STROCK_PHOTO : DEF_PROFILE_PHOTO);
                isVideo = model.profilePhotos[defPos].isVideo;
                String image = model.profilePhotos[defPos]
                    .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);

                if (widget.fromStrock) {
                  image = model.hookUpPhotos[defPos].imageUrl;
                  isVideo = model.hookUpPhotos[defPos].isVideo;
                  image = model.hookUpPhotos[defPos]
                      .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                }

                bool addMargin = p == 1 || (p % 3 == 0);

                return Container(
                  // margin: EdgeInsets.only(top: p == 1 ? 50 : 0),
                  //margin: EdgeInsets.only(top: addMargin ? 50 : 0),
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: Stack(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          GestureDetector(
                            onTap: () {
                              //clickChat(context, model, false);
                              pushAndResult(
                                  context,
                                  ShowProfile(
                                    theUser: model,
                                    fromMeetMe: widget.fromStrock,
                                  ));
                            },
                            child: Container(
                              height: 110,
                              //width: double.infinity,
                              //alignment: Alignment.center,
                              child: Stack(
                                children: [
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Stack(
                                        children: [
                                          CachedNetworkImage(
                                            imageUrl: image,
                                            width: 100,
                                            height: 100,
                                            fit: BoxFit.cover,
                                            placeholder: (c, s) {
                                              return Container(
                                                  width: 100,
                                                  height: 100,
                                                  child: Center(
                                                      child: Icon(
                                                    Icons.person,
                                                    color: white,
                                                    size: 15,
                                                  )),
                                                  decoration: BoxDecoration(
                                                    color:
                                                        black.withOpacity(.09),
                                                    //shape: BoxShape.circle
                                                  ));
                                            },
                                          ),
                                          if (isOnline(model))
                                            Container(
                                              height: 15,
                                              width: 15,
                                              padding: EdgeInsets.all(2),
                                              // child: Text(
                                              //   "Online",
                                              //   style:
                                              //       textStyle(false, 10, white),
                                              // ),
                                              decoration: BoxDecoration(
                                                color: green,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(10),
                                                  bottomRight:
                                                      Radius.circular(10),
                                                ),

                                                //shape: BoxShape.circle
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                        //alignment: Alignment.bottomCenter,
                                        padding:
                                            EdgeInsets.fromLTRB(5, 3, 5, 3),
                                        decoration: BoxDecoration(
                                            color: AppConfig.appColor,
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            border: Border.all(
                                                color: white, width: 2)),
                                        child: Text(
                                          "${(model.getDouble(DISTANCE)).roundToDouble()} KM",
                                          style: textStyle(false, 12, white),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Text(
                            model.getString(NAME),
                            style: textStyle(true, 12, black),
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                      if (isVideo)
                        Center(
                          child: Container(
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.play_arrow,
                              color: Colors.white,
                              size: 14,
                            ),
                            decoration: BoxDecoration(
                                color: Colors.black.withOpacity(0.8),
                                border:
                                    Border.all(color: Colors.white, width: 1.5),
                                shape: BoxShape.circle),
                          ),
                        ),
                    ],
                  ),
                );
              },
              padding: EdgeInsets.all(0),
              staggeredTileBuilder: (int p) {
                BaseModel model = peopleList[p];
                //if (model.getBoolean(IS_ADS)) return StaggeredTile.count(1, 1);
                //return StaggeredTile.count(3, model.getBoolean(IS_ADS) ? 1 : 3);
                return new StaggeredTile.extent(
                    model.getBoolean(IS_ADS) ? 3 : 1, 160);
                return new StaggeredTile.extent(
                    model.getBoolean(IS_ADS) ? 3 : 1,
                    model.getBoolean(IS_ADS)
                        ? 150
                        : (p == 1)
                            ? 180
                            : 130);
              },
              shrinkWrap: true,
              mainAxisSpacing: 4.0,
              crossAxisSpacing: 4.0,
            );
          }),
        ),
      ],
    );
  }

  btmTab(int p, icon, String title, {bool isAsset = false}) {
    bool active = currentPage == p;
    final width = MediaQuery.of(context).size.width / 4;
    return Flexible(
      child: GestureDetector(
          onTap: () {
            setState(() {
              pc.jumpToPage(p);
            });
          },
          child: Container(
            width: width,
            height: 64,
            alignment: Alignment.center,
            color: transparent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Builder(
                  builder: (ctx) {
                    if (isAsset)
                      return Image.asset(
                        icon,
                        height: 20,
                        width: 20,
                        color: black.withOpacity(active ? 1 : 0.5),
                      );
                    return Icon(
                      icon,
                      size: 20,
                      color: black.withOpacity(active ? 1 : 0.5),
                    );
                  },
                ),
                Text(
                  title,
                  style: textStyle(active, active ? 13 : 12,
                      black.withOpacity(active ? 1 : 0.5)),
                )
              ],
            ),
          )),
    );
  }
}

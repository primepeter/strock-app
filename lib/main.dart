import 'dart:io';

import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/preinit.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  if (Platform.isIOS) InAppPurchaseConnection.enablePendingPurchases();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext c) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Strock",
        color: white,
        theme: ThemeData(
          fontFamily: 'Averta',
          accentColor: Colors.orange,
          primaryColor: Colors.orange,
          pageTransitionsTheme: PageTransitionsTheme(
            builders: <TargetPlatform, PageTransitionsBuilder>{
              TargetPlatform.iOS: createTransition(),
              TargetPlatform.android: createTransition(),
            },
          ),
        ),
        navigatorObservers: [],
        home: MainHome());
  }

  PageTransitionsBuilder createTransition() {
    return ZoomPageTransitionsBuilder();
  }
}

class MainHome extends StatefulWidget {
  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  bool showNoInternet = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      loadSettings();
      checkUser();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        if (showNoInternet)
          emptyLayout(Icons.network_check_outlined, 'No Internet',
              "Could not establish a connection to the internet. Kindly check your internet and retry again.",
              clickText: 'Retry', click: () {
            showNoInternet = false;
            checkUser();
            loadSettings();
            setState(() {});
          })
        else
          loadingLayout()
      ],
    );
  }

  checkUser() async {
    User user = FirebaseAuth.instance.currentUser;
    if (user == null) {
      popUpUntil(context, PreInit());
    } else {
      loadLocalUser(user.uid, onInComplete: () {
        popUpUntil(context, PreInit());
      }, onLoaded: () {
        popUpUntil(context, MainAdmin());
        return;
      });
    }
  }

  loadSettings() {
    FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .get(/*source: Source.cache*/)
        .then((doc) {
      if (!doc.exists) {
        appSettingsModel = new BaseModel();
        appSettingsModel.saveItem(APP_SETTINGS_BASE, false,
            document: APP_SETTINGS);
        return;
      }
      appSettingsModel = BaseModel(doc: doc);
    });
  }

  loadLocalUser(String userId, {onLoaded, onInComplete}) {
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(userId)
//        .document("PhMActPOcKUrNuX4HfiRrMAhCd93")
        .get(GetOptions(source: Source.server))
        .then((doc) async {
      userModel = BaseModel(doc: doc);
      isAdmin = userModel.getBoolean(IS_ADMIN) ||
          userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
          userModel.getString(EMAIL) == "ammaugost@gmail.com";
      if (!userModel.signUpCompleted || !doc.exists) {
        await GoogleSignIn().signOut();
        await FacebookLogin().logOut();
        await FirebaseAuth.instance.signOut();
//        if(!doc.exists)userModel.deleteItem();
        userModel = BaseModel();
        onInComplete();
        return;
      }
      onLoaded();
    }).catchError((e) {
      print(e);

      if (e.toString().contains('The service is currently unavailable')) {
        showNoInternet = true;
        setState(() {});
        return;
      }
      //showMessage(context, Icons.error, red, "No Internet", 'Could not establish a connection to the internet. Kindly check your internet and retry again.',);
      popUpUntil(context, PreInit());
    });
  }

  platformScreen() {}
}

import 'dart:async';
import 'dart:io';
import 'dart:io' as io;
import 'dart:math';
import 'dart:ui';

import 'package:Strokes/ad_manager.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/main_pages/ShowProfile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_admob/flutter_native_admob.dart';
import 'package:flutter_native_admob/native_admob_controller.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:location/location.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synchronized/synchronized.dart';

import 'AppEngine.dart';
import 'ChatMain.dart';
import 'NewUpdate.dart';
import 'ReportMain.dart';
import 'app_config.dart';
import 'its_a_match.dart';
import 'main_pages/ShowProfileViews.dart';
import 'main_pages/ShowRecommendations.dart';
import 'main_pages/chat_page.dart';
import 'main_pages/nearby_search.dart';
import 'tinder_cards/meetMe.dart';

Map<String, List> unreadCounter = Map();
Map otherPeronInfo = Map();
List<BaseModel> allStoryList = new List();
final firebaseMessaging = FirebaseMessaging();
final chatMessageController = StreamController<bool>.broadcast();
final homeRefreshController = StreamController<bool>.broadcast();
final uploadingController = StreamController<String>.broadcast();
final pageSubController = StreamController<int>.broadcast();
final overlayController = StreamController<bool>.broadcast();
final matchedController = StreamController<BaseModel>.broadcast();
final subscriptionController = StreamController<bool>.broadcast();
final adsController = StreamController<bool>.broadcast();

const bool kAutoConsume = true;
final connection = InAppPurchaseConnection.instance;

List<String> _notFoundIds = [];
List<ProductDetails> availablePackages = [];
List<PurchaseDetails> _purchases = [];

bool packagesAvailable = false;
bool _purchasePending = false;
bool _loading = true;
String _queryProductError;

List connectCount = [];
List<String> stopListening = List();
List<BaseModel> lastMessages = List();
bool chatSetup = false;
List showNewMessageDot = [];
bool showNewNotifyDot = false;
List newStoryIds = [];
String visibleChatId;
bool itemsLoaded = false;
List hookupList = [];
bool strockSetup = false;

List<BaseModel> recommendedMatches = [];
bool recommendedSetup = false;

Location location = new Location();
GeoFirePoint myLocation;

bool serviceEnabled = false;
PermissionStatus permissionGranted;
LocationData locationData;

List<BaseModel> adsList = [];
bool adsSetup = false;

List<BaseModel> likesList = [];
bool likesSetup = false;
List<BaseModel> superLikesList = [];
bool superSetup = false;
List<BaseModel> matchesList = [];
bool matchesSetup = false;
List<BaseModel> seenList = [];
bool seenSetup = false;
List<BaseModel> seenByList = [];
bool seenBySetup = false;

//var notificationsPlugin = FlutterLocalNotificationsPlugin();

Future<dynamic> myBackgroundMessageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('on background ${message.data}');
}

class MainAdmin extends StatefulWidget {
  @override
  _MainAdminState createState() => _MainAdminState();
}

class _MainAdminState extends State<MainAdmin>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  PageController peoplePageController = PageController();
  List<StreamSubscription> subs = List();
  int timeOnline = 0;
  String noInternetText = "";

  String flashText = "";
  bool setup = false;
  bool settingsLoaded = false;
  String uploadingText;
  int peopleCurrentPage = 0;
  bool tipHandled = false;
  bool tipShown = true;

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    for (var sub in subs) {
      sub.cancel();
    }
    strockImagesTimer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    itemsLoaded = false;
    WidgetsBinding.instance.addObserver(this);
    Future.delayed(Duration(seconds: 1), () {
      createUserListener();
    });
    var subSub = subscriptionController.stream.listen((b) {
      if (!b) return;
      showMessage(context, Icons.check, green, "Congratulations!",
          "You are now a Premium User. Enjoy the benefits!");
    });
    subs.add(subSub);
    var pageSub = pageSubController.stream.listen((int p) {
      setState(() {
        peopleCurrentPage = p;
      });
    });
    subs.add(pageSub);
    var uploadingSub = uploadingController.stream.listen((text) {
      setState(() {
        uploadingText = text;
      });
    });
    subs.add(uploadingSub);

    var matchedSub = matchedController.stream.listen((user) {
      userModel
        ..putInList(MATCHED_LIST, user.getUserId(), true)
        ..updateItems();
      Future.delayed(Duration(milliseconds: 15), () {
        pushAndResult(
            context,
            ItsAMatch(
              user: user,
            ),
            depend: false);
      });
    });
    subs.add(matchedSub);
  }

  checkTip() {
    if (!tipHandled &&
        peopleCurrentPage != hookupList.length - 1 &&
        hookupList.length > 1) {
      tipHandled = true;
      Future.delayed(Duration(seconds: 1), () async {
        SharedPreferences pref = await SharedPreferences.getInstance();
        tipShown = pref.getBool("swipe_tipx") ?? false;
        if (!tipShown) {
          pref.setBool("swipe_tipx", true);
          setState(() {});
        }
      });
    }
  }

  okLayout(bool manually) {
    checkTip();
    itemsLoaded = true;
    if (mounted) setState(() {});
    Future.delayed(Duration(milliseconds: 1000), () {
//      if(manually)pageScrollController.add(-1);
      if (manually) peoplePageController.jumpToPage(peopleCurrentPage);
    });
  }

  Future<int> getSeenCount(String id) async {
    var pref = await SharedPreferences.getInstance();
    List<String> list = pref.getStringList(SHOWN) ?? [];
    int index = list.indexWhere((s) => s.contains(id));
    if (index != -1) {
      String item = list[index];
//      print(item);
      var parts = item.split(SPACE);
      int seenCount = int.parse(parts[1].trim());
      return seenCount;
    }
    return 0;
  }

  void createUserListener() async {
    User user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      var userSub = FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(user.uid)
          .snapshots()
          .listen((shot) async {
        if (shot != null) {
          if (user == null) return;

          userModel = BaseModel(doc: shot);
          if (userModel.getBoolean(DELETED)) {
            shot.reference.delete();
            userModel = BaseModel();
            return;
          }

          isAdmin = userModel.getBoolean(IS_ADMIN) ||
              userModel.getString(EMAIL) == "amqueenester@gmail.com" ||
              userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
              userModel.getString(EMAIL) == "ammaugost@gmail.com";
          loadBlocked();
          if (userModel.getInt(DEF_PROFILE_PHOTO).isNegative) {
            userModel
              ..put(DEF_PROFILE_PHOTO, 0)
              ..updateItems(delaySeconds: 2);
          }

          if (userModel.getInt(DEF_STROCK_PHOTO).isNegative) {
            userModel
              ..put(DEF_STROCK_PHOTO, 0)
              ..updateItems(delaySeconds: 2);
          }

          if (!userModel.getBoolean(FIX_APPLIED)) {
            userModel
              ..put(VIEWED_LIST, [])
              ..put(FIX_APPLIED, true)
              ..updateItems(delaySeconds: 2);
          }

          if (!settingsLoaded) {
            settingsLoaded = true;
            loadSettings();
            loadReset();
          }
        }
      });
      subs.add(userSub);
    }
  }

  loadSettings() async {
    var settingsSub = Firestore.instance
        .collection(APP_SETTINGS_BASE)
        .document(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        chkUpdate();
        List banned = appSettingsModel.getList(BANNED);
        List deletedAcct = appSettingsModel.getList(DELETED_ACCOUNTS);

        if (banned.contains(userModel.getObjectId()) ||
            banned.contains(userModel.getString(DEVICE_ID)) ||
            banned.contains(userModel.getEmail())) {
          io.exit(0);
        }

        if (deletedAcct.contains(userModel.getObjectId())) {
          print("Deleting account...");
          Future.delayed(Duration(milliseconds: 1000), () {
            deleteAccount(context);
          });
          return;
        }

        String genMessage = appSettingsModel.getString(GEN_MESSAGE);
        int genMessageTime = appSettingsModel.getInt(GEN_MESSAGE_TIME);

        if (userModel.getInt(GEN_MESSAGE_TIME) != genMessageTime &&
            genMessageTime > userModel.getTime()) {
          userModel.put(GEN_MESSAGE_TIME, genMessageTime);
          userModel.updateItems();

          String title = !genMessage.contains("+")
              ? "Announcement!"
              : genMessage.split("+")[0].trim();
          String message = !genMessage.contains("+")
              ? genMessage
              : genMessage.split("+")[1].trim();
          showMessage(context, Icons.info, blue0, title, message);
        }

        if (!setup) {
          setup = true;
          blockedIds.addAll(userModel.getList(BLOCKED));
          onResume();
          //loadItems();
          loadNotification();
          loadMessages();
          loadConnects();
          setupPush();

          loadBlocked();
          loadStory();
          updatePackage();
          setUpLocation();
          checkPhotos();
          //loadQuickStrock();
          //loadStrockTimer();
          loadRecommended();
          //loadAds();
          //loadStoreInfo();
          loadSeenBy();
          loadMatches();
          loadLikes();
          loadSuperLikes();
          loadMyViews();
          _initAdMob();
        }
      }
    });
    subs.add(settingsSub);
  }

  loadReset() {
    final liveMode = appSettingsModel.getBoolean(STRIPE_IS_LIVE);
    if (!isAdmin && userModel.subscriptionExpired)
      userModel
        ..put(ACCOUNT_TYPE, 0)
        ..updateItems();
    print("UserModel ${userModel.subscriptionExpired}");
    return;
  }

  loadSeenBy() async {
    //load persons i have seen there profile
    Firestore.instance
        .collection(USER_BASE)
        .where(VIEWED_LIST, arrayContains: userModel.getUserId())
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(VIEWED_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        int index = seenByList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          seenByList.add(model);
        } else {
          seenByList[index] = model;
        }
      }
      seenBySetup = true;
      setState(() {});
    });
  }

  loadMatches() async {
    //load persons i have been matched with
    //load my matches
    Firestore.instance
        .collection(USER_BASE)
        .where(MATCHED_LIST, arrayContains: userModel.getObjectId())
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(MATCHED_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
//        if (!userModel.getList(MATCHED_LIST).contains(model.getObjectId())) continue;
        int index = matchesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          matchesList.add(model);
        } else {
          matchesList[index] = model;
        }
      }
      matchesSetup = true;
      setState(() {});
    });
  }

  loadSuperLikes() async {
    Firestore.instance
        .collection(USER_BASE)
        .where(SUPER_LIKE_LIST, arrayContains: userModel.getObjectId())
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(SUPER_LIKE_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        //if (!userModel.getList(SUPER_LIKE_LIST).contains(model.getObjectId()))continue;
        int index = superLikesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          superLikesList.add(model);
        } else {
          superLikesList[index] = model;
        }
      }
      superSetup = true;
      setState(() {});
    });
  }

  loadLikes() async {
    Firestore.instance
        .collection(USER_BASE)
        .where(LIKE_LIST, arrayContains: userModel.getObjectId())
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(LIKE_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        //if (!userModel.getList(LIKE_LIST).contains(model.getObjectId()))continue;
        int index = likesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          likesList.add(model);
        } else {
          likesList[index] = model;
        }
      }
      likesSetup = true;
      setState(() {});
    });
  }

  loadMyViews() async {
    final list = userModel.getList(VIEWED_LIST);
    for (var docId in list) {
      final doc =
          await Firestore.instance.collection(USER_BASE).document(docId).get();
      if (!doc.exists) continue;
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
      if (!model.signUpCompleted) {
//        userModel
//          ..putInList(VIEWED_LIST, model.getUserId(), false)
//          ..updateItems();
        continue;
      }
      int index =
          seenList.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
      if (index == -1) {
        seenList.add(model);
      } else {
        seenList[index] = model;
      }
    }
    seenSetup = true;
    setState(() {});
  }

  loadStoreInfo() async {
    final bool isAvailable = await connection.isAvailable();
    print("ava $isAvailable");
    if (!isAvailable) {
      setState(() {
        packagesAvailable = isAvailable;
        availablePackages = [];
        _purchases = [];
        _notFoundIds = [];
//        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    final productIds =
        Set<String>.from(appSettingsModel.getList(PURCHASE_PRODUCT_IDS))
            .toSet();

    //return;

    ProductDetailsResponse productDetailResponse =
        await connection.queryProductDetails(productIds);
    if (productDetailResponse.error != null) {
      setState(() {
        _queryProductError = productDetailResponse.error.message;
        packagesAvailable = isAvailable;
        availablePackages = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
//        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    if (productDetailResponse.productDetails.isEmpty) {
      setState(() {
        _queryProductError = null;
        packagesAvailable = isAvailable;
        availablePackages = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
//        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    final QueryPurchaseDetailsResponse purchaseResponse =
        await connection.queryPastPurchases();
    if (purchaseResponse.error != null) {
      // handle query past purchase error..
    }
    final List<PurchaseDetails> verifiedPurchases = [];
    for (PurchaseDetails purchase in purchaseResponse.pastPurchases) {
      if (await _verifyPurchase(purchase)) {
        verifiedPurchases.add(purchase);
      }
    }
//    List<String> consumables = await ConsumableStore.load();
    setState(() {
      packagesAvailable = isAvailable;
      availablePackages = productDetailResponse.productDetails;
      _purchases = verifiedPurchases;
      _notFoundIds = productDetailResponse.notFoundIDs;
//      _consumables = consumables;
      _purchasePending = false;
      _loading = false;
    });
  }

  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) {
    // IMPORTANT!! Always verify a purchase before delivering the product.
    // For the purpose of an example, we directly return true.
    return Future<bool>.value(true);
  }

  //static const _adUnitID = "ca-app-pub-3940256099942544/8135179316";
  final _nativeAdController = NativeAdmobController();

  Future<void> _initAdMob() {
    // TODO: Initialize AdMob SDK
    //FirebaseAdMob.instance.initialize(appId: FirebaseAdMob.testAppId);

    FirebaseAdMob.instance.initialize(appId: AdManager.appId);
    _bannerAd = createBannerAd()..load();
    RewardedVideoAd.instance.listener =
        (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
      print("RewardedVideoAd event $event");
      if (event == RewardedVideoAdEvent.rewarded) {
        setState(() {
          _coins += rewardAmount;
        });
      }
    };
  }

  BannerAd _bannerAd;
  NativeAd _nativeAd;
  InterstitialAd _interstitialAd;
  int _coins = 0;

  BannerAd createBannerAd() {
    return BannerAd(
      adUnitId: AdManager.bannerAdUnitId,
      size: AdSize.banner,
      targetingInfo: MobileAdTargetingInfo(),
      listener: (MobileAdEvent event) {
        print("BannerAd event $event");
      },
    );
  }

  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
      adUnitId: InterstitialAd.testAdUnitId,
      targetingInfo: MobileAdTargetingInfo(),
      listener: (MobileAdEvent event) {
        print("InterstitialAd event $event");
      },
    );
  }

  NativeAd createNativeAd() {
    return NativeAd(
      adUnitId: NativeAd.testAdUnitId,
      factoryId: 'adFactoryExample',
      targetingInfo: MobileAdTargetingInfo(),
      listener: (MobileAdEvent event) {
        print("$NativeAd event $event");
      },
    );
  }

  loadAds() async {
    var adsSub = Firestore.instance
        .collection(ADS_BASE)
        .where(COUNTRY, isEqualTo: userModel.getString(COUNTRY))
        .where(ADS_EXPIRY, isLessThan: DateTime.now().millisecondsSinceEpoch)
        .snapshots()
        .listen((shots) {
      for (var doc in shots.documentChanges) {
        final model = BaseModel(doc: doc.document);
        if (doc.type == DocumentChangeType.removed) {
          adsList.removeWhere((e) => e.getObjectId() == model.getObjectId());
          continue;
        }
        //if (model.myItem()) continue;
        if (model.getInt(STATUS) != APPROVED) continue;
        int p =
            adsList.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          adsList[p] = model;
        } else {
          adsList.add(model);
        }
      }

      adsSetup = true;
      if (mounted) setState(() {});
    });
    subs.add(adsSub);
  }

  loadRecommended({bool reset = false}) async {
    recommendedMatches.clear();
    setState(() {});
    QuerySnapshot shots1 = await Firestore.instance
        .collection(USER_BASE)
        .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
        .where(RELATIONSHIP, isEqualTo: userModel.getInt(RELATIONSHIP))
        .getDocuments();

    QuerySnapshot shots2 = await Firestore.instance
        .collection(USER_BASE)
        .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
        .where(ETHNICITY, isEqualTo: userModel.getInt(ETHNICITY))
        .getDocuments();

    List allShots = [];
    allShots.addAll(shots1.documents);
    allShots.addAll(shots2.documents);

    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);

    for (DocumentSnapshot doc in allShots) {
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (!model.signUpCompleted) continue;
      if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;

      if (useLimit && model.getMap(POSITION).isEmpty) continue;
      if (model.getMap(POSITION).isNotEmpty) {
        final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
        final distance = (await calculateDistanceTo(geoPoint) / 1000);
        if (useLimit && (distance > limit)) {
          String email = model.getEmail();
          print("Distance $distance limit $limit email $email");
          continue;
        }
        model.put(DISTANCE, distance);
      }

      int index = recommendedMatches
          .indexWhere((bm) => model.getObjectId() == bm.getObjectId());
      if (index == -1) {
        recommendedMatches.add(model);
      } else {
        recommendedMatches[index] = model;
      }
    }
    recommendedSetup = true;
    if (mounted) setState(() {});
  }

  String strockImage;
  var loadingSub;
  int minAge = 18;
  int maxAge = 80;
  int onlineType = -1;
  int interestType = -1;
  int genderType = -1;
  Timer strockImagesTimer;

  loadQuickStrock() async {
    if (loadingSub != null) {
      loadingSub.cancel();
      hookupList.clear();
      setup = false;
      setState(() {});
    }
    Geoflutterfire geo = Geoflutterfire();
    Map myPosition = userModel.getMap(POSITION);
    if (myPosition.isEmpty) {
      return;
    }
    GeoPoint geoPoint = myPosition["geopoint"];
    double lat = geoPoint.latitude;
    double lon = geoPoint.longitude;
    // if (filterLocation != null) {
    //   lat = filterLocation.getDouble(LATITUDE);
    //   lon = filterLocation.getDouble(LONGITUDE);
    // }
    GeoFirePoint center = geo.point(latitude: lat, longitude: lon);

    // get the collection reference or query
    var collectionReference = Firestore.instance
        .collection(USER_BASE)
        .where(QUICK_HOOKUP, isEqualTo: 0);
    double radius = 50000;
    loadingSub = geo
        .collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: POSITION)
        .listen((event) {
      for (DocumentSnapshot doc in event) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (!model.signUpCompleted) continue;
        if (genderType != -1) if (model.getInt(GENDER) != genderType) continue;
        if (onlineType > 0) {
          if (onlineType == 1) if (!isOnline(model)) continue;
          int now = DateTime.now().millisecondsSinceEpoch;
          if (onlineType == 2) if ((now - (model.getInt(TIME))) >
              Duration.millisecondsPerSecond) continue;
        }

        if (minAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (minAge > age) continue;
        }
        if (maxAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (maxAge < age) continue;
        }
        if (interestType != -1) {
          if (model.getInt(RELATIONSHIP) != interestType) continue;
        }

        if (model.hookUpPhotos.isEmpty) continue;
        if (model.getInt(GENDER) != userModel.getInt(PREFERENCE)) continue;

//        if (model.getInt(DEF_STROCK_PHOTO) > model.hookUpPhotos.length ||
//            model.getInt(DEF_STROCK_PHOTO) < 0) {
//          model
//            ..put(DEF_STROCK_PHOTO, 0)
//            ..updateItems(delaySeconds: 1);
//        }
//        model
//          ..put(DEF_PROFILE_PHOTO, 0)
//          ..updateItems(delaySeconds: 1);

        int index = hookupList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) hookupList.add(model);
      }
      loadingSub.cancel();
      strockSetup = true;
      if (mounted) setState(() {});
    });
  }

  loadStrockTimer() {
    strockImagesTimer = Timer.periodic(Duration(seconds: 5), (timer) {
      if (hookupList.isEmpty) return;

      final rand = Random();
      int p = rand.nextInt(hookupList.length);
      BaseModel model = hookupList[p];
      //print(p);
      //print(model.getList(HOOKUP_PHOTOS));
      //print(model.items);
      // if (model.hookUpPhotos.isEmpty) {
      //   model = peopleList[nextP];
      //   strockImage = model.hookUpPhotos[0].imageUrl;
      // }
      strockImage = model.hookUpPhotos[model.getInt(DEF_STROCK_PHOTO)].imageUrl;
      if (mounted) setState(() {});
    });
  }

  checkPhotos() {
    List<BaseModel> profilePhotos = userModel.profilePhotos;
    List<BaseModel> hookUpPhotos = userModel.hookUpPhotos;

    final notUploadedPhotos = profilePhotos.where((e) => e.isLocal);
    final notUploadedHooks = hookUpPhotos.where((e) => e.isLocal);

    if (notUploadedPhotos.isEmpty && notUploadedHooks.isEmpty) return;

    uploadMediaFiles(profilePhotos, onError: (e) {
      uploadMediaFiles(profilePhotos, onError: (e) {},
          onUploaded: (List<BaseModel> p) {
        userModel
          ..put(PROFILE_PHOTOS, p.map((e) => e.items).toList())
          ..updateItems();
        setState(() {});
      });
    }, onUploaded: (List<BaseModel> p) {
      userModel
        ..put(PROFILE_PHOTOS, p.map((e) => e.items).toList())
        ..updateItems();
      setState(() {});
    });

    uploadMediaFiles(hookUpPhotos, onError: (e) {
      uploadMediaFiles(hookUpPhotos, onError: (e) {},
          onUploaded: (List<BaseModel> p) {
        userModel
          ..put(HOOKUP_PHOTOS, p.map((e) => e.items).toList())
          ..updateItems();
        setState(() {});
      });
    }, onUploaded: (List<BaseModel> p) {
      userModel
        ..put(HOOKUP_PHOTOS, p.map((e) => e.items).toList())
        ..updateItems();
      setState(() {});
    });
  }

  chkUpdate() async {
    int version = appSettingsModel.getInt(VERSION_CODE);
    if (Platform.isIOS) version = appSettingsModel.getInt(VERSION_CODE_IOS);
    PackageInfo pack = await PackageInfo.fromPlatform();
    String v = pack.buildNumber;
    int myVersion = int.parse(v);
    if (myVersion < version) {
      pushAndResult(context, UpdateLayout(), opaque: false);
    }
  }

  handleMessage(var message) async {
    final dynamic data = message;
    BaseModel model = BaseModel(items: message);
    String title = model.getString("title");
    String body = model.getString("message");

    if (data != null) {
      String type = data[TYPE];
      String id = data[OBJECT_ID];
      if (type != null) {
        if (type == PUSH_TYPE_CHAT && visibleChatId != id) {
          pushAndResult(
              context,
              ChatMain(
                id,
                otherPerson: null,
              ));
        }
      }
    }
  }

  setUpLocation() async {
    print(permissionGranted);
    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) return;
    }
    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) return;
    }

    locationData = await location.getLocation();
    Geoflutterfire geo = Geoflutterfire();

    myLocation = geo.point(
        latitude: locationData.latitude, longitude: locationData.longitude);

    //PlaceMark
    final place = await Geolocator()
        .placemarkFromCoordinates(myLocation.latitude, myLocation.longitude);

    userModel
      ..put(POSITION, myLocation.data)
      ..put(MY_LOCATION, place[0].name)
      ..put(COUNTRY, place[0].country)
      ..put(COUNTRY_CODE, place[0].isoCountryCode)
      ..updateItems();
  }

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  /// Create a [AndroidNotificationChannel] for heads up notifications
  AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
    enableVibration: true,
    playSound: true,
  );

  setupPush() async {
    FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);

    /// Create an Android Notification Channel.
    ///
    /// We use this channel in the `AndroidManifest.xml` file to override the
    /// default FCM channel to enable heads up notifications.
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    /// Update the iOS foreground notification presentation options to allow
    /// heads up notifications.
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'ic_notify',
              ),
            ));
      }
    });

    FirebaseMessaging.instance.setAutoInitEnabled(true);
    FirebaseMessaging.instance.subscribeToTopic('all');
    FirebaseMessaging.instance.subscribeToTopic(userModel.getObjectId());
    FirebaseMessaging.instance.getToken().then((String token) async {
      if (userModel.getString(TOKEN).isEmpty)
        userModel
          ..put(TOKEN, token)
          ..updateItems();
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      handleMessage(message.data);
    });

/*    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        //handleMessage(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        handleMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        handleMessage(message);
      },
      //onBackgroundMessage: myBackgroundMessageHandler,
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: false));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    firebaseMessaging.subscribeToTopic('all');
    firebaseMessaging.subscribeToTopic(userModel.getObjectId());
    firebaseMessaging.getToken().then((String token) async {
      if (userModel.getString(TOKEN).isEmpty)
        userModel
          ..put(TOKEN, token)
          ..updateItems();
    });*/
  }

  void onPause() {
    if (userModel == null) return;
    int prevTimeOnline = userModel.getInt(TIME_ONLINE);
    int timeActive = (DateTime.now().millisecondsSinceEpoch) - timeOnline;
    int newTimeOnline = timeActive + prevTimeOnline;
    userModel.put(IS_ONLINE, false);
    userModel.put(TIME_ONLINE, newTimeOnline);
    userModel.updateItems();
    timeOnline = 0;
  }

  void onResume() async {
    if (userModel == null) return;
    timeOnline = DateTime.now().millisecondsSinceEpoch;
    userModel.put(IS_ONLINE, true);
    userModel.put(
        PLATFORM,
        Platform.isAndroid
            ? ANDROID
            : Platform.isIOS
                ? IOS
                : WEB);
    if (!userModel.getBoolean(NEW_APP)) {
      userModel.put(NEW_APP, true);
    }
    userModel.updateItems();

    Future.delayed(Duration(seconds: 2), () {
      setUpLocation();
      checkLaunch();
    });
  }

  Future<void> checkLaunch() async {
    const platform = const MethodChannel("channel.john");
    try {
      Map response = await platform
          .invokeMethod('launch', <String, String>{'message': ""});
      int type = response[TYPE];
      String chatId = response[CHAT_ID];

//      toastInAndroid(type.toString());
//      toastInAndroid(chatId);

      if (type == LAUNCH_CHAT) {
        pushAndResult(
            context,
            ChatMain(
              chatId,
              otherPerson: null,
            ));
      }

      if (type == LAUNCH_REPORTS) {
        pushAndResult(context, ReportMain());
      }

      //toastInAndroid(response);
    } catch (e) {
      //toastInAndroid(e.toString());
      //batteryLevel = "Failed to get what he said: '${e.message}'.";
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    if (state == AppLifecycleState.paused) {
      onPause();
    }
    if (state == AppLifecycleState.resumed) {
      onResume();
    }

    super.didChangeAppLifecycleState(state);
  }

  List loadedIds = [];
  loadMessages() async {
    var lock = Lock();
    await lock.synchronized(() async {
//      List<Map> myChats = List.from(userModel.getList(MY_CHATS));
      var sub = Firestore.instance
          .collection(CHAT_IDS_BASE)
          .where(PARTIES, arrayContains: userModel.getObjectId())
          .snapshots()
          .listen((shots) {
        for (DocumentSnapshot doc in shots.documents) {
          BaseModel chatIdModel = BaseModel(doc: doc);
          String chatId = chatIdModel.getObjectId();
          if (userModel.getList(DELETED_CHATS).contains(chatId)) continue;
          if (loadedIds.contains(chatId)) {
            continue;
          }
          loadedIds.add(chatId);

          var sub = Firestore.instance
              .collection(CHAT_BASE)
              .where(PARTIES, arrayContains: userModel.getUserId())
              .where(CHAT_ID, isEqualTo: chatId)
              .orderBy(TIME, descending: true)
              .limit(1)
              .snapshots()
              .listen((shots) async {
            if (shots.documents.isNotEmpty) {
              BaseModel cModel = BaseModel(doc: (shots.documents[0]));
              if (isBlocked(null, userId: getOtherPersonId(cModel))) {
                lastMessages.removeWhere(
                    (bm) => bm.getString(CHAT_ID) == cModel.getString(CHAT_ID));
                chatMessageController.add(true);
                return;
              }
            }
            if (stopListening.contains(chatId)) return;
            for (DocumentSnapshot doc in shots.documents) {
              BaseModel model = BaseModel(doc: doc);
              String chatId = model.getString(CHAT_ID);
              int index = lastMessages.indexWhere(
                  (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
              if (index == -1) {
                lastMessages.add(model);
              } else {
                lastMessages[index] = model;
              }

              if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                  !model.myItem() &&
                  visibleChatId != model.getString(CHAT_ID)) {
                try {
                  if (!showNewMessageDot.contains(chatId))
                    showNewMessageDot.add(chatId);
                  setState(() {});
                } catch (E) {
                  if (!showNewMessageDot.contains(chatId))
                    showNewMessageDot.add(chatId);
                  setState(() {});
                }
                countUnread(chatId);
              }
            }

            String otherPersonId = getOtherPersonId(chatIdModel);
            loadOtherPerson(otherPersonId);

            try {
              lastMessages
                  .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
            } catch (E) {}
          });

          subs.add(sub);
        }
        chatSetup = true;
        if (mounted) setState(() {});
      });
      subs.add(sub);
    });
  }

  loadConnects() async {
    var lock = Lock();
    await lock.synchronized(() async {
      var sub = Firestore.instance
          .collection(CONNECTS_BASE)
          .where(PARTIES, arrayContains: userModel.getObjectId())
          .snapshots()
          .listen((shots) {
        for (DocumentSnapshot doc in shots.documents) {
          BaseModel connect = BaseModel(doc: doc);
          if (!connect.getList(READ_BY).contains(userModel.getObjectId())) {
            if (!connectCount.contains(connect.getObjectId()))
              connectCount.add(connect.getObjectId());
          }
        }
        if (mounted) setState(() {});
      });
      subs.add(sub);
    });
  }

  loadOtherPerson(String uId, {int delay = 0}) async {
    var lock = Lock();
    await lock.synchronized(() async {
      Future.delayed(Duration(seconds: delay), () async {
        if (uId.isEmpty) return;
        Firestore.instance
            .collection(USER_BASE)
            .document(uId)
            .get()
            .then((doc) {
          if (doc == null) return;
          if (!doc.exists) return;

          BaseModel user = BaseModel(doc: doc);
          otherPeronInfo[uId] = user;
          if (mounted) setState(() {});
        });
      });
    }, timeout: Duration(seconds: 10));
  }

  countUnread(String chatId) async {
    var lock = Lock();
    lock.synchronized(() async {
      int count = 0;
      QuerySnapshot shots = await Firestore.instance
          .collection(CHAT_BASE)
          .where(CHAT_ID, isEqualTo: chatId)
          .getDocuments();

      List list = [];
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          count++;
          list.add(model);
        }
      }
      if (list.isNotEmpty) unreadCounter[chatId] = list;
      chatMessageController.add(true);
    });
  }

  loadNotification() async {
    var sub = Firestore.instance
        .collection(NOTIFY_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .limit(1)
        .orderBy(TIME_UPDATED, descending: true)
        .snapshots()
        .listen((shots) {
      //toastInAndroid(shots.documents.length.toString());
      for (DocumentSnapshot d in shots.documents) {
        BaseModel model = BaseModel(doc: d);
        /*int p = nList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (p == -1) {
          nList.add(model);
        } else {
          nList[p] = model;
        }*/

        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          showNewNotifyDot = true;
          setState(() {});
        }
      }
      /*nList.sort((bm1, bm2) =>
          bm2.getInt(TIME_UPDATED).compareTo(bm1.getInt(TIME_UPDATED)));*/
    });

    subs.add(sub);
    //notifySetup = true;
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    //print(userModel.getUserId());
    return WillPopScope(
      onWillPop: () {
        //backThings();
        io.exit(0);
        return;
      },
      child: Scaffold(
        backgroundColor: AppConfig.appColor,
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                color: AppConfig.appColor,
                height: MediaQuery.of(context).size.height * .3,
              ),
              page()
            ],
          ),
        ),
      ),
    );
  }

  topWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          //duration: Duration(milliseconds: 500),
//      color: AppConfig.appColor,
          color: showTop ? AppConfig.appColor : transparent,
//      height: 100,
          margin: EdgeInsets.only(bottom: 0),
          padding: EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Row(
//        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  fit: FlexFit.tight,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(TextSpan(children: [
                        TextSpan(
                            text: "Hi ",
                            style: textStyle(true, showTop ? 20 : 30, black)),
                        TextSpan(
                            text: "${userModel.firstName},",
                            // text: "Maugost,",
                            style: textStyle(true, showTop ? 20 : 30, black))
                      ])),
                      Text("Meet awesome people.",
                          style: textStyle(true, showTop ? 15 : 20, black))
                    ],
                  ),
                ),
                /*userModel.profilePhotos[0].isLocal
              ? ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Image.file(
              File(userModel.profilePhotos[0].imageUrl),
              height: 80,
              width: 80,
              fit: BoxFit.cover,
            ),
          )
              : imageHolder(80, userModel.profilePhotos[0].imageUrl,
              // getFirstPhoto(
              //     userModel.profilePhotos.map((e) => e.items).toList()),
              stroke: 2,
              strokeColor: white.withOpacity(.4), onImageTap: () {
                pushAndResult(context, MyProfile());
              })*/
                userImageItem(context, userModel, size: showTop ? 55 : 80,
                    onResult: (_) {
                  if (_) loadRecommended();
                  setState(() {});
                })
              ],
            ),
          ]),
        ),
        if (showTop) gradientLine(reverse: true)
      ],
    );
  }

  bool showTop = false;
  page() {
    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              color: AppConfig.appColor,
              height: 200,
              width: double.infinity,
              child: Opacity(
                  opacity: .2,
                  child: Image.asset(love_back,
                      height: 200, width: double.infinity, fit: BoxFit.cover)),
            ),
            Expanded(
                child: Container(
              color: white,
              width: double.infinity,
            ))
          ],
        ),
        NotificationListener(
          onNotification: (ScrollNotification n) {
            double px = n.metrics.pixels;
//            print("Scroll: $px");
            if (px > 85) {
              if (!showTop) {
                showTop = true;
                setState(() {});
              }
            } else {
              if (showTop) {
                showTop = false;
                setState(() {});
              }
            }
            return true;
          },
          child: Container(
              child: ListView(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            children: [
              addSpace(40),
              topWidget(),
              //addSpace(20),
              Card(
//                  decoration: BoxDecoration(color: white, boxShadow: [
//                    BoxShadow(color: black.withOpacity(.3), blurRadius: 5)
//                  ],
//                  borderRadius: BorderRadius.all(Radius.circular(10))),
                margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                elevation: 5, shadowColor: black.withOpacity(.5),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                clipBehavior: Clip.antiAlias,
                //padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    addSpace(10),
                    InkWell(
                      onTap: () {
                        pushAndResult(context, MessagesPage());
                      },
                      // onLongPress: () {
                      //   RewardedVideoAd.instance.show();
                      // },
                      // onDoubleTap: () {
                      //   print(AdManager.rewardedAdUnitId);
                      //
                      //   RewardedVideoAd.instance.load(
                      //       adUnitId: AdManager.rewardedAdUnitId,
                      //       targetingInfo: MobileAdTargetingInfo());
                      //
                      //   /* _nativeAd ??= createNativeAd();
                      //   _nativeAd
                      //     ..load()
                      //     ..show(
                      //         anchorType: Platform.isAndroid
                      //             ? AnchorType.bottom
                      //             : AnchorType.top);*/
                      // },
                      child: Container(
                        height: 100,
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Container(
                                decoration: BoxDecoration(
                                    color: white,
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        width: 2,
                                        color: black.withOpacity(.7))),
                                padding: EdgeInsets.all(10),
                                height: 60,
                                width: 60,
                                child: Image.asset(
                                  "assets/icons/messages.png",
                                  height: 40,
                                  width: 40,
                                )),
                            addSpaceWidth(10),
                            Flexible(
                              child: unreadCounter.isEmpty
                                  ? Text(
                                      "You have no new messages",
                                      style: textStyle(false, 18, black),
                                    )
                                  : Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${unreadCounter.length} unread conversation${unreadCounter.length > 1 ? "s" : ""}",
                                          style: textStyle(true, 16, blue0),
                                        ),
                                        addSpace(5),
                                        Container(
                                          height: 30,
                                          child: ListView.builder(
                                            itemBuilder: (c, p) {
                                              List allList =
                                                  unreadCounter.values.toList();
                                              List list = allList[p];
                                              BaseModel model =
                                                  list[list.length - 1];
                                              return Container(
                                                height: 30,
                                                margin:
                                                    EdgeInsets.only(right: 5),
                                                padding: EdgeInsets.fromLTRB(
                                                    0, 0, 10, 0),
                                                constraints: BoxConstraints(
                                                    maxWidth: 150),
                                                decoration: BoxDecoration(
                                                    color: default_white,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                25))),
                                                child: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Container(
                                                      width: 30,
                                                      height: 30,
                                                      child: Card(
                                                        color: black
                                                            .withOpacity(.1),
                                                        elevation: 0,
                                                        clipBehavior:
                                                            Clip.antiAlias,
                                                        shape: CircleBorder(
                                                            side: BorderSide(
                                                                color: black
                                                                    .withOpacity(
                                                                        .2),
                                                                width: .9)),
                                                        child:
                                                            CachedNetworkImage(
                                                          imageUrl:
                                                              model.getString(
                                                                  USER_IMAGE),
                                                          fit: BoxFit.cover,
                                                        ),
                                                      ),
                                                    ),
                                                    addSpaceWidth(5),
                                                    if (model.getType() !=
                                                        CHAT_TYPE_TEXT)
                                                      Icon(getChatIcon(model)),
                                                    if (model.getType() !=
                                                        CHAT_TYPE_TEXT)
                                                      addSpaceWidth(5),
                                                    Flexible(
                                                      fit: FlexFit.loose,
                                                      child: Text(
                                                        getChatMessage(model),
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            },
                                            padding: EdgeInsets.all(0),
                                            itemCount: unreadCounter.length,
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                          ),
                                        )
                                      ],
                                    ),
                            )
                          ],
                        ),
                      ),
                    ),
//                    GestureDetector(
//                      onTap: () {
//                        if (isAdmin || userModel.isPremium) {
//                          pushAndResult(
//                              context,
//                              NearbySearch(
//                                fromStrock: true,
//                              ));
//                          return;
//                        }
//                        showMessage(context, Icons.error, red0, "Opps Sorry!",
//                            "You cannot view persons on Quick Strock until you become a Premium User",
//                            textSize: 14,
//                            clickYesText: "Subscribe", onClicked: (_) {
//                          if (_) {
//                            openSubscriptionPage(context);
//                          }
//                        }, clickNoText: "Cancel");
//                      },
//                      child: Container(
//                        padding: EdgeInsets.all(10),
//                        color: transparent,
//                        child: Row(
//                          children: [
//                            Container(
//                                decoration: BoxDecoration(
//                                    color: white,
//                                    shape: BoxShape.circle,
//                                    border: Border.all(
//                                        width: null == strockImage ? 2 : 1,
//                                        color: black.withOpacity(.7))),
//                                padding: EdgeInsets.all(
//                                    null == strockImage ? 10 : 0),
//                                height: 60,
//                                width: 60,
//                                child: null == strockImage
//                                    ? Image.asset(
//                                        "assets/icons/hookUps.png",
//                                        height: 40,
//                                        width: 40,
//                                      )
//                                    : ClipRRect(
//                                        borderRadius: BorderRadius.circular(30),
//                                        child: CachedNetworkImage(
//                                          imageUrl: strockImage,
//                                          height: 60,
//                                          width: 60,
//                                          fit: BoxFit.cover,
//                                          fadeInCurve: Curves.easeInOutCirc,
//                                          placeholder: (c, s) {
//                                            return Container(
//                                                height: 60,
//                                                width: 60,
//                                                child: Center(
//                                                    child: Icon(
//                                                  Icons.person,
//                                                  color: white,
//                                                  size: 15,
//                                                )),
//                                                decoration: BoxDecoration(
//                                                    color: AppConfig.appColor,
//                                                    shape: BoxShape.circle));
//                                          },
//                                        ),
//                                      )),
//                            addSpaceWidth(10),
//                            Flexible(
//                              child: Column(
//                                crossAxisAlignment: CrossAxisAlignment.start,
//                                children: [
//                                  Text(
//                                    (isAdmin || userModel.isPremium)
//                                        ? "View available Strockers"
//                                        : "Join in and get Quick Strock!",
//                                    style: textStyle(
//                                        hookupList.isNotEmpty,
//                                        hookupList.isNotEmpty ? 16 : 18,
//                                        hookupList.isNotEmpty
//                                            ? (userModel.isPremium
//                                                ? green
//                                                : red0)
//                                            : black),
//                                  ),
//                                  if (hookupList.isNotEmpty)
//                                    Container(
//                                      margin: EdgeInsets.only(top: 5),
//                                      height: 50,
//                                      child: getStackedImages(hookupList),
//                                    )
//                                ],
//                              ),
//                            )
//                          ],
//                        ),
//                      ),
//                    ),

//                    addSpace(10),
//                    FlatButton(
//                      onPressed: () {
//                        //pushAndResult(context, PaymentTest(),
//                        openSubscriptionPage(context);
//                      },
//                      padding: EdgeInsets.all(15),
//                      color: AppConfig.appColor,
//                      child: Center(
//                          child: Text(
//                            "Go Premium",
//                            style: textStyle(true, 18, black),
//                          )),
//                    ),

                    if (!isAdmin && !userModel.isPremium) ...[
                      addSpace(10),
                      FlatButton(
                        onPressed: () {
                          //pushAndResult(context, PaymentTest(),
                          openSubscriptionPage(context);
                        },
                        padding: EdgeInsets.all(15),
                        color: AppConfig.appColor,
                        child: Center(
                            child: Text(
                          "Go Premium",
                          style: textStyle(true, 18, black),
                        )),
                      )
                    ]
                  ],
                ),
              ),

              Container(
                color: white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    addSpace(10),
                    Container(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, top: 10, bottom: 10),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "See who's around you",
                        style: textStyle(true, 18, black),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                              height: 120,
                              child: MaterialButton(
                                onPressed: () {
                                  pushAndResult(context, NearbySearch());
                                },
                                onLongPress: () {
                                  if (userModel.getEmail() ==
                                      'amqueenester@gmail.com')
                                    pushAndResult(context, NewUpdate());
                                },
                                elevation: 5,
                                color: white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                child: Stack(
                                  fit: StackFit.expand,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      width: double.infinity,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            "assets/icons/nearby.png",
                                            height: 40,
                                            width: 40,
                                          ),
                                          addSpace(10),
                                          Text(
                                            "NearBy",
                                            textAlign: TextAlign.center,
                                            style: textStyle(true, 15, black),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          addSpaceWidth(20),
                          Flexible(
                            child: Container(
                              height: 120,
                              child: MaterialButton(
                                onPressed: () {
                                  pushAndResult(context, MeetMe(),
                                      depend: false);
                                },
                                elevation: 5,
                                color: white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                child: Stack(
                                  fit: StackFit.expand,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      width: double.infinity,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            "assets/icons/heart1.png",
                                            height: 40,
                                            width: 40,
                                          ),
                                          addSpace(10),
                                          Text(
                                            "Meet Me",
                                            textAlign: TextAlign.center,
                                            style: textStyle(true, 15, black),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 50,
                      margin: EdgeInsets.only(right: 20, left: 20),
                      child: MaterialButton(
                        onPressed: () {
                          if (isAdmin || userModel.isPremium) {
                            pushAndResult(context, ShowProfileViews(),
                                result: (_) {
                              loadSeenBy();
                              setState(() {});
                            });
                            return;
                          }
                          showMessage(context, Icons.error, red0, "Opps Sorry!",
                              "You cannot view persons who viewed your profile until you become a Premium User",
                              textSize: 14,
                              clickYesText: "Subscribe", onClicked: (_) {
                            if (_) {
                              openSubscriptionPage(context);
                            }
                          }, clickNoText: "Cancel");
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        clipBehavior: Clip.antiAlias,
                        color: white,
                        padding: EdgeInsets.only(),
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Container(
                                  padding: EdgeInsets.all(16),
                                  child: Text(
                                    seenByList.isEmpty
                                        ? "You have no Profile views yet"
                                        : "Your Profile was viewed by ${seenByList.length} people",
                                    style: textStyle(true, 16, black),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(16),
                                color: AppConfig.appColor,
                                child: Icon(
                                  Icons.navigate_next,
                                  color: white,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    addSpace(30),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            pushAndResult(context, ShowRecommendations());
                          },
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 20,
                              right: 20,
                            ),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Recommended Matches",
                                  style: textStyle(true, 18, black),
                                ),
                                Text(
                                  "View All",
                                  style: textStyle(
                                    false,
                                    14,
                                    black.withOpacity(.5),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        addSpace(10),
                        if (recommendedMatches.isNotEmpty)
//                          Container(
//                            height: 110,
//                            margin: EdgeInsets.only(left: 10),
//                            child: ListView.builder(
//                                itemCount: matches.length,
//                                shrinkWrap: true,
//                                scrollDirection: Axis.horizontal,
//                                itemBuilder: (ctx, p) {
//
//                                }),
//                          )
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: List.generate(
                                  recommendedMatches.length > 15
                                      ? 15
                                      : recommendedMatches.length, (p) {
                                BaseModel model = recommendedMatches[p];

                                String image = "";
                                bool isVideo = false;
                                if (model.profilePhotos.isNotEmpty) {
                                  isVideo = model
                                      .profilePhotos[
                                          model.getInt(DEF_PROFILE_PHOTO)]
                                      .isVideo;
                                  image = isVideo
                                      ? model
                                          .profilePhotos[
                                              model.getInt(DEF_PROFILE_PHOTO)]
                                          .thumbnailUrl
                                      : model
                                          .profilePhotos[
                                              model.getInt(DEF_PROFILE_PHOTO)]
                                          .imageUrl;
                                }

                                return Container(
                                  width: 90,
                                  margin: EdgeInsets.all(4),
//                            height: 90,
//                            width: 90,
                                  //margin: EdgeInsets.all(6),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
//                                        userImageItem(context, model,
//                                            size: 80, padLeft: false),
                                      Stack(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: GestureDetector(
                                              onTap: () {
                                                pushAndResult(
                                                    context,
                                                    ShowProfile(
                                                      theUser: model,
                                                      fromMeetMe: false,
                                                    ),
                                                    depend: false);
                                              },
                                              child: CachedNetworkImage(
                                                imageUrl: image,
                                                width: 100,
                                                height: 100,
                                                fit: BoxFit.cover,
                                                placeholder: (c, s) {
                                                  return Container(
                                                      width: 100,
                                                      height: 100,
                                                      child: Center(
                                                          child: Icon(
                                                        Icons.person,
                                                        color: white,
                                                        size: 15,
                                                      )),
                                                      decoration: BoxDecoration(
                                                        color: black
                                                            .withOpacity(.09),
                                                        //shape: BoxShape.circle
                                                      ));
                                                },
                                              ),
                                            ),
                                          ),
                                          if (isVideo)
                                            Container(
                                              width: 100,
                                              height: 100,
                                              child: Center(
                                                child: Container(
                                                  height: 30,
                                                  width: 30,
                                                  child: Icon(
                                                    Icons.play_arrow,
                                                    color: Colors.white,
                                                    size: 14,
                                                  ),
                                                  decoration: BoxDecoration(
                                                      color: Colors.black
                                                          .withOpacity(0.8),
                                                      border: Border.all(
                                                          color: Colors.white,
                                                          width: 1.5),
                                                      shape: BoxShape.circle),
                                                ),
                                              ),
                                            ),
                                        ],
                                      ),
                                      addSpace(5),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          if (isOnline(model)) ...[
                                            Container(
                                              height: 8,
                                              width: 8,
                                              decoration: BoxDecoration(
                                                  color: green,
                                                  shape: BoxShape.circle),
                                            ),
                                            addSpaceWidth(2),
                                          ],
                                          Flexible(
                                            child: Text(
                                              getFirstName(model),
                                              style: textStyle(true, 14, black),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                );
                              }),
                            ),
                          )
                      ],
                    ),
                    addSpace(50),
                  ],
                ),
              ),
              Container(
                height: 90,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(bottom: 20.0),
                child: NativeAdmob(
                  // Your ad unit id
                  //adUnitID: appSettingsModel.getString('adUnitID'),
                  adUnitID: AdManager.nativeAdUnitId,
                  numberAds: 3,
                  controller: _nativeAdController,
                  type: NativeAdmobType.banner,
                ),
              ),
            ],
          )),
        ),

        /*AnimatedOpacity(opacity: showTop?1:0, duration: Duration(milliseconds: 500),
        child: IgnorePointer(ignoring: !showTop,
        child: topWidget(),),)
       */
        if (showTop) topWidget()
      ],
    );
  }

  backThings() {
    if (userModel != null && !userModel.getBoolean(HAS_RATED)) {
      showMessage(context, Icons.star, blue0, "Rate Us",
          "Enjoying the App? Please support us with 5 stars",
          clickYesText: "RATE APP", clickNoText: "Later", onClicked: (_) {
        if (_ == true) {
          if (appSettingsModel == null ||
              appSettingsModel.getString(PACKAGE_NAME).isEmpty) {
            onPause();
            Future.delayed(Duration(seconds: 1), () {
              io.exit(0);
            });
          } else {
            rateApp();
          }
        } else {
          onPause();
          Future.delayed(Duration(seconds: 1), () {
            io.exit(0);
          });
        }
      });
      return;
    }
    onPause();
    Future.delayed(Duration(seconds: 1), () {
      io.exit(0);
    });
  }

  loadStory() async {
    var storySub = Firestore.instance
        .collection(STORY_BASE)
        .where(GENDER, isEqualTo: userModel.isMale() ? FEMALE : MALE)
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 2)))
        .snapshots()
        .listen((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.documents) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStoryList
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStoryList.add(model);
          added = true;
          if (!model.myItem() &&
              !model.getList(SHOWN).contains(userModel.getObjectId())) {
            if (!newStoryIds.contains(model.getObjectId()))
              newStoryIds.add(model.getObjectId());
          }
        } else {
          allStoryList[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
    var myStorySub = Firestore.instance
        .collection(STORY_BASE)
        .where(USER_ID, isEqualTo: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.documents) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStoryList
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStoryList.add(model);
          added = true;
        } else {
          allStoryList[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
    subs.add(storySub);
    subs.add(myStorySub);
  }

  loadBlocked() async {
    var lock = Lock();
    lock.synchronized(() async {
      QuerySnapshot shots = await Firestore.instance
          .collection(USER_BASE)
          .where(BLOCKED, arrayContains: userModel.getObjectId())
          .getDocuments();

      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        String uId = model.getObjectId();
        String deviceId = model.getString(DEVICE_ID);
        if (!blockedIds.contains(uId)) blockedIds.add(uId);
        if (deviceId.isNotEmpty) if (!blockedIds.contains(deviceId))
          blockedIds.add(deviceId);
      }
    }, timeout: Duration(seconds: 10));
  }

  saveStories(List models) async {
    if (models.isEmpty) {
      uploadingController.add("Uploading Successful");
      Future.delayed(Duration(seconds: 1), () {
        uploadingController.add(null);
      });
      return;
    }
    uploadingController.add("Uploading Story");

    BaseModel model = models[0];
    String image = model.getString(STORY_IMAGE);
    uploadFile(File(image), (res, error) {
      if (error != null) {
        saveStories(models);
        return;
      }
      model.put(STORY_IMAGE, res);
      model.saveItem(STORY_BASE, true);
      models.removeAt(0);
      saveStories(models);
    });
  }

  getStackedImages(List list) {
    List items = [];
    int count = 0;
    for (int i = 0; i < list.length; i++) {
      if (count > 10) break;
      BaseModel model = hookupList[i];
      items.add(Container(
        margin: EdgeInsets.only(left: double.parse((i * 20).toString())),
        child: userImageItem(context, model,
            size: 40, padLeft: false, type: "nah"),
      ));
      count++;
    }
    List<Widget> children = List.from(items.reversed);
    return IgnorePointer(
      ignoring: true,
      child: Container(
        height: 40,
        child: Stack(
          children: children,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class UpdateLayout extends StatelessWidget {
  BuildContext con;
  @override
  Widget build(BuildContext context) {
    String features = appSettingsModel.getString(NEW_FEATURE);
    if (features.isNotEmpty) features = "* $features";
    bool mustUpdate = appSettingsModel.getBoolean(MUST_UPDATE);
    con = context;
    return WillPopScope(
      onWillPop: () {},
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                color: black.withOpacity(.6),
              )),
          Container(
            padding: EdgeInsets.all(15),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (isAdmin) {
                        Navigator.pop(con);
                      }
                    },
                    child: Image.asset(
                      ic_plain,
                      width: 60,
                      height: 60,
                      color: white,
                    ),
                  ),
                  addSpace(10),
                  Text(
                    "New Update Available",
                    style: textStyle(true, 22, white),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(10),
                  Text(
                    features.isEmpty
                        ? "Please update your App to proceed"
                        : features,
                    style: textStyle(false, 16, white.withOpacity(.5)),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(15),
                  Container(
                    height: 40,
                    width: 120,
                    child: FlatButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        color: blue3,
                        onPressed: () {
                          String appLink =
                              appSettingsModel.getString(APP_LINK_IOS);
                          if (Platform.isAndroid)
                            appLink =
                                appSettingsModel.getString(APP_LINK_ANDROID);
                          openLink(appLink);
                        },
                        child: Text(
                          "UPDATE",
                          style: textStyle(true, 14, white),
                        )),
                  ),
                  addSpace(15),
                  if (!mustUpdate)
                    Container(
                      height: 40,
                      child: FlatButton(
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          color: red0,
                          onPressed: () {
                            Navigator.pop(con);
                          },
                          child: Text(
                            "Later",
                            style: textStyle(true, 14, white),
                          )),
                    )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app/dotsIndicator.dart';
import 'package:Strokes/app_config.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

class MainAdminWeb extends StatefulWidget {
  @override
  _MainAdminWebState createState() => _MainAdminWebState();
}

class _MainAdminWebState extends State<MainAdminWeb> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.orange,
      child: LayoutBuilder(builder: (context, con) {
        double padding = scaledSize(100);
        bool showRight = true;
        if (con.maxWidth <= 500) {
          padding = scaledSize(50);
          showRight = false;
        }
        if (con.maxWidth > 500 && con.maxWidth < 900) padding = 50;
        //if (con.maxWidth > 800) padding = scaledSize(300);

        print('wddd ${con.maxWidth}');

        return Stack(
          children: [
            Image.asset(
              'assets/icons/bg.jpg',
              fit: BoxFit.cover,
              width: getScreenWidth(context),
              height: getScreenHeight(context),
            ),
            Opacity(
              opacity: .6,
              child: Container(
                decoration: BoxDecoration(
                    color: AppConfig.appColor,
                    gradient: LinearGradient(colors: [
                      Colors.orange,
                      Colors.deepOrangeAccent,
                    ])),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(15.0),
              child: Builder(builder: (context) {
                return ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: Material(
                    elevation: 20,
                    color: white,
                    child: Center(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 400,
                            color: white,
                            padding: const EdgeInsets.all(25.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Image.asset(
                                      ic_launcher,
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover,
                                    ),
                                    addSpaceWidth(5),
                                    Text(
                                      'Strock.fun',
                                      style: TextStyle(
                                          color: AppConfig.appColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          //fontFamily: 'AvertaB',
                                          // decoration: TextDecoration.combine([
                                          //   TextDecoration.overline,
                                          //   TextDecoration.underline
                                          // ]),
                                          shadows: [
                                            Shadow(color: black.withOpacity(.4))
                                          ]),
                                    ),
                                  ],
                                ),
                                addSpace(10),
                                Text(
                                  'Already Part of the Community?',
                                  style: textStyle(true, 13, black),
                                ),
                                addSpace(10),
                                Column(
                                  children: List.generate(2, (p) {
                                    String asset = ic_facebook;
                                    String text = "Sign in via Facebook";
                                    Color color = Color(0xFF4267B2);

                                    if (p == 1) {
                                      asset = ic_google;
                                      text = "Sign in with Google";
                                      color = Colors.deepOrange;
                                    }

                                    if (p == 2) {
                                      asset = ic_gender;
                                      text = "Others";
                                    }

                                    return RaisedButton.icon(
                                        onPressed: () {},
                                        color: color,
                                        padding: const EdgeInsets.all(10.0),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25)),
                                        icon: Image.asset(
                                          asset,
                                          height: 18,
                                          width: 18,
                                          color: white.withOpacity(.8),
                                        ),
                                        label: Container(
                                          //width: 150,
                                          alignment: Alignment.center,
                                          child: Text(
                                            text,
                                            style: textStyle(
                                                true, 14, Colors.white),
                                          ),
                                        ));
                                  }),
                                ),
                                addSpace(10),
                                Text(
                                  'What are you waiting for?',
                                  style: textStyle(true, 13, black),
                                ),
                                addSpace(10),
                                Container(
                                  decoration: BoxDecoration(
                                      color: AppConfig.appColor,
                                      borderRadius: BorderRadius.circular(25),
                                      gradient: LinearGradient(
                                          colors: [
                                            AppConfig.appColor,
                                            AppConfig.appColor_dark,
                                          ],
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          stops: [0.5, 1.0])),
                                  padding: const EdgeInsets.only(
                                      top: 10,
                                      bottom: 10,
                                      left: 30.0,
                                      right: 30),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Tell us a bit about yourself',
                                        style: textStyle(false, 14, white),
                                      ),
                                      addSpace(10),
                                      Text(
                                        'You are:',
                                        style: textStyle(true, 20, white),
                                      ),
                                      addSpace(10),
                                      Column(
                                        children: List.generate(3, (p) {
                                          String asset = ic_male;
                                          String text = "Male";

                                          if (p == 1) {
                                            asset = ic_female;
                                            text = "Female";
                                          }

                                          if (p == 2) {
                                            asset = ic_gender;
                                            text = "Others";
                                          }

                                          return RaisedButton.icon(
                                              onPressed: () {},
                                              color: Colors.deepOrange,
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          25)),
                                              icon: Image.asset(
                                                asset,
                                                height: 18,
                                                width: 18,
                                                color: white.withOpacity(.8),
                                              ),
                                              label: Container(
                                                width: 100,
                                                alignment: Alignment.center,
                                                child: Text(
                                                  text,
                                                  style: textStyle(
                                                      true, 14, Colors.white),
                                                ),
                                              ));
                                        }),
                                      ),
                                      addSpace(10),
                                      DotsIndicator(
                                        dotsCount: 4,
                                        position: 0,
                                        decorator: DotsDecorator(
                                            size: Size(6, 6),
                                            activeSize: Size(10, 10),
                                            spacing: EdgeInsets.all(2),
                                            activeColor: white,
                                            color: white.withOpacity(.5)),
                                      )
                                    ],
                                  ),
                                ),
                                addSpace(10),
                                Text(
                                  '©2020 Strock.fun',
                                  style: textStyle(false, 12, black),
                                ),
                              ],
                            ),
                          ),
                          if (showRight)
                            Expanded(
                              child: Image.asset(
                                "assets/icons/couple.jpg",
                                fit: BoxFit.cover,
                                height: getScreenHeight(context),
                                width: getScreenWidth(context),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
          ],
        );
      }),
    );
  }

  Container buildAuthPanel() {
    return Container(
      width: 500,
      color: white,
      padding: const EdgeInsets.all(25.0),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                ic_launcher,
                height: 40,
                width: 40,
                fit: BoxFit.cover,
              ),
              Text(
                'Strock.fun',
                style: TextStyle(
                    color: AppConfig.appColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    //fontFamily: 'AvertaB',
                    decoration: TextDecoration.combine(
                        [TextDecoration.overline, TextDecoration.underline]),
                    shadows: [Shadow(color: black.withOpacity(.4))]),
              ),
            ],
          ),
          addSpace(10),
          Text(
            'have already joined,watcha waiting for?',
            style: textStyle(true, 14, Colors.deepOrange),
          ),
          addSpace(10),
          Column(
            children: List.generate(2, (p) {
              String asset = ic_facebook;
              String text = "Sign in via Facebook";
              Color color = Color(0xFF4267B2);

              if (p == 1) {
                asset = ic_google;
                text = "Sign in with Google";
                color = Colors.deepOrange;
              }

              if (p == 2) {
                asset = ic_gender;
                text = "Others";
              }

              return RaisedButton.icon(
                  onPressed: () {},
                  color: color,
                  padding: const EdgeInsets.all(10.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  icon: Image.asset(
                    asset,
                    height: 18,
                    width: 18,
                    color: white.withOpacity(.8),
                  ),
                  label: Container(
                    //width: 150,
                    alignment: Alignment.center,
                    child: Text(
                      text,
                      style: textStyle(true, 14, Colors.white),
                    ),
                  ));
            }),
          ),
          addSpace(10),
          Text(
            'OR',
            style: textStyle(true, 20, black),
          ),
          addSpace(10),
          Container(
            decoration: BoxDecoration(
                color: AppConfig.appColor,
                borderRadius: BorderRadius.circular(25),
                gradient: LinearGradient(colors: [
                  Colors.orange,
                  Colors.deepOrangeAccent,
                ])),
            padding: const EdgeInsets.all(25.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Tell us a bit about yourself',
                  style: textStyle(false, 16, white),
                ),
                addSpace(10),
                Text(
                  'You are:',
                  style: textStyle(true, 25, white),
                ),
                addSpace(10),
                Column(
                  children: List.generate(3, (p) {
                    String asset = ic_male;
                    String text = "Male";

                    if (p == 1) {
                      asset = ic_female;
                      text = "Female";
                    }

                    if (p == 2) {
                      asset = ic_gender;
                      text = "Others";
                    }

                    return RaisedButton.icon(
                        onPressed: () {},
                        color: Colors.deepOrange,
                        padding: const EdgeInsets.all(10.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        icon: Image.asset(
                          asset,
                          height: 18,
                          width: 18,
                          color: white.withOpacity(.8),
                        ),
                        label: Container(
                          width: 100,
                          alignment: Alignment.center,
                          child: Text(
                            text,
                            style: textStyle(true, 14, Colors.white),
                          ),
                        ));
                  }),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  double scaledSize(double size) {
    double screenWidth = getScreenWidth(context);
    double scaleFactor = (screenWidth / 1000).clamp(0.5, 1);
    return size * scaleFactor;
  }

  Widget buildRightPanel() {
    return Expanded(
      child: Image.asset(
        "assets/icons/cover.png",
        fit: BoxFit.cover,
      ),
    );
    Image.asset(
      'assets/icons/couple.jpg',
      fit: BoxFit.cover,
      width: getScreenWidth(context),
      height: getScreenHeight(context),
    );
  }
  /* Container buildRightPanel() {
    return Container(
      decoration: BoxDecoration(
          color: AppConfig.appColor,
          gradient: LinearGradient(colors: [
            Colors.orange,
            Colors.deepOrangeAccent,
          ])),
      padding: const EdgeInsets.all(25.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Tell us a bit about yourself',
            style: textStyle(false, 16, white),
          ),
          addSpace(10),
          Text(
            'You are:',
            style: textStyle(true, 25, white),
          ),
          addSpace(10),
          Column(
            children: List.generate(3, (p) {
              String asset = ic_male;
              String text = "Male";

              if (p == 1) {
                asset = ic_female;
                text = "Female";
              }

              if (p == 2) {
                asset = ic_gender;
                text = "Others";
              }

              return RaisedButton.icon(
                  onPressed: () {},
                  color: Colors.deepOrange,
                  padding: const EdgeInsets.all(10.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  icon: Image.asset(
                    asset,
                    height: 18,
                    width: 18,
                    color: white.withOpacity(.8),
                  ),
                  label: Container(
                    width: 100,
                    alignment: Alignment.center,
                    child: Text(
                      text,
                      style: textStyle(true, 14, Colors.white),
                    ),
                  ));
            }),
          ),
        ],
      ),
    );
  }*/
}

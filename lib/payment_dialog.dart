import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/assets.dart';
import 'package:flutter/material.dart';

import 'admin/StripeService2.dart';
import 'basemodel.dart';
import 'payment_details.dart';

class PaymentDialog extends StatefulWidget {
  final bool isAds;
  final double amount;
  final int premiumPosition;
  final BaseModel adsModel;
  const PaymentDialog(
      {Key key,
      this.isAds = false,
      this.amount,
      this.premiumPosition,
      this.adsModel})
      : super(key: key);
  @override
  _PaymentDialogState createState() => _PaymentDialogState();
}

class _PaymentDialogState extends State<PaymentDialog> {
  double amount;
  BaseModel package = appSettingsModel.getModel(FEATURES_PREMIUM);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    amount = widget.amount;
    setState(() {});
    print(widget.amount);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    StripeService2.init();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: transparent,
      child: Stack(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              color: black.withOpacity(.4),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  )),
              //padding: EdgeInsets.all(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        "Choose Payment Options",
                        style: textStyle(true, 13, black.withOpacity(0.6)),
                      )),
                  FlatButton(
                    onPressed: () {
                      pushReplacementAndResult(
                          context,
                          PaymentDetails(
                            amount: widget.amount,
                            premiumIndex: widget.premiumPosition,
                            adsModel: widget.adsModel,
                            isAds: widget.isAds,
                          ),
                          depend: false);
                    },
                    padding: EdgeInsets.all(15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/icons/stripe.png",
                          width: 60,
                          fit: BoxFit.fitWidth,
                        ),
                        addSpaceWidth(10),
                        Text("Pay With Card"),
                      ],
                    ),
                  ),
                  /*if (Platform.isIOS)
                    FlatButton(
                      onPressed: () async {


                        StripeService2.payWithNative(
                                amount: widget.amount.toString(),
                                currency:
                                    appSettingsModel.getString(APP_CURRENCY))
                            .then((res) {
                          if (!res.success) {
                            onError(res.message);
                            return;
                          }
                          String id = getRandomId();
                          final model = BaseModel(items: res.body);
                          model.put(OBJECT_ID, id);
                          model.put(AMOUNT, amount);
                          model.put(TYPE, widget.isAds ? 1 : 0);
                          model.saveItem(TRANSACTION_BASE, true, document: id,
                              onComplete: () {
                            if (widget.isAds) {
                              uploadFile(
                                  File(widget.adsModel.getString(ADS_IMAGE)),
                                  (res, e) {
                                if (e != null) {
                                  onError(e);
                                  return;
                                }

                                String id = widget.adsModel.getObjectId();
                                widget.adsModel
                                  ..put(ADS_IMAGE, res)
                                  ..put(HAS_PAID, true)
                                  ..put(AMOUNT, amount)
                                  ..saveItem(ADS_BASE, true, document: id,
                                      onComplete: () {
                                    showProgress(false, context);
                                    Future.delayed(Duration(milliseconds: 500),
                                        () {
                                      adsController.add(true);
                                      int count = 0;
                                      Navigator.of(context).popUntil((_) {
                                        return count++ >= 1;
                                      });
                                    });
                                  });
                              });

                              return;
                            }

                            int months;
                            if (widget.premiumPosition == 0) months = 1;
                            if (widget.premiumPosition == 1) months = 6;
                            if (widget.premiumPosition == 2) months = 12;

                            final liveMode =
                                appSettingsModel.getBoolean(STRIPE_IS_LIVE);
                            Duration howLong;
                            if (liveMode) {
                              howLong = Duration(days: months * 30);
                            } else {
                              howLong = Duration(minutes: 10);
                            }

                            int duration = DateTime.now()
                                .add(howLong)
                                .millisecondsSinceEpoch;
                            userModel
                              ..put(ACCOUNT_TYPE, ACCOUNT_TYPE_PREMIUM)
                              ..put(PREMIUM_INDEX, widget.premiumPosition)
                              ..put(AMOUNT, amount)
                              ..put(SUBSCRIPTION_EXPIRY, duration)
                              ..updateItems();
                            showProgress(false, context);
                            Future.delayed(Duration(milliseconds: 500), () {
                              subscriptionController.add(true);
                              popUpUntil(context, MainAdmin());
                            });
                          });
                        }).catchError(onError);
                      },
                      padding: EdgeInsets.all(15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            "assets/icons/${Platform.isIOS ? "apple_pay" : "google_pay"}.png",
                            //height: 20,
                            width: 60,
                            fit: BoxFit.fitWidth,
                          ),
                          addSpaceWidth(10),
                          Text(
                              "Pay With ${Platform.isIOS ? "Apple" : "Google"} Pay"),
                        ],
                      ),
                    ),*/
                  addSpace(20)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  onError(e) {
    showProgress(false, context);
    showMessage(
      context,
      Icons.error,
      red0,
      "Opps Error!",
      e.toString(),
    );
  }
}

//keytool -exportcert -alias key -keystore /Users/bappstack/key.jks | openssl sha1 -binary | openssl base64
//keytool -exportcert -list -v \-alias key -keystore /Users/bappstack/key.jks

import 'dart:async';
import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/PaymentSubAndroid.dart';
import 'package:Strokes/PaymentSubIOS.dart';
import 'package:Strokes/app/dotsIndicator.dart';
import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/date_picker/flutter_datetime_picker.dart';
import 'package:Strokes/dialogs/inputDialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chat_bubble/chat_bubble.dart';
import 'package:filesize/filesize.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:photo/photo.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:video_compress/video_compress.dart';

import '../MainAdmin.dart';
import '../preinit.dart';

class AuthMain extends StatefulWidget {
  @override
  _AuthMainState createState() => _AuthMainState();
}

class _AuthMainState extends State<AuthMain> {
  final pc = PageController();
  int currentPage = 0;
  String photoUrl = "";
  var scaffoldKey = GlobalKey<ScaffoldState>();

  int selectedPreference = -1;
  int selectedRelationship = -1;
  int selectedIntent = -1;
  int selectedSmoke = -1;
  int selectedQuickHookUp = 1;
  List<BaseModel> profilePhotos = [];
  List<BaseModel> hookUpPhotos = [];

  int selectedGender = -1;
  int selectedEthnicity = -1;
  bool emailNotification = false;
  bool pushNotification = false;
  String birthDate;
  bool activatePremuim = false;
  String yourCity = "";
  String wowFactor = "";

  final packagesIOS =
      availablePackages.where((e) => !e.id.contains("ads")).toList();
  //BaseModel package = appSettingsModel.getModel(FEATURES_PREMIUM);

  StreamSubscription<List<PurchaseDetails>> _subscription;
  List<StreamSubscription> subs = List();
  bool _purchasePending = false;
  //final FlutterInappPurchase flutterIAP = FlutterInappPurchase.instance;
  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];
  bool inAppSetup = false;

  String get pageTitle {
    if (currentPage == 0) return "Your Account";
    return "Dating Preferences";
  }

  String get btnTitle {
    if (currentPage == 0) return "Continue";
    return "Finish";
  }

  @override
  void initState() {
    super.initState();
    loadUser();
    asyncInitState();
  }

  void asyncInitState() async {
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');
    if (!mounted) return;
//    String msg = await FlutterInappPurchase.instance.consumeAllItems;
//    print("consumeAllItems: $msg");

    if (Platform.isAndroid) {
      // refresh items for android
      try {
        String msg = await FlutterInappPurchase.instance.consumeAllItems;
        print('consumeAllItems: $msg');
      } catch (err) {
        print('consumeAllItems error: $err');
      }
    }

    getInAppProducts();

//    var sub = FlutterInappPurchase.connectionUpdated.listen((connected) {
//      print('connected: $connected');
//    });
//
//    var sub1 = FlutterInappPurchase.purchaseUpdated.listen((productItem) {
//      print('purchase-updated: $productItem');
//      //handleSubscribed();
//    });
//
//    var sub2 = FlutterInappPurchase.purchaseError.listen((purchaseError) {
//      print('purchase-error: $purchaseError');
//      //onError(purchaseError);
//    });
//    subs.add(sub);
//    subs.add(sub1);
//    subs.add(sub2);
  }

  @override
  void dispose() {
//    _subscription.cancel();
    for (var s in subs) s?.cancel();
    FlutterInappPurchase.instance?.endConnection;
    super.dispose();
  }

  void getInAppProducts() async {
    final inAppIds = appSettingsModel.getList(SUB_IAP_IDS);
    List<IAPItem> items = await FlutterInappPurchase.instance
        .getProducts(List<String>.from(inAppIds));
    for (var item in items) {
      print('${item.productId}');
      if (!item.productId.contains("sub_")) continue;
      int p = items.indexWhere((e) => e.productId == item.productId);
      if (p == -1)
        this._items[p] = item;
      else
        this._items.add(item);
    }
    inAppSetup = true;
    setState(() {});
  }

  loadUser() {
    selectedGender = userModel.selectedGender;
    selectedEthnicity = userModel.selectedEthnicity;
    emailNotification = userModel.emailNotification;
    pushNotification = userModel.pushNotification;
    selectedPreference = userModel.selectedPreference;
    selectedRelationship = userModel.selectedRelationship;
    selectedQuickHookUp = userModel.selectedQuickHookUp;
    profilePhotos = userModel.profilePhotos;
    hookUpPhotos = userModel.hookUpPhotos;
    birthDate = userModel.birthDate;
    yourCity = userModel.city;
    wowFactor = userModel.getString(WOW_FACTOR);
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //showProgress(true, context, msg: "Hello");
    setState(() {});
    return WillPopScope(
      onWillPop: () async {
        //io.exit(0);
        if (currentPage != 0) {
          pc.jumpTo((currentPage - 1).toDouble());
          return false;
        }
        await FirebaseAuth.instance.signOut();
        await GoogleSignIn().signOut();
        await FacebookLogin().logOut();
        popUpUntil(context, PreInit());
        return false;
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 40, right: 0, left: 10, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          pageTitle,
                          style: textStyle(true, 25, black),
                        ),
                        addSpace(10),
                        Container(
                          padding: EdgeInsets.all(1),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: black.withOpacity(.7)),
                          child: DotsIndicator(
                            dotsCount: 2,
                            position: currentPage,
                            decorator: DotsDecorator(
                              size: const Size.square(5.0),
                              color: white,
                              activeColor: AppConfig.appColor,
                              activeSize: const Size(10.0, 7.0),
                              activeShape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: () async {
                      if (currentPage != 0) {
                        pc.jumpTo((currentPage - 1).toDouble());
                        return;
                      }
                      await FirebaseAuth.instance.signOut();
                      await GoogleSignIn().signOut();
                      await FacebookLogin().logOut();
                      popUpUntil(context, PreInit());
                    },
                    shape: CircleBorder(),
                    padding: EdgeInsets.all(20),
                    child: Icon(
                      Icons.close,
                      color: black,
                    ),
                  )
                ],
              ),
            ),
            Flexible(
              child: PageView(
                controller: pc,
                physics: NeverScrollableScrollPhysics(),
                onPageChanged: (p) {
                  setState(() {
                    currentPage = p;
                  });
                },
                children: [
                  authPage1(),
                  authPage2(),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(14),
              child: FlatButton(
                onPressed: () {
                  if (currentPage == 0)
                    validateAuth0();
                  else
                    validateAuth1();
                },
                color: AppConfig.appColor,
                padding: EdgeInsets.all(16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: Center(
                  child: Text(
                    btnTitle,
                    style: textStyle(true, 16, white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  authPage1() {
    //GoogleFonts.futu;
    return Container(
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
          Text("Profile Photos", style: textStyle(true, 16, black)),
          profilePhotoBox(),
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
          Text("Gender", style: textStyle(true, 16, black)),
          addSpace(5),
          Row(
            children: List.generate(genderType.length, (p) {
              bool active = selectedGender == p;
              return fieldSelector(genderType[p], active: active, onTap: () {
                setState(() {
                  selectedGender = p;
                });
              });
            }),
          ),
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
          Text("Birthdate", style: textStyle(true, 16, black)),
          GestureDetector(
            onTap: () {
              bool empty = null == birthDate || birthDate.isEmpty;

              int year;
              int month;
              int day;
              if (!empty) {
                var birthDay = birthDate.split("-");
                year = num.parse(birthDay[0]);
                month = num.parse(birthDay[1]);
                day = num.parse(birthDay[2]);
              }

              DatePicker.showDatePicker(
                context,
                showTitleActions: true,
                minTime: DateTime(1930, 12, 31),
                maxTime: DateTime(2040, 12, 31),
                onChanged: (date) {},
                onConfirm: (date) {
                  setState(() {
                    int year = date.year;
                    int month = date.month;
                    int day = date.day;
                    birthDate = "$year-${formatDOB(month)}-${formatDOB(day)}";
                  });
                },
                currentTime: empty ? null : DateTime(year, month, day),
              );
            },
            child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: black.withOpacity(.1)),
                    borderRadius: BorderRadius.circular(8)),
                padding: EdgeInsets.all(14),
                margin: EdgeInsets.all(10),
                child: Builder(
                  builder: (ctx) {
                    bool empty = null == birthDate || birthDate.isEmpty;

                    return Row(children: [
                      Text(empty ? "Date" : birthDate,
                          style: textStyle(
                              true, 14, black.withOpacity(empty ? 0.6 : 1))),
                      Spacer(),
                      Icon(Icons.event,
                          color: black.withOpacity(empty ? 0.6 : 1))
                    ]);
                  },
                )),
          ),
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
          Text("Ethnicity", style: textStyle(true, 16, black)),
          addSpace(5),
          Wrap(
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: List.generate(ethnicityType.length, (p) {
              bool active = selectedEthnicity == p;
              return fieldSelector(ethnicityType[p],
                  active: active,
                  size: 100,
                  //alignment: Alignment.centerLeft,
                  margin: 8, onTap: () {
                setState(() {
                  selectedEthnicity = p;
                });
              });
            }),
          ),
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 20),
          Container(
            decoration: BoxDecoration(
                color: red, borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.info,
                  color: white,
                ),
                addSpaceWidth(5),
                Flexible(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text:
                                'By Clicking on "Continue", You hereby agree to our ',
                            style: textStyle(false, 15, white)),
                        TextSpan(
                            text: 'Terms of Service',
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () => openLink(
                                  appSettingsModel.getString(TERMS_LINK)),
                            style: textStyle(true, 15, AppConfig.appColor)),
                        TextSpan(
                            text: ' and ', style: textStyle(false, 15, white)),
                        TextSpan(
                            text: 'Privacy Policy',
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () => openLink(
                                  appSettingsModel.getString(PRIVACY_LINK)),
                            style: textStyle(true, 15, AppConfig.appColor)),
                        TextSpan(
                            text: ' Binding our community.',
                            style: textStyle(false, 15, white)),
                      ],
                    ),
                    //textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          addSpace(10),
          InkWell(
            onTap: () {
              setState(() {
                emailNotification = !emailNotification;
              });
            },
            child: Container(
              child: Row(
                children: [
                  Checkbox(
                      activeColor: AppConfig.appColor,
                      value: emailNotification,
                      onChanged: (b) {
                        setState(() {
                          emailNotification = b;
                        });
                      }),
                  Flexible(
                    child: Row(
                      children: [
                        Text("I want to recieve ",
                            style: textStyle(false, 16, black)),
                        Container(
                          child: Text("Email Notifications",
                              style: textStyle(false, 16, black)),
                          decoration: BoxDecoration(
                              border: Border(bottom: BorderSide())),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                pushNotification = !pushNotification;
              });
            },
            child: Container(
              child: Row(
                children: [
                  Checkbox(
                      activeColor: AppConfig.appColor,
                      value: pushNotification,
                      onChanged: (b) {
                        setState(() {
                          pushNotification = b;
                        });
                      }),
                  //addSpaceWidth(5),

                  Flexible(
                    child: Row(
                      children: [
                        Text("I want to recieve ",
                            style: textStyle(false, 16, black)),
                        Container(
                          child: Text("Push Notifications",
                              style: textStyle(false, 16, black)),
                          decoration: BoxDecoration(
                              border: Border(bottom: BorderSide())),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ]),
      ),
    );
  }

  authPage2() {
    return Container(
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ChatBubble(
            direction: ChatBubbleNipDirection.LEFT,
            nipLength: 25.0,
            nipRadius: 10.0,
            nipTop: 40.0,
            radius: 10,
            child: Container(
              padding:
                  EdgeInsets.only(left: 40, right: 20, top: 20, bottom: 20),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                color: AppConfig.appColor,
                //border: Border.all(color: AppConfig.appColor)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Who are you looking for,",
                      style: textStyle(false, 20, black)),
                  addSpace(5),
                  Text("Who do you see spending all your time with?",
                      style: textStyle(false, 14, black)),
                ],
              ),
            ),
          ),

          // addSpace(20),
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
//           Text("What city do you live in?", style: textStyle(true, 16, black)),
//
//           GestureDetector(
//             onTap: () {
// //              pushAndResult(context, SearchPlace(), result: (BaseModel res) {
// //                setState(() {
// //                  yourCity = res.getString(PLACE_NAME);
// //                });
// //              }, depend: false);
//               pushAndResult(
//                   context,
//                   inputDialog(
//                     "Your City",
//                     hint: "What city do you live in?",
//                     message: userModel.getString(WOW_FACTOR),
//                   ), result: (_) {
//                 if (null == _) return;
//                 yourCity = _;
//                 setState(() {});
//               });
//             },
//             child: Container(
//               width: double.infinity,
//               padding: EdgeInsets.all(14),
//               margin: EdgeInsets.all(10),
//               decoration: BoxDecoration(
//                   border: Border.all(color: black.withOpacity(.1), width: 1),
//                   borderRadius: BorderRadius.circular(15),
//                   color: (yourCity.isEmpty) ? white : AppConfig.appColor),
//               child: Row(
//                 children: [
//                   Flexible(
//                     fit: FlexFit.tight,
//                     child: Text(
//                       (yourCity.isEmpty) ? "Enter your city" : yourCity,
//                       style: textStyle(false, 18,
//                           (yourCity.isEmpty) ? black.withOpacity(.6) : white),
//                       maxLines: 1,
//                     ),
//                   ),
//                   Icon(
//                     Icons.place,
//                     size: 18,
//                     color: (yourCity.isEmpty) ? black.withOpacity(.6) : white,
//                   )
//                 ],
//               ),
//             ),
//           ),
//           addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),

          Text("What's your preference?", style: textStyle(true, 16, black)),
          addSpace(5),
          Row(
            children: List.generate(preferenceType.length, (p) {
              bool active = selectedPreference == p;
              return fieldSelector(preferenceType[p], active: active,
                  onTap: () {
                setState(() {
                  selectedPreference = p;
                });
              });
            }),
          ),
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
          Text("What are you looking for?", style: textStyle(true, 16, black)),
          addSpace(5),
          Wrap(
            children: List.generate(relationshipType.length, (p) {
              bool active = selectedRelationship == p;
              return fieldSelector(relationshipType[p], active: active,
                  onTap: () {
                setState(() {
                  selectedRelationship = p;
                });
              });
            }),
          ),
//          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
//          Row(
//            children: [
//              Text("Do you want to be listed on Quick Strock?",
//                  style: textStyle(true, 16, black)),
//              addSpaceWidth(5),
//              GestureDetector(
//                  onTap: () {
//                    showMessage(context, Icons.info, black, "Quick Strock",
//                        "Quick Strock is for spontaneous meet/date");
//                  },
//                  child: Icon(Icons.info)),
//            ],
//          ),
//          Row(
//            children: List.generate(quickHookUps.length, (p) {
//              bool active = selectedQuickHookUp == p;
//              return fieldSelector(quickHookUps[p], active: active, onTap: () {
//                setState(() {
//                  selectedQuickHookUp = p;
//                });
//              });
//            }),
//          ),
          if (selectedQuickHookUp == 0) ...[
            hookUpPhotoBox(),
            Row(
              children: [
                Text("Whats your Wow Factor?",
                    style: textStyle(true, 16, black)),
                addSpaceWidth(5),
                GestureDetector(
                    onTap: () {
                      showMessage(context, Icons.info, black, "Quick Strock",
                          "What kind of person are you interested to hookup with?");
                    },
                    child: Icon(Icons.info)),
                Spacer(),
                IconButton(
                  icon: Image.asset(
                    ic_edit_profile,
                    color: black,
                    height: 20,
                  ),
                  onPressed: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Wow Factor",
                          hint: "Write about your wow factor",
                          message: userModel.getString(WOW_FACTOR),
                        ), result: (_) {
                      if (null == _) return;
                      wowFactor = _;
                      userModel
                        ..put(WOW_FACTOR, _)
                        ..updateItems();

                      setState(() {});
                    });
                  },
                ),
              ],
            ),
            GestureDetector(
              onTap: () {
                pushAndResult(
                    context,
                    inputDialog(
                      "Wow Factor",
                      hint: "Write about your wow factor",
                      message: userModel.getString(WOW_FACTOR),
                    ), result: (_) {
                  if (null == _) return;
                  userModel
                    ..put(WOW_FACTOR, _)
                    ..updateItems();

                  setState(() {});
                });
              },
              child: Container(
                  decoration: BoxDecoration(
                      color: black.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.all(15),
                  alignment: Alignment.centerLeft,
                  child: Text(
                      userModel.getString(WOW_FACTOR).isEmpty
                          ? "Write about your wow factor"
                          : userModel.getString(ABOUT_ME),
                      style: textStyle(false, 16, black.withOpacity(.6)))),
            ),
          ],
          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),

          Row(
            children: [
              Text("What's your Wow Factor?",
                  style: textStyle(true, 16, black)),
              addSpaceWidth(5),
              GestureDetector(
                  onTap: () {
                    showMessage(context, Icons.info, black, "Quick Strock",
                        "What kind of person are you interested to hookup with?");
                  },
                  child: Icon(Icons.info)),
              Spacer(),
              IconButton(
                icon: Image.asset(
                  ic_edit_profile,
                  color: black,
                  height: 20,
                ),
                onPressed: () {
                  pushAndResult(
                      context,
                      inputDialog(
                        "Wow Factor",
                        hint: "Write about your wow factor",
                        message: wowFactor,
                      ), result: (_) {
                    if (null == _) return;
                    wowFactor = _;

                    setState(() {});
                  }, depend: false);
                },
              ),
            ],
          ),
          GestureDetector(
            onTap: () {
              pushAndResult(
                  context,
                  inputDialog(
                    "Wow Factor",
                    hint: "Write about your wow factor",
                    message: wowFactor,
                  ), result: (_) {
                if (null == _) return;
                wowFactor = _;

                setState(() {});
              }, depend: false);
            },
            child: Container(
                decoration: BoxDecoration(
                    color: black.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(15)),
                padding: EdgeInsets.all(15),
                alignment: Alignment.centerLeft,
                child: Text(
                    wowFactor.isEmpty
                        ? "Write about your wow factor"
                        : wowFactor,
                    style: textStyle(false, 16, black.withOpacity(.6)))),
          ),

//          addLine(.3, black.withOpacity(.2), 0, 20, 0, 10),
//          InkWell(
//            onTap: () {
//              setState(() {
//                activatePremuim = !activatePremuim;
//              });
//            },
//            child: Container(
//              padding: EdgeInsets.only(top: 15, bottom: 15),
//              child: Row(
//                children: [
//                  Text("Become a Premium User Today?",
//                      style: textStyle(true, 16, black)),
//                  addSpaceWidth(5),
//                  GestureDetector(
//                      onTap: () {
//                        showMessage(
//                            context,
//                            Icons.info,
//                            black,
//                            "Premium Access",
//                            "Would you like to get Premium Access to all our features?");
//                      },
//                      child: Icon(Icons.info)),
//                  Spacer(),
//                  if (Platform.isIOS)
//                    CupertinoSwitch(
//                        value: activatePremuim,
//                        activeColor: AppConfig.appColor,
//                        onChanged: (b) {
//                          //_items.clear();
//                          setState(() {
//                            activatePremuim = b;
//                          });
////                          Future.delayed(Duration(milliseconds: 15),(){
////                            getInAppProducts();
////                          });
//                        })
//                  else
//                    Switch(
//                        value: activatePremuim,
//                        activeColor: AppConfig.appColor,
//                        onChanged: (b) {
//                          setState(() {
//                            activatePremuim = b;
//                          });
//                        })
//                ],
//              ),
//            ),
//          ),
//          if (activatePremuim) packagesBox()
        ]),
      ),
    );
  }

  int selectedPlan = -1;

  packagesBox() {
    bool isIOS = Platform.isIOS;

    final iosItems = _items.where((e) => e.title.contains("Months")).toList();

    return Container(
      color: black.withOpacity(.02),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(isIOS ? _items.length : 3, (p) {
            if (isIOS)
              return packagesForIOS(p);
            else
              return packages(p);
          })),
    );
  }

  packagesForIOS(int p) {
    BaseModel package = appSettingsModel.getModel(FEATURES_PREMIUM);
    final fee = _items[p].price;
    final features = package.getString(FEATURES).split("&");
    String baseCurrency = _items[p].currency;
    String title = _items[p].title;

    //String fee = plan["fee"];
    bool active = selectedPlan == p;
    double mSize = active ? 30 : 20;
    double fSize = active ? 20 : 16;

    return Flexible(
        child: GestureDetector(
      onTap: () {
        setState(() {
          selectedPlan = p;
        });
      },
      child: Container(
        padding: EdgeInsets.all(14),
        decoration: BoxDecoration(
            color: active ? white : black.withOpacity(.04),
            border: Border.all(
                width: active ? 3 : 1,
                color: active ? AppConfig.appColor : black.withOpacity(.1))),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: textStyle(true, fSize, black.withOpacity(.7)),
            ),
//            Text("Months"),
//            addSpace(10),
            Text(
              fee,
              style: textStyle(true, fSize,
                  active ? AppConfig.appColor : black.withOpacity(.7)),
            ),
            Text(
              "($baseCurrency)",
            ),
          ],
        ),
      ),
    ));
  }

  packages(int p) {
    BaseModel package = appSettingsModel.getModel(FEATURES_PREMIUM);
    final fee = package.getList(PREMIUM_FEES)[p];
    final features = package.getString(FEATURES).split("&");
    String baseCurrency = appSettingsModel.getString(APP_CURRENCY);

    String title = p == 0
        ? "1 Month"
        : p == 1
            ? "6 Months"
            : "12 Months";

    //String fee = plan["fee"];
    bool active = selectedPlan == p;
    double mSize = active ? 30 : 20;
    double fSize = active ? 20 : 16;

    return Flexible(
        child: GestureDetector(
      onTap: () {
        setState(() {
          selectedPlan = p;
        });
      },
      child: Container(
        padding: EdgeInsets.all(14),
        decoration: BoxDecoration(
            color: active ? white : black.withOpacity(.04),
            border: Border.all(
                width: active ? 3 : 1,
                color: active ? AppConfig.appColor : black.withOpacity(.1))),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: textStyle(true, fSize, black.withOpacity(.7)),
            ),
//            Text("Months"),
//            addSpace(10),
            Text(
              fee,
              style: textStyle(true, fSize,
                  active ? AppConfig.appColor : black.withOpacity(.7)),
            ),
            Text(
              "($baseCurrency)",
            ),
          ],
        ),
      ),
    ));
  }

  profilePhotoBox() {
    final photos = profilePhotos.where((e) => !e.isHookUps).toList();

    return Column(
      children: [
        addSpace(10),
        if (profilePhotos.isNotEmpty)
          Container(
            height: 240,
            child: LayoutBuilder(
              builder: (ctx, b) {
                int photoLength = profilePhotos.length;
                return Column(
                  children: <Widget>[
                    Flexible(
                      child: ListView.builder(
                          itemCount: photoLength,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (ctx, p) {
                            BaseModel photo = profilePhotos[p];
                            bool isVideo = photo.isVideo;
                            String imageUrl = photo
                                .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                            bool isLocal = photo.isLocal;
                            return Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.all(8),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: isLocal
                                        ? Image.file(
                                            File(imageUrl),
                                            height: 200,
                                            width: 160,
                                            fit: BoxFit.cover,
                                          )
                                        : CachedNetworkImage(
                                            imageUrl: imageUrl,
                                            height: 200,
                                            width: 160,
                                            fit: BoxFit.cover,
                                            placeholder: (ctx, s) {
                                              return placeHolder(200,
                                                  width: 160);
                                            },
                                          ),
                                  ),
                                ),
                                if (isVideo)
                                  Center(
                                    child: Container(
                                      height: 50,
                                      width: 50,
                                      child: Icon(
                                        Icons.play_arrow,
                                        color: Colors.white,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.8),
                                          border: Border.all(
                                              color: Colors.white, width: 1.5),
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    height: 200,
                                    width: 160,
                                    alignment: Alignment.topLeft,
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(5, 25, 0, 0),
                                      width: 30,
                                      height: 30,
                                      child: new RaisedButton(
                                          padding: EdgeInsets.all(0),
                                          elevation: 2,
                                          shape: CircleBorder(),
                                          color: red0,
                                          child: Icon(
                                            Icons.close,
                                            color: white,
                                            size: 13,
                                          ),
                                          onPressed: () {
                                            //toast(scaffoldKey, "Removed!");

                                            profilePhotos.removeAt(p);
                                            setState(
                                              () {},
                                            );
                                          }),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          }),
                    ),
                  ],
                );
              },
            ),
          ),
        if (profilePhotos.length < 10) ...[
          //addSpace(10),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: FlatButton(
              onPressed: () async {
                pickAssets("normal");
              },
              color: black.withOpacity(.5),
              padding: EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              child: Center(
                child: Text(
                  'Add',
                  style: textStyle(true, 14, white),
                ),
              ),
            ),
          )
        ],
        addSpace(10),
      ],
    );
  }

  hookUpPhotoBox() {
    final photos = profilePhotos.where((e) => e.isHookUps).toList();

    return Column(
      children: [
        addSpace(10),
        if (hookUpPhotos.isNotEmpty)
          Container(
            height: 240,
            child: LayoutBuilder(
              builder: (ctx, b) {
                int photoLength = hookUpPhotos.length;
                return Column(
                  children: <Widget>[
                    Flexible(
                      child: ListView.builder(
                          itemCount: photoLength,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (ctx, p) {
                            BaseModel photo = hookUpPhotos[p];
                            bool isVideo = photo.isVideo;
                            String imageUrl = photo
                                .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                            bool isLocal = photo.isLocal;
                            return Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.all(8),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: isLocal
                                        ? Image.file(
                                            File(imageUrl),
                                            height: 200,
                                            width: 160,
                                            fit: BoxFit.cover,
                                          )
                                        : CachedNetworkImage(
                                            imageUrl: imageUrl,
                                            height: 200,
                                            width: 160,
                                            fit: BoxFit.cover,
                                            placeholder: (ctx, s) {
                                              return placeHolder(200,
                                                  width: 160);
                                            },
                                          ),
                                  ),
                                ),
                                if (isVideo)
                                  Center(
                                    child: Container(
                                      height: 50,
                                      width: 50,
                                      child: Icon(
                                        Icons.play_arrow,
                                        color: Colors.white,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.8),
                                          border: Border.all(
                                              color: Colors.white, width: 1.5),
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    height: 200,
                                    width: 160,
                                    alignment: Alignment.topLeft,
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(5, 25, 0, 0),
                                      width: 30,
                                      height: 30,
                                      child: new RaisedButton(
                                          padding: EdgeInsets.all(0),
                                          elevation: 2,
                                          shape: CircleBorder(),
                                          color: red0,
                                          child: Icon(
                                            Icons.close,
                                            color: white,
                                            size: 13,
                                          ),
                                          onPressed: () {
                                            //toast(scaffoldKey, "Removed!");

                                            hookUpPhotos.removeAt(p);
                                            setState(
                                              () {},
                                            );
                                          }),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          }),
                    ),
                  ],
                );
              },
            ),
          ),
        if (hookUpPhotos.length < 10) ...[
          //addSpace(10),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: FlatButton(
              onPressed: () async {
                pickAssets("nahh");
              },
              color: black.withOpacity(.5),
              padding: EdgeInsets.all(8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              child: Center(
                child: Text(
                  'Add Photos/Videos',
                  style: textStyle(true, 14, white),
                ),
              ),
            ),
          )
        ],
        addSpace(10),
      ],
    );
  }

  void uploadPhotos(BaseModel model, String s, String id) {
    final photos = s == "normal" ? profilePhotos : hookUpPhotos;
    photos.add(model);
    String key = s == "normal" ? PROFILE_PHOTOS : HOOKUP_PHOTOS;
//    userModel
//      ..put(key, photos.map((e) => e.items).toList())
//      ..updateItems();
    if (mounted) setState(() {});
    //return;
    uploadFile(File(model.imageUrl), (res, err) {
      if (null != err) return;
      final photos = userModel.profilePhotos;

      int pos = photos.indexWhere((e) => e.getObjectId() == id);
      if (pos != -1) {
        model.put(IMAGE_URL, res);
        photos[pos] = model;
        userModel
          ..put(key, photos.map((e) => e.items).toList())
          ..updateItems();
      }

      if (mounted) setState(() {});
    });
  }

  void pickAssets(String s) async {
    PhotoPicker.pickAsset(
            thumbSize: 250,
            context: context,
            provider: I18nProvider.english,
            pickType: PickType.onlyImage,
            rowCount: 3)
        .then((value) async {
      if (value == null) return;
      for (var a in value) {
        String path = (await a.originFile).path;
        bool isVideo = a.type == AssetType.video;
        BaseModel model = BaseModel();
        model.put(OBJECT_ID, a.id);
        model.put(IMAGE_URL, path);
        model.put(IS_VIDEO, isVideo);
        if (isVideo) {
          File file = File(path);
          String fileSize = filesize(file.lengthSync())
              .replaceAll("MB", "")
              .replaceAll("KB", "")
              .replaceAll("TB", "");
          bool hasMB = filesize(file.lengthSync()).contains("MB");
          bool tooLarge = num.parse(fileSize).toInt() > 15;
          if (isVideo && hasMB && tooLarge) {
            showMessage(context, Icons.error, red0, "Opps Sorry!",
                "You cannot upload videos larger than 15MB. Try selecting another video file",
                textSize: 14);
            continue;
          }
          model.put(THUMBNAIL_URL,
              (await VideoCompress().getThumbnailWithFile(path)).path);
        }
        uploadPhotos(model, s, a.id);
      }

      setState(() {});
    }).catchError((e) {});

    /// Use assetList to do something.
  }

  void validateAuth0() async {
    int minAge = 18;
    int maxAge = 80;

    bool empty = null == birthDate || birthDate.isEmpty;
    int age = getAge(DateTime.parse(birthDate));

    if (profilePhotos.isEmpty) {
      toast(scaffoldKey, "Add Profile Photo or Video");
      return;
    }
    if (selectedGender == -1) {
      toast(scaffoldKey, "Choose your Gender");
      return;
    }
    if (empty) {
      toast(scaffoldKey, "Choose your BirthDate");
      return;
    }

    if (minAge > age) {
      toast(scaffoldKey, "Sorry, You must be up to 18 years");
      return;
    }

    if (maxAge < age) {
      toast(scaffoldKey, "Sorry, You can't be above 80 years");
      return;
    }

    if (selectedEthnicity == -1) {
      toast(scaffoldKey, "Choose your Ethnicity");
      return;
    }

    userModel
      ..put(BIRTH_DATE, birthDate)
      ..put(GENDER, selectedGender)
      ..put(ETHNICITY, selectedEthnicity)
      ..put(EMAIL_NOTIFICATION, emailNotification)
      ..put(PUSH_NOTIFICATION, pushNotification)
      ..updateItems();

    pc.nextPage(
        duration: Duration(milliseconds: 500), curve: Curves.easeInToLinear);
    return;
  }

  void validateAuth1() async {
    if (yourCity.isEmpty) {
      toast(scaffoldKey, "What city do you live in?");
      return;
    }
    if (selectedPreference == -1) {
      toast(scaffoldKey, "Choose your Preference");
      return;
    }
    if (selectedRelationship == -1) {
      toast(scaffoldKey, "Choose your Relationship Kind");
      return;
    }
//    if (selectedQuickHookUp == -1) {
//      toast(scaffoldKey, "Choose your Quick Strock Choice");
//      return;
//    }
//    if (selectedQuickHookUp == 0 && hookUpPhotos.isEmpty) {
//      toast(scaffoldKey, "Quick Strock Photos cannot be empty!");
//      return;
//    }

    //  if (selectedQuickHookUp == 0 && userModel.getString(WOW_FACTOR).isEmpty) {
    //   toast(scaffoldKey, "Write about your wow factor!");
    //   return;
    // }

    showProgress(true, context, msg: "Saving Profile...");
    userModel
      ..put(PROFILE_PHOTOS, profilePhotos.map((e) => e.items).toList())
      ..put(HOOKUP_PHOTOS, hookUpPhotos.map((e) => e.items).toList())
      ..put(PREFERENCE, selectedPreference)
      ..put(RELATIONSHIP, selectedRelationship)
      ..put(QUICK_HOOKUP, selectedQuickHookUp)
      ..put(WOW_FACTOR, wowFactor)
      ..put(SIGNUP_COMPLETED, true)
      ..put(CITY, yourCity)
      ..updateItems();
    Future.delayed(Duration(seconds: 3), () {
      popUpUntil(context, MainAdmin());
      return;
      bool isIos = Platform.isIOS;
      if (selectedPlan != -1) {
        popUpUntil(
            context,
            isIos
                ? PaymentSubIOS(
                    fromSignUp: true,
                    premiumIndex: selectedPlan,
                  )
                : PaymentSubAndroid(
                    fromSignUp: true,
                    premiumIndex: selectedPlan,
                  ));
      } else {
        popUpUntil(context, MainAdmin());
      }
    });
  }
}

import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/auth/signUp_page.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/preinit.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'auth_main.dart';
import 'forgot_password.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final nameController = TextEditingController();
  bool emailInvalid = false;
  bool passwordInvalid = false;
  FocusNode focusEmail;
  FocusNode focusPassword;
  BuildContext context;
  bool passwordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isSignUp = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    focusEmail = new FocusNode();
    focusPassword = new FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext cc) {
    return WillPopScope(
      onWillPop: () async {
        pushReplacementAndResult(context, PreInit());
        return false;
      },
      child: Scaffold(
          backgroundColor: white,
          key: _scaffoldKey,
          resizeToAvoidBottomInset: true,
          body: Builder(builder: (c) {
            context = c;
            return page();
          })),
    );
  }

  page() {
    return Column(
      children: <Widget>[
        Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: EdgeInsets.only(top: 40, left: 10),
              child: Row(
                children: [
                  BackButton(
                    color: black,
                    onPressed: () {
                      pushReplacementAndResult(context, PreInit());
                    },
                  ),
                  Text("Login to Strock", style: textStyle(true, 22, black)),
                  // Image.asset("assets/icons/ic_launcher.png",
                  //     height: 50, width: 50),
                ],
              ),
            )),
        Image.asset("assets/icons/ic_launcher.png", height: 50, width: 50),
        addSpace(20),
        Expanded(
          flex: 1,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(0),
            child: Container(
              color: white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  textbox(emailController, "Email Address",
                      focusNode: focusEmail),
                  textbox(passwordController, "Password",
                      focusNode: focusPassword,
                      isPass: true,
                      refresh: () => setState(() {})),
//                  addSpace(10),
                  GestureDetector(
                    onTap: () {
                      pushAndResult(context, ForgotPassword(), depend: false);
                    },
                    child: new Text(
                      "FORGOT PASSWORD?",
                      style: textStyle(true, 17, black),
                    ),
                  ),

//          addLine(.5, black.withOpacity(.1), 15, 0, 15, 0),
                ],
              ),
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            pushAndResult(context, SignUp(), depend: false);
          },
          child: Center(
            child: new Text(
              "CREATE ACCOUNT",
              style: textStyle(true, 17, black),
            ),
          ),
        ),
        Container(
          width: double.infinity,
          height: 60,
          margin: EdgeInsets.all(0),
          child: FlatButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0)),
              color: AppConfig.appColor,
              onPressed: () {
                login();
              },
              child: Text(
                "SIGN IN",
                style: textStyle(true, 16, white),
              )),
        )
      ],
    );
  }

  void login() async {
    String email = emailController.text.trim();
    String password = passwordController.text;

    emailInvalid = !isEmailValid(email);
    passwordInvalid = password.length < 6;
    if (emailInvalid) {
      passwordInvalid = false;
      FocusScope.of(context).requestFocus(focusEmail);
      setState(() {});
      snack("Enter your email address");
      return;
    }

    if (!emailInvalid && password.isEmpty) {
      passwordInvalid = false;
      FocusScope.of(context).requestFocus(focusPassword);
      setState(() {});
      snack("Enter your password");
      return;
    }

    if (passwordInvalid) {
      FocusScope.of(context).requestFocus(focusPassword);
      setState(() {});
      snack("Enter your password");
      return;
    }

    startLogin(email, password);
  }

  void startLogin(email, password) async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      snack("No internet connectivity");
      return;
    }

    showProgress(true, context, msg: "Signing in");

    final FirebaseAuth mAuth = FirebaseAuth.instance;
    mAuth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((user) {
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(user.user.uid)
          .get()
          .then((shot) {
        //showProgress(false, context);
        userModel = BaseModel(doc: shot);
        if (!userModel.signUpCompleted) {
          popUpUntil(context, AuthMain());
          return;
        }
        Future.delayed(Duration(seconds: 1), () {
          popUpUntil(context, MainAdmin());
        });
      }).catchError((error) {
        showProgress(false, context);
        snack(error.toString());
      });
    }).catchError((e) {
      handleError(email, password, e);
    });
  }

  handleError(email, password, error) {
    showProgress(false, context);

    if (error.toString().toLowerCase().contains("no user")) {
      snack("No user found");
      FocusScope.of(context).requestFocus(focusEmail);
      return;
    }

    if (error.toString().toLowerCase().contains("badly formatted")) {
      snack("Invalid email address/password");
      FocusScope.of(context).requestFocus(focusEmail);
      return;
    }

    if (error.toString().toLowerCase().contains("password is invalid")) {
      snack("Invalid email address/password");
      FocusScope.of(context).requestFocus(focusPassword);
      return;
    }
    snack(error.toString());
  }

  final String progressId = getRandomId();

  bool isEmailValid(String email) {
    if (!email.contains("@") || !email.contains(".")) return false;
    return true;
  }

  snack(String text) {
    Future.delayed(Duration(milliseconds: 500), () {
      showSnack(_scaffoldKey, text, useWife: true);
    });
  }
}

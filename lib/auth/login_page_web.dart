import 'dart:convert';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/admin/WebAdmin.dart';
import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../assets.dart';

class LoginPageWeb extends StatefulWidget {
  LoginPageWeb({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPageWeb> {
  bool isChecked = false;
  String errorText = "";
  bool isLoading = false;
  final emailController = TextEditingController();
  final passController = TextEditingController();

  showError(String text) {
    showLoading(false);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      errorText = "";
      setState(() {});
    });
  }

  showLoading(bool value) {
    isLoading = value;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 2,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
              ),
              Container(
                height: MediaQuery.of(context).size.height / 2,
                width: MediaQuery.of(context).size.width,
                color: AppConfig.appColor,
              ),
            ],
          ),
          Center(
            child: Card(
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Container(
                width: MediaQuery.of(context).size.width / 2.5,
                //height: MediaQuery.of(context).size.height / 1.5,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      width: double.infinity,
                      height: errorText.isEmpty ? 0 : 40,
                      color: red0,
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Center(
                          child: Text(
                        errorText,
                        style: textStyle(true, 16, white),
                      )),
                    ),
                    AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      width: double.infinity,
                      height: isLoading ? 40 : 0,
//                      height: 40,
                      color: green,
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Stack(
                        children: [
                          Center(
                              child: Text(
                            "Loggin In...",
                            style: textStyle(true, 16, white),
                          )),
                          LinearProgressIndicator()
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(30),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Image.asset(
                            ic_launcher,
                            height: 50,
                            width: 50,
                          ),
                          addSpace(20),
                          Text(
                            "Strock Admin",
                            textAlign: TextAlign.center,
                            style: textStyle(true, 28, black),
                          ),
                          addSpace(48),
                          TextFormField(
                            controller: emailController,
                            enabled: !isLoading,
                            keyboardType: TextInputType.emailAddress,
                            autofocus: false,
                            decoration: InputDecoration(
                              hintText: 'Email',
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            ),
                          ),
                          addSpace(8),
                          TextFormField(
                            controller: passController,
                            enabled: !isLoading,
                            autofocus: false,
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: 'Password',
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            ),
                          ),
                          addSpace(24),
//                          Row(
//                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                            children: <Widget>[
//                              Row(
//                                children: <Widget>[
//                                  Checkbox(
//                                    value: isChecked,
//                                    onChanged: (value) {
//                                      setState(() {
//                                        isChecked = value;
//                                      });
//                                    },
//                                  ),
//                                  Text("Remember Me")
//                                ],
//                              ),
//                              FlatButton(
//                                child: Text(
//                                  'Forgot password?',
//                                  style: TextStyle(color: Colors.black54),
//                                ),
//                                onPressed: () {},
//                              ),
//                            ],
//                          ),
//                          addSpace(18),
                          RaisedButton(
                            onPressed: isLoading
                                ? null
                                : () {
                                    handleLogin();
                                  },
                            padding: EdgeInsets.all(12),
                            color: AppConfig.appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15)),
                            child: Center(
                              child: Text('Log In',
                                  style: textStyle(true, 16, black)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  handleLogin() async {
    final email = emailController.text.toLowerCase().trim();
    final password = passController.text..toLowerCase().trim();

    if (email.isEmpty) {
      showError("Enter your email address");
      return;
    }

    if (password.isEmpty) {
      showError("Enter your password");
      return;
    }

    print("email $email password $password");

    showLoading(true);
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) {
      print("User ${value.user.email}");
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(value.user.uid)
          .get()
          .then((value) {
        userModel = BaseModel(doc: value);
        isAdmin = userModel.getBoolean(IS_ADMIN) ||
            userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
            userModel.getString(EMAIL) == "ammaugost@gmail.com"||userModel.getBoolean(IS_SUPPORT);


        print("usermodel ${userModel.items}");
        if (!isAdmin) {
          showError("Portal Only Only For Admins && Support!!!");
          FirebaseAuth.instance.signOut();
          return;
        }
        SharedPreferences.getInstance().then((pref) {
          final model = userModel
            ..remove(CREATED_AT)
            ..remove(UPDATED_AT)
            ..remove(POSITION)
            ..remove(MY_LOCATION);
          pref.setString(USER_BASE, jsonEncode(model.items));
          popUpUntil(context, WebAdmin());
        });
      }).catchError((e) {
        showLoading(false);
        showError(e.message);
      });
    }).catchError((e) {
      showLoading(false);
      showError(e.message);
    });
  }
}

import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/auth/login_page_web.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'WebReports.dart';
import 'WebSettings.dart';
import 'WebUsersItem.dart';

class WebAdmin extends StatefulWidget {
  @override
  WebAdminState createState() => WebAdminState();
}

class WebAdminState extends State<WebAdmin> {
  int menuIndex = 0;
  final pageController = PageController();
  double get sideMenuWidth => MediaQuery.of(context).size.width * .2;
  bool setup = false;
  List<StreamSubscription> subs = [];
  List<BaseModel> reportList = [];
  List<BaseModel> usersList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadUsers();
    loadReports();
  }

  @override
  void dispose() {
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadReports() async {
    var sub = Firestore.instance
        .collection(REPORT_BASE)
        .where(STATUS, isEqualTo: STATUS_UNDONE)
        .orderBy(TIME, descending: false)
        .snapshots()
        .listen((value) {
      for (var doc in value.documents) {
        BaseModel model = BaseModel(doc: doc);
        int p = reportList.indexWhere(
            (element) => element.getObjectId() == model.getObjectId());
        if (p != -1) {
          reportList[p] = model;
        } else {
          reportList.add(model);
        }
      }
      setup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  loadUsers() async {
    var sub =
        Firestore.instance.collection(USER_BASE).snapshots().listen((value) {
      for (var doc in value.documents) {
        BaseModel model = BaseModel(doc: doc);
        int p = usersList.indexWhere(
            (element) => element.getObjectId() == model.getObjectId());
        if (p != -1) {
          usersList[p] = model;
        } else {
          usersList.add(model);
        }
      }
      setup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveBuilder(
        builder: (context, sizingInformation) {
          return page();

          // Check the sizing information here and return your UI
          if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
            return Container(color: Colors.blue);
          }

          if (sizingInformation.deviceScreenType == DeviceScreenType.tablet) {
            return Container(color: Colors.red);
          }

          if (sizingInformation.deviceScreenType == DeviceScreenType.watch) {
            return Container(color: Colors.yellow);
          }

          return Container(color: Colors.purple);
        },
      ),
    );
  }

  page() {
    return Stack(
      children: [
        Container(
          color: AppConfig.appColor,
        ),
        Row(
          //mainAxisSize: MainAxisSize.min,
          children: [pageSideMenu(), pageMenuBody()],
        ),
      ],
    );
  }

  pageSideMenu() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: sideMenuWidth,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: transparent,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: white.withOpacity(.0))),
            margin: EdgeInsets.only(left: 5, right: 5, top: 20, bottom: 0),
            padding: EdgeInsets.all(8),
            child: Row(
              children: [
                userImageItem(context, userModel, strokeSize: 1),
                addSpaceWidth(10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      userModel.getString(NAME),
                      style: textStyle(true, 18, white),
                    ),
                    Text(
                      userModel.getEmail(),
                      style: textStyle(false, 14, white),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            width: sideMenuWidth,
            alignment: Alignment.center,
            margin: EdgeInsets.only(left: 5, right: 5, top: 20, bottom: 20),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    supportDialog(context, (_) {});
                  },
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: blue3,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: white.withOpacity(.0))),
                    child: Row(
                      children: [
                        Icon(
                          Icons.add,
                          color: white,
                        ),
                        addSpaceWidth(10),
                        Text(
                          "Add Support",
                          style: textStyle(true, 18, white),
                        ),
                      ],
                    ),
                  ),
                ),
//                addSpace(10),
//                GestureDetector(
//                  onTap: () {
//                    print("members view...");
//                    menuIndex = 4;
//                    pageController.jumpTo(4);
//                    setState(() {});
//                  },
//                  child: Container(
//                      padding: EdgeInsets.all(10),
//                      color: transparent,
//                      child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        children: [
//                          Text(
//                            "View Members",
//                            style: textStyle(menuIndex == 4, 17,
//                                menuIndex == 4 ? white : black),
//                          ),
//                          Icon(
//                            Icons.navigate_next,
//                            color: menuIndex == 4 ? white : black,
//                          )
//                        ],
//                      )),
//                )
              ],
            ),
          ),
          menuTabs(),
          GestureDetector(
            onTap: () {
              yesNoDialog(
                  context, "Logout?", "Are you sure you want to logout?", () {
                FirebaseAuth.instance.signOut();
                popUpUntil(context, LoginPageWeb());
              });
            },
            child: Container(
              width: sideMenuWidth,
              padding: EdgeInsets.all(10),
              child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Logout",
                    style: textStyle(true, 16, black),
                  ),
                  Spacer(),
                  Icon(Icons.navigate_next)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  menuTabs() {
    return Flexible(
      child: Column(
        //mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(isAdmin ? 7 : 6, (index) {
          bool active = menuIndex == index;

          String title = "Users (${usersList.length})";
          dynamic icon = Icons.people;

          if (index == 1) {
            final regular =
                usersList.where((e) => e.getInt(ACCOUNT_TYPE) == 0).toList();
            title = "Users-Regular (${regular.length})";
          }

          if (index == 2) {
            final premium =
                usersList.where((e) => e.getInt(ACCOUNT_TYPE) == 1).toList();
            title = "Users-Premium (${premium.length})";
          }

          if (index == 3) {
            final premium = usersList.where((e) => !e.signUpCompleted).toList();
            title = "Users-Incomplete (${premium.length})";
          }

          if (index == 4) {
            title = "Reports (${reportList.length})";
            icon = Icons.report;
          }

//          if (index == 5) {
//            title = "Ads";
//            icon = Icons.streetview;
//          }

          if (index == 5) {
            title = "Settings";
            icon = Icons.settings;
          }

          if (index == 6) {
            title = "Support Members";
            icon = Icons.people;
          }

          return GestureDetector(
            onTap: () {
              setState(() {
                menuIndex = index;
              });
              pageController.jumpToPage(index);
            },
            child: Container(
              width: sideMenuWidth,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(active ? 0.1 : 0),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  )),
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(active ? 1 : 0),
                        shape: BoxShape.circle),
                    child: Center(
                      child: Icon(
                        icon,
                        size: 15,
                        color: Colors.black.withOpacity(active ? 1 : 0.6),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    title,
                    style: TextStyle(
                        color: Colors.black.withOpacity(active ? 1 : 0.6),
                        fontSize: active ? 16 : 14),
                  )
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  pageMenuBody() {
    return Flexible(
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          bottomLeft: Radius.circular(30),
          topRight: Radius.circular(15),
          bottomRight: Radius.circular(15),
        ),
        child: Container(
          margin: EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                bottomLeft: Radius.circular(30),
                topRight: Radius.circular(15),
                bottomRight: Radius.circular(15),
              )),
          child: PageView(
            controller: pageController,
            //physics: NeverScrollableScrollPhysics(),
            onPageChanged: (p) {
              menuIndex = p;
              setState(() {});
            },
            children: [
              WebUsersItem(0),
              WebUsersItem(1),
              WebUsersItem(2),
              WebUsersItem(3),
              WebReports(),
              //WebAds(),
              WebSettings(),
              WebUsersItem(4)
            ],
          ),
        ),
      ),
    );
  }
}

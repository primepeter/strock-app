import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../MainAdmin.dart';
import '../notificationService.dart';

class WebReports extends StatefulWidget {
  @override
  _WebReportsState createState() => _WebReportsState();
}

class _WebReportsState extends State<WebReports>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  TextEditingController searchController = TextEditingController();
  bool _showCancel = false;
  FocusNode focusSearch = FocusNode();

  List<BaseModel> usersList = [];
  List<BaseModel> mainList = [];
  StreamSubscription<QuerySnapshot> sub;
  bool showProfile = false;

  List<BaseModel> reportItems = List();
  bool setup = false;
  BaseModel theUser;
  double matchRate = 0;
  double pixels = 0;

  String get formatBirthDate {
    final date = DateTime.parse(theUser.birthDate);
    return new DateFormat("MMMM d").format(date);
  }

  reload() async {
    usersList.clear();
    String search = searchController.text.toString().toLowerCase().trim();
    for (BaseModel model in mainList) {
      String contactName = model.getString(NAME).toLowerCase().trim();
      String number = model.getString(PHONE_NUMBER).toLowerCase().trim();
      String email = model.getEmail().toLowerCase().trim();
      if (search.isNotEmpty) {
        if (!contactName.contains(search)) {
          if (!number.contains(search)) {
            if (!email.contains(search)) continue;
          }
        }
      }
      usersList.add(model);
    }

    setState(() {});
  }

  loadUsers() async {
    /*sub =*/ Firestore.instance
        .collection(USER_BASE)
        .getDocuments()
        .then((value) {
      for (var doc in value.documents) {
        BaseModel model = BaseModel(doc: doc);
        int p = usersList.indexWhere(
            (element) => element.getObjectId() == model.getObjectId());
        if (p != -1) {
          usersList[p] = model;
        } else {
          usersList.add(model);
        }
      }
      mainList.addAll(usersList);
      setup = true;
      if (mounted) setState(() {});
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //reload();
    //loadUsers();
    loadReports();
  }

  loadReports() {
    Firestore.instance
        .collection(REPORT_BASE)
        .where(STATUS, isEqualTo: STATUS_UNDONE)
        .orderBy(TIME, descending: false)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot d in shots.documents) {
        BaseModel model = BaseModel(doc: d);
        int p = reportItems
            .indexWhere((bm) => (bm.getObjectId() == model.getObjectId()));
        if (p == -1) reportItems.add(model);
      }
      setup = true;
      if (mounted) setState(() {});
    });
  }

  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }

  double get sideMenuWidth => MediaQuery.of(context).size.width * .2;
  double get screenWidth => getScreenWidth(context) - sideMenuWidth;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: Row(
        children: [
          reportsView(),
          profilesView(),
        ],
      ),
    );
  }

  reportsView() {
    return Flexible(
      child: Container(
        width: screenWidth,
        child: Column(
          children: [
            Expanded(
                flex: 1,
                child: Builder(builder: (ctx) {
                  if (!setup) return loadingLayout();
                  if (reportItems.isEmpty)
                    return emptyLayout(Icons.person, "No Reports Yet!", "");

                  return Container(
                      child: ListView.builder(
                    itemBuilder: (c, p) {
                      return itemView(p);
                      return reportItem(p);
                    },
                    shrinkWrap: true,
                    itemCount: reportItems.length,
                    padding: EdgeInsets.only(top: 10),
                  ));
                }))
          ],
        ),
      ),
    );
  }

  profilesView() {
    return Flexible(
      child: Container(
        width: screenWidth,
        child: theUser == null ? null : profilePage(),
      ),
    );
  }

  profilePage() {
    String image =
        theUser.profilePhotos[theUser.getInt(DEF_PROFILE_PHOTO)].imageUrl;
    return ListView(
      physics: BouncingScrollPhysics(),
      children: [
        addSpace(10),
        Image.network(
          image,
          height: 300,
          width: double.infinity,
          fit: BoxFit.cover,
        ),
        addSpace(10),
        Container(
          color: transparent,
          child: Container(
            child: Stack(
              //mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(color: black.withOpacity(.3), blurRadius: 5),
                  ], color: white, borderRadius: BorderRadius.circular(25)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        theUser.getString(NAME),
                        style: textStyle(true, 18, black),
                      ),
                      addSpace(10),
                      Row(
                        children: [
                          Icon(
                            Icons.date_range,
                            color: black.withOpacity(.5),
                          ),
                          addSpaceWidth(5),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                              text:
                                  "I am ${getMyAge(theUser).toString()} years old",
                              style: textStyle(false, 18, black),
                            ),
//                                        TextSpan(
//                                          text: getMyAge(theUser).toString(),
//                                          style: textStyle(false, 18, black),
//                                        )
                          ]))
                        ],
                      ),
                      addSpace(10),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: black.withOpacity(.5),
                          ),
                          addSpaceWidth(5),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                              text: "Lives in ${theUser.city}",
                              style: textStyle(false, 18, black),
                            ),
//                                        TextSpan(
//                                          text: getMyAge(theUser).toString(),
//                                          style: textStyle(false, 18, black),
//                                        )
                          ]))
                        ],
                      ),
                      addSpace(10),
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/gender.png",
                            height: 30,
                            width: 30,
                            color: black.withOpacity(.6),
                            fit: BoxFit.cover,
                          ),
                          addSpaceWidth(5),
                          Text.rich(TextSpan(children: [
                            TextSpan(text: "Interested in "),
                            TextSpan(
                              text: preferenceType[theUser.getInt(PREFERENCE)],
                              style: textStyle(false, 18, black),
                            )
                          ]))
                        ],
                      ),
                      addSpace(10),
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/gender.png",
                            height: 30,
                            width: 30,
                            color: black.withOpacity(.6),
                            fit: BoxFit.cover,
                          ),
                          addSpaceWidth(5),
                          Text.rich(TextSpan(children: [
                            TextSpan(text: "Relationship Pref "),
                            TextSpan(
                              text: relationshipType[
                                  theUser.getInt(RELATIONSHIP)],
                              style: textStyle(false, 18, black),
                            )
                          ]))
                        ],
                      ),
                      addSpace(10),
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/network.png",
                            height: 30,
                            width: 30,
                            color: black.withOpacity(.6),
                            fit: BoxFit.cover,
                          ),
                          addSpaceWidth(5),
                          Text.rich(TextSpan(children: [
                            TextSpan(text: "Ethnicity Pref "),
                            TextSpan(
                              text: ethnicityType[theUser.getInt(ETHNICITY)],
                              style: textStyle(false, 18, black),
                            )
                          ]))
                        ],
                      ),
                      addSpace(10),
                      Row(
                        children: [
                          Icon(
                            Icons.access_time,
                            color: black.withOpacity(.5),
                          ),
                          addSpaceWidth(5),
                          Text.rich(TextSpan(children: [
                            //TextSpan(text: "Last Seen "),
                            TextSpan(
                              text: getLastSeen(theUser),
                              style: textStyle(false, 18, black),
                            )
                          ]))
                        ],
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: AppConfig.appColor,
                        gradient: LinearGradient(
                            colors: [
                              orange01,
                              orange04,
                              orange01,
                              //AppConfig.appColor.withOpacity(.7),
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight),
                        borderRadius: BorderRadius.circular(25)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.favorite,
                          color: white,
                          size: 20,
                        ),
                        addSpaceWidth(10),
                        Text(
                          "${matchRate.toInt()}% Match!",
                          style: textStyle(false, 16, white),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(20),
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: black.withOpacity(.3), blurRadius: 5),
          ], color: white, borderRadius: BorderRadius.circular(25)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Photos",
                style: textStyle(true, 16, black),
              ),
              //addSpace(10),
              photoBox(theUser.profilePhotos)
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.all(20),
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: black.withOpacity(.3), blurRadius: 5),
          ], color: white, borderRadius: BorderRadius.circular(25)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Quick Strock Photos",
                style: textStyle(true, 16, black),
              ),
              //addSpace(10),
              photoBox(theUser.hookUpPhotos, type: "nah")
            ],
          ),
        ),
        if (theUser.getString(ABOUT_ME).isNotEmpty)
          Container(
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(color: black.withOpacity(.3), blurRadius: 5),
            ], color: white, borderRadius: BorderRadius.circular(25)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "About Me",
                  style: textStyle(true, 16, black),
                ),
                addSpace(10),
                Text(
                  theUser.getString(ABOUT_ME),
                  style: textStyle(false, 14, black),
                ),
              ],
            ),
          ),
        if (theUser.getString(WOW_FACTOR).isNotEmpty)
          Container(
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(color: black.withOpacity(.3), blurRadius: 5),
            ], color: white, borderRadius: BorderRadius.circular(25)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Wow Factor",
                  style: textStyle(true, 16, black),
                ),
                addSpace(10),
                Text(
                  theUser.getString(WOW_FACTOR),
                  style: textStyle(false, 14, black),
                ),
              ],
            ),
          ),
      ],
    );
  }

  photoBox(List<BaseModel> photos, {String type = "normal"}) {
    return Column(
      children: [
        if (photos.isNotEmpty)
          Container(
            height: 240,
            child: LayoutBuilder(
              builder: (ctx, b) {
                int photoLength = photos.length;
                return Column(
                  children: <Widget>[
                    Flexible(
                      child: ListView.builder(
                          itemCount: photoLength,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (ctx, p) {
                            BaseModel photo = photos[p];
                            bool isVideo = photo.isVideo;
                            String imageUrl = photo
                                .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                            bool isLocal = photo.isLocal;

                            List visibleList =
                                List.from(theUser.getList(VISIBLE_TO));
                            bool isVisible =
                                visibleList.contains(userModel.getUserId());

                            bool isPublic = photo.getInt(VISIBILITY) == 0;

                            return GestureDetector(
                              onTap: () {
                                if (isVideo) {
                                  if (photo.imageUrl.isEmpty) return;
                                  pushAndResult(
                                      context,
                                      PlayVideo(
                                          photo.getObjectId(), photo.imageUrl));
                                } else {
                                  pushAndResult(
                                      context, ViewImage([imageUrl], 0),
                                      depend: false);
                                }
                              },
                              child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.all(8),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: isLocal
                                          ? Image.file(
                                              File(imageUrl),
                                              height: 200,
                                              width: 160,
                                              fit: BoxFit.cover,
                                            )
                                          : Stack(
                                              children: [
                                                placeHolder(200, width: 160),
                                                Image.network(
                                                  imageUrl,
                                                  height: 200,
                                                  width: 160,
                                                  fit: BoxFit.cover,
                                                ),
                                              ],
                                            ),
                                    ),
                                  ),
                                  if ((type == "nah") &&
                                      !isPublic &&
                                      !isVisible)
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                        height: 200,
                                        width: 160,
                                        alignment: Alignment.center,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: black.withOpacity(.5),
                                              shape: BoxShape.circle,
                                              border: Border.all(color: white)),
                                          padding: EdgeInsets.all(10),
                                          child: Icon(
                                            Icons.https,
                                            color: white,
                                          ),
                                        ),
                                        decoration: BoxDecoration(
                                          //color: AppConfig.appColor,
                                          gradient: LinearGradient(
                                              colors: [
                                                orange01.withOpacity(.9),
                                                orange04.withOpacity(.9),
                                                //AppConfig.appColor.withOpacity(.7),
                                              ],
                                              begin: Alignment.topLeft,
                                              end: Alignment.bottomRight),
                                        ),
                                      ),
                                    ),
                                  if (isVideo)
                                    Center(
                                      child: Container(
                                        height: 50,
                                        width: 50,
                                        child: Icon(
                                          Icons.play_arrow,
                                          color: Colors.white,
                                        ),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.8),
                                            border: Border.all(
                                                color: Colors.white,
                                                width: 1.5),
                                            shape: BoxShape.circle),
                                      ),
                                    ),
                                ],
                              ),
                            );
                          }),
                    ),
                  ],
                );
              },
            ),
          ),
      ],
    );
  }

  itemView(int p) {
    BaseModel model = reportItems[p];
    bool banned = appSettingsModel.getList(BANNED).contains(model.getUserId());
    bool signUpCompleted = model.signUpCompleted;
    String name = signUpCompleted ? model.getString(NAME) : "Incomplete SignUp";
    String email = model.getEmail();
    String image =
        model.profilePhotos.isEmpty ? "" : model.profilePhotos[0].imageUrl;
    int type = model.getInt(REPORT_TYPE);
    Map map = model.getMap(THE_MODEL);
    BaseModel user = BaseModel(items: map);

    bool isAdmin = model.getBoolean(IS_ADMIN);
    bool isPremium = model.isPremium;
    String keyAdmin = isAdmin ? "DeActivate" : "Activate";
    String keyPremium = isPremium ? "DeActivate" : "Activate";
    int platform = model.getInt(PLATFORM);
    String deviceType =
        platform == 0 ? "Android" : platform == 1 ? "IPhone" : "Web";
    String country = model.getString(COUNTRY);
    bool fraudulent = model.getBoolean(FRAUDULENT);
    return GestureDetector(
      onTap: () {
        theUser = model;
        setState(() {});
      },
      child: Container(
        decoration: BoxDecoration(
            color: (null != theUser && theUser == model)
                ? black.withOpacity(.05)
                : white,
            border: Border.all(color: black.withOpacity(.09))),
        padding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        margin: EdgeInsets.fromLTRB(0, .5, 0, 0),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  width: 40,
                  height: 40,
                  child: Card(
                    color: black.withOpacity(.1),
                    elevation: 0,
                    clipBehavior: Clip.antiAlias,
                    shape: CircleBorder(
                        side: BorderSide(
                            color: black.withOpacity(.2), width: .9)),
                    child: Stack(
                      children: [
                        Container(
                          width: 40,
                          height: 40,
                          child: Center(
                            child: Icon(Icons.account_circle),
                          ),
                        ),
                        Image.network(
                          image,
                          fit: BoxFit.cover,
                          width: 40,
                          height: 40,
                        ),
                      ],
                    ),
                  ),
                ),
                addSpaceWidth(10),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: textStyle(true, 16, black),
                      ),
                      addSpace(3),
                      Text(
                        email,
                        style: textStyle(false, 12, black),
                      ),
                      addSpace(3),
                      Row(
                        children: [
                          Icon(
                            Icons.device_unknown,
                            size: 14,
                            color: black.withOpacity(.5),
                          ),
                          addSpaceWidth(5),
                          Text(
                            deviceType,
                            style: textStyle(false, 12, black),
                          ),
                        ],
                      ),
                      addSpace(3),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            size: 14,
                            color: black.withOpacity(.5),
                          ),
                          addSpaceWidth(5),
                          Text(
                            country,
                            style: textStyle(false, 12, black),
                          ),
                        ],
                      ),
                      if (model.isDeveloper())
                        Text(
                          "DevTeam",
                          style: textStyle(false, 12, black),
                        )
                    ],
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    model
                      ..put(FRAUDULENT, !fraudulent)
                      ..updateItems();
                    setState(() {});
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  color: brown,
                  //padding: EdgeInsets.all(5),
                  child: Row(
                    children: [
                      Icon(
                        fraudulent ? Icons.visibility : Icons.visibility_off,
                        size: 12,
                        color: white,
                      ),
                      addSpaceWidth(4),
                      Text(
                        "${fraudulent ? "" : "Not"} fraudulent",
                        style: textStyle(false, 8, white),
                      ),
                    ],
                  ),
                ),
                addSpaceWidth(4),
                Icon(Icons.navigate_next)
              ],
            ),
            addSpace(5),
            Row(
              children: List.generate(3, (i) {
                String title = banned ? "UnBan" : "Ban";
                Color color = red;
                IconData icon = Icons.block;

                if (i == 1) {
                  title = "Delete";
                  color = blue3;
                  icon = Icons.delete_outline;
                }

                if (i == 2) {
                  title = "Notify";
                  color = AppConfig.appColor;
                  icon = Icons.notifications_active;
                }

                return Flexible(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      onPressed: () {
                        if (i == 0) {
                          yesNoDialog(
                              context,
                              "${banned ? "Unban" : "Ban"} Account?",
                              "Are you sure you want to ${banned ? "Unban" : "Ban"} this users account?",
                              () {
                            appSettingsModel.putInList(
                                BANNED, model.getObjectId(), !banned);
                            appSettingsModel.updateItems();
                            setState(() {});
                          });
                        }

                        if (i == 1) {
                          yesNoDialog(context, "Delete Report?",
                              "Are you sure want this report deleted?", () {
                            model.deleteItem();
                            reportItems.remove(model);
                            setState(() {});
                          });
                        }

                        if (i == 2) {
                          notifyDialog(context, (_) {
                            sendMessageAsAdmin(_, user, () {
                              showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Message Sent!",
                                  "Notification message has been sent to the user.");
                            });
                          });
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      color: color,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            icon,
                            size: 15,
                            color: white,
                          ),
                          addSpaceWidth(5),
                          Text(
                            title,
                            style: textStyle(true, 12, white),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
            addSpace(10),
            commentItem(context, model, List(), () {}, () {}, true,
                isReport: true),
          ],
        ),
      ),
    );
  }

  reportItem(int p) {
    BaseModel model = reportItems[p];
    int type = model.getInt(REPORT_TYPE);
    Map map = model.getMap(THE_MODEL);
    BaseModel user = BaseModel(items: map);
    bool disabled =
        appSettingsModel.getList(DISABLED).contains((user.getObjectId()));
    bool banned =
        appSettingsModel.getList(BANNED).contains((user.getObjectId()));
    return Container(
      padding: EdgeInsets.fromLTRB(10, 8, 10, 8),
      margin: EdgeInsets.fromLTRB(0, .5, 0, 0),
      decoration: BoxDecoration(
          color: (null != theUser && theUser == user)
              ? black.withOpacity(.05)
              : white,
          border: Border.all(color: black.withOpacity(.09))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (type == REPORT_TYPE_PROFILE)
            Opacity(
              opacity: disabled ? (.8) : banned ? (.3) : (1),
              child: GestureDetector(
                onTap: () {
                  theUser = user;
                  setState(() {});
                },
                onLongPress: () {
                  showListDialog(context, [
                    "Mark Completed",
                    "Mark Beware",
                    disabled ? "Enable" : "Disable",
                    banned ? "Unban" : "Ban"
                  ], (int p) {
                    if (p == 0) {
                      showMessage(context, Icons.check, blue0, "Mark Report?",
                          "Are you sure you have handled this report?",
                          clickYesText: "YES",
                          cancellable: true,
                          clickNoText: "Cancel", onClicked: (_) {
                        if (_ == true) {
                          model.put(STATUS, STATUS_COMPLETED);
                          model.updateItems();
                          reportItems.removeWhere(
                              (bm) => bm.getObjectId() == model.getObjectId());
                          setState(() {});
                        }
                      });
                    }
                    if (p == 1) {
//                  pushAndResult(context, ShowAllfromIds(bm.getString(DEVICE_ID)));
                      yesNoDialog(
                          context,
                          "${disabled ? "Enable" : "Disable"} Account?",
                          "Are you sure?", () {
                        appSettingsModel.putInList(
                            DISABLED, user.getObjectId(), !disabled);
                        appSettingsModel.updateItems();
                        setState(() {});
                      });
                    }
                    if (p == 2) {
                      yesNoDialog(
                          context,
                          "${disabled ? "Enable" : "Disable"} Account?",
                          "Are you sure?", () {
                        appSettingsModel.putInList(
                            DISABLED, user.getObjectId(), !disabled);
                        appSettingsModel.updateItems();
                        setState(() {});
                      });
                    }
                    if (p == 3) {
                      yesNoDialog(
                          context,
                          "${banned ? "Unban" : "Ban"} Account?",
                          "Are you sure?", () {
                        appSettingsModel.putInList(
                            BANNED, user.getObjectId(), !banned);
                        appSettingsModel.updateItems();
                        setState(() {});
                      });
                    }
                  });
                },
                child: Container(
                  height: 200,
                  child: Card(
                    color: default_white,
                    elevation: 0,
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        side: BorderSide(
                            color: black.withOpacity(.1), width: .5)),
                    child: Stack(fit: StackFit.expand, children: <Widget>[
                      Image.network(
                        user.profilePhotos[0].imageUrl,
                        fit: BoxFit.cover,
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: gradientLine(
                            height: 120, alpha: .8, reverse: false),
                      ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                getFullName(user),
                                style: textStyle(
                                  false,
                                  20,
                                  white,
                                ),
                                maxLines: 2,
                              ),
                              Text(
                                user.isMale() ? "Male" : "Female",
                                style: textStyle(
                                  true,
                                  12,
                                  white.withOpacity(.5),
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  ),
                ),
              ),
            ),
          addSpace(10),
          commentItem(context, model, List(), () {}, () {}, true,
              isReport: true),
          addSpace(5),
          Row(
            children: List.generate(3, (i) {
              String title = banned ? "UnBan" : "Ban";
              Color color = red;
              IconData icon = Icons.block;

              if (i == 1) {
                title = "Delete";
                color = blue3;
                icon = Icons.delete_outline;
              }

              if (i == 2) {
                title = "Notify";
                color = AppConfig.appColor;
                icon = Icons.notifications_active;
              }

              return Flexible(
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: FlatButton(
                    onPressed: () {
                      if (i == 0) {
                        yesNoDialog(
                            context,
                            "${banned ? "Unban" : "Ban"} Account?",
                            "Are you sure you want to ${banned ? "Unban" : "Ban"} this users account?",
                            () {
                          appSettingsModel.putInList(
                              BANNED, user.getObjectId(), !banned);
                          appSettingsModel.updateItems();
                          setState(() {});
                        });
                      }

                      if (i == 1) {
                        yesNoDialog(context, "Delete Report?",
                            "Are you sure want this report deleted?", () {
                          model.deleteItem();
                          reportItems.remove(model);
                          setState(() {});
                        });
                      }

                      if (i == 2) {
                        notifyDialog(context, (_) {
                          sendMessageAsAdmin(_, user, () {
                            showMessage(
                                context,
                                Icons.check,
                                green,
                                "Message Sent!",
                                "Notification message has been sent to the user.");
                          });
                        });
                      }
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    color: color,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          icon,
                          size: 15,
                          color: white,
                        ),
                        addSpaceWidth(5),
                        Text(
                          title,
                          style: textStyle(true, 12, white),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  sendMessageAsAdmin(String text, BaseModel theUser, onSent) {
    final chatId = "${theUser.getUserId()}$SUPPORT_ID";
    BaseModel chat = BaseModel();
    chat.put(PARTIES, [chatId, theUser.getObjectId()]);
    chat.saveItem(CHAT_IDS_BASE, false, document: chatId);

    final String id = getRandomId();
    bool sDate = canShowDate(chatId);
    final BaseModel model = new BaseModel();
    model.put(CHAT_ID, chatId);
    model.put(MESSAGE, text);
    model.put(OBJECT_ID, id);
    model.put(SHOW_DATE, sDate);
    model.put(IS_ADMIN, true);
    model.put(PARTIES, [SUPPORT_ID, theUser.getUserId()]);
    model.saveItem(CHAT_BASE, true, document: id, onComplete: () {
      print("has hit ${model.items}");
      Map data = Map();
      data[TYPE] = PUSH_TYPE_CHAT;
      data[OBJECT_ID] = chatId;
      data[TITLE] = getFullName(userModel);
      data[MESSAGE] = text;
      NotificationService.sendPush(
          topic: chatId,
          title: getFullName(userModel),
          body: text,
          tag: '${userModel.getObjectId()}chat',
          data: data);
      onSent();
    });
  }

  bool canShowDate(String chatId) {
    bool sDate = true;
    final chatList =
        lastMessages.where((e) => e.getString(CHAT_ID) == chatId).toList();
    if (chatList.isNotEmpty) {
      BaseModel lastChat = chatList[0];
      sDate =
          !isSameDay(lastChat.getTime(), DateTime.now().millisecondsSinceEpoch);
    }
    return sDate;
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
